<?php include("header.php") ?>

<div class="page-wrapper">

    <div class="container p-20">

        <div class="row-15">

            <main class="main-side col-md-9 col-sm-12 col-xs-12" data-main-col="1">

                <div class="gallery-list">

                    <nav class="bg-white shadow p-15 m-b-20">

                        <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="index.php" title="goldnews.az - Ana səhifə" itemprop="item">
                                    <span itemprop="name">Ana səhifə</span>
                                </a>
                                <meta itemprop="position" content="1">
                            </li>
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="photos.php" title="Foto xəbərlər" itemprop="item">
                                    <span itemprop="name">Foto xəbərlər</span>
                                </a>
                                <meta itemprop="position" content="2">
                            </li>
                        </ol>

                    </nav>

                    <div class="row-10">

                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <article class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="photo-view.php" title="title" itemprop="url">

                                    <figure class="thumb hover"  itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <i class="icon-photo-gallery"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300">

                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="500">
                                        <meta itemprop="height" content="300">

                                    </figure>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-photo"></i></span>

                                        <span class="m-r-15" itemprop="genre">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun</time>

                                    </div>

                                </a>

                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                        <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                        <meta itemprop="width" content="400">
                                        <meta itemprop="height" content="400">
                                    </div>
                                    <meta itemprop="name" content="Planksoft">
                                </div>

                                <meta itemprop="author" content="Eldar Zeynallı">
                                <meta itemprop="description" content="description text">
                                <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                            </article>

                        </div>
                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <article class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="photo-view.php" title="title" itemprop="url">

                                    <figure class="thumb hover"  itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <i class="icon-photo-gallery"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300">

                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="500">
                                        <meta itemprop="height" content="300">

                                    </figure>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-photo"></i></span>

                                        <span class="m-r-15" itemprop="genre">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun</time>

                                    </div>

                                </a>

                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                        <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                        <meta itemprop="width" content="400">
                                        <meta itemprop="height" content="400">
                                    </div>
                                    <meta itemprop="name" content="Planksoft">
                                </div>

                                <meta itemprop="author" content="Eldar Zeynallı">
                                <meta itemprop="description" content="description text">
                                <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                            </article>

                        </div>
                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <article class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="photo-view.php" title="title" itemprop="url">

                                    <figure class="thumb hover"  itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <i class="icon-photo-gallery"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300">

                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="500">
                                        <meta itemprop="height" content="300">

                                    </figure>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-photo"></i></span>

                                        <span class="m-r-15" itemprop="genre">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun</time>

                                    </div>

                                </a>

                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                        <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                        <meta itemprop="width" content="400">
                                        <meta itemprop="height" content="400">
                                    </div>
                                    <meta itemprop="name" content="Planksoft">
                                </div>

                                <meta itemprop="author" content="Eldar Zeynallı">
                                <meta itemprop="description" content="description text">
                                <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                            </article>

                        </div>
                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <article class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="photo-view.php" title="title" itemprop="url">

                                    <figure class="thumb hover"  itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <i class="icon-photo-gallery"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300">

                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="500">
                                        <meta itemprop="height" content="300">

                                    </figure>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-photo"></i></span>

                                        <span class="m-r-15" itemprop="genre">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun</time>

                                    </div>

                                </a>

                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                        <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                        <meta itemprop="width" content="400">
                                        <meta itemprop="height" content="400">
                                    </div>
                                    <meta itemprop="name" content="Planksoft">
                                </div>

                                <meta itemprop="author" content="Eldar Zeynallı">
                                <meta itemprop="description" content="description text">
                                <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                            </article>

                        </div>
                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <article class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="photo-view.php" title="title" itemprop="url">

                                    <figure class="thumb hover"  itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <i class="icon-photo-gallery"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300">

                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="500">
                                        <meta itemprop="height" content="300">

                                    </figure>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-photo"></i></span>

                                        <span class="m-r-15" itemprop="genre">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun</time>

                                    </div>

                                </a>

                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                        <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                        <meta itemprop="width" content="400">
                                        <meta itemprop="height" content="400">
                                    </div>
                                    <meta itemprop="name" content="Planksoft">
                                </div>

                                <meta itemprop="author" content="Eldar Zeynallı">
                                <meta itemprop="description" content="description text">
                                <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                            </article>

                        </div>
                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <article class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="photo-view.php" title="title" itemprop="url">

                                    <figure class="thumb hover"  itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <i class="icon-photo-gallery"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300">

                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="500">
                                        <meta itemprop="height" content="300">

                                    </figure>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-photo"></i></span>

                                        <span class="m-r-15" itemprop="genre">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun</time>

                                    </div>

                                </a>

                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                        <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                        <meta itemprop="width" content="400">
                                        <meta itemprop="height" content="400">
                                    </div>
                                    <meta itemprop="name" content="Planksoft">
                                </div>

                                <meta itemprop="author" content="Eldar Zeynallı">
                                <meta itemprop="description" content="description text">
                                <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                            </article>

                        </div>
                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <article class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="photo-view.php" title="title" itemprop="url">

                                    <figure class="thumb hover"  itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <i class="icon-photo-gallery"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300">

                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="500">
                                        <meta itemprop="height" content="300">

                                    </figure>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-photo"></i></span>

                                        <span class="m-r-15" itemprop="genre">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun</time>

                                    </div>

                                </a>

                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                        <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                        <meta itemprop="width" content="400">
                                        <meta itemprop="height" content="400">
                                    </div>
                                    <meta itemprop="name" content="Planksoft">
                                </div>

                                <meta itemprop="author" content="Eldar Zeynallı">
                                <meta itemprop="description" content="description text">
                                <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                            </article>

                        </div>
                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <article class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="photo-view.php" title="title" itemprop="url">

                                    <figure class="thumb hover"  itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <i class="icon-photo-gallery"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300">

                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="500">
                                        <meta itemprop="height" content="300">

                                    </figure>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-photo"></i></span>

                                        <span class="m-r-15" itemprop="genre">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun</time>

                                    </div>

                                </a>

                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                        <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                        <meta itemprop="width" content="400">
                                        <meta itemprop="height" content="400">
                                    </div>
                                    <meta itemprop="name" content="Planksoft">
                                </div>

                                <meta itemprop="author" content="Eldar Zeynallı">
                                <meta itemprop="description" content="description text">
                                <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                            </article>

                        </div>
                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <article class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="photo-view.php" title="title" itemprop="url">

                                    <figure class="thumb hover"  itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <i class="icon-photo-gallery"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300">

                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="500">
                                        <meta itemprop="height" content="300">

                                    </figure>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-photo"></i></span>

                                        <span class="m-r-15" itemprop="genre">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun</time>

                                    </div>

                                </a>

                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                        <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                        <meta itemprop="width" content="400">
                                        <meta itemprop="height" content="400">
                                    </div>
                                    <meta itemprop="name" content="Planksoft">
                                </div>

                                <meta itemprop="author" content="Eldar Zeynallı">
                                <meta itemprop="description" content="description text">
                                <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                            </article>

                        </div>
                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <article class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="photo-view.php" title="title" itemprop="url">

                                    <figure class="thumb hover"  itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <i class="icon-photo-gallery"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300">

                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="500">
                                        <meta itemprop="height" content="300">

                                    </figure>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-photo"></i></span>

                                        <span class="m-r-15" itemprop="genre">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun</time>

                                    </div>

                                </a>

                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                        <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                        <meta itemprop="width" content="400">
                                        <meta itemprop="height" content="400">
                                    </div>
                                    <meta itemprop="name" content="Planksoft">
                                </div>

                                <meta itemprop="author" content="Eldar Zeynallı">
                                <meta itemprop="description" content="description text">
                                <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                            </article>

                        </div>
                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <article class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="photo-view.php" title="title" itemprop="url">

                                    <figure class="thumb hover"  itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <i class="icon-photo-gallery"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300">

                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="500">
                                        <meta itemprop="height" content="300">

                                    </figure>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-photo"></i></span>

                                        <span class="m-r-15" itemprop="genre">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun</time>

                                    </div>

                                </a>

                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                        <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                        <meta itemprop="width" content="400">
                                        <meta itemprop="height" content="400">
                                    </div>
                                    <meta itemprop="name" content="Planksoft">
                                </div>

                                <meta itemprop="author" content="Eldar Zeynallı">
                                <meta itemprop="description" content="description text">
                                <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                            </article>

                        </div>
                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <article class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="photo-view.php" title="title" itemprop="url">

                                    <figure class="thumb hover"  itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <i class="icon-photo-gallery"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300">

                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="500">
                                        <meta itemprop="height" content="300">

                                    </figure>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-photo"></i></span>

                                        <span class="m-r-15" itemprop="genre">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun</time>

                                    </div>

                                </a>

                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                        <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                        <meta itemprop="width" content="400">
                                        <meta itemprop="height" content="400">
                                    </div>
                                    <meta itemprop="name" content="Planksoft">
                                </div>

                                <meta itemprop="author" content="Eldar Zeynallı">
                                <meta itemprop="description" content="description text">
                                <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                            </article>

                        </div>
                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <article class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="photo-view.php" title="title" itemprop="url">

                                    <figure class="thumb hover"  itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <i class="icon-photo-gallery"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300">

                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="500">
                                        <meta itemprop="height" content="300">

                                    </figure>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-photo"></i></span>

                                        <span class="m-r-15" itemprop="genre">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun</time>

                                    </div>

                                </a>

                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                        <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                        <meta itemprop="width" content="400">
                                        <meta itemprop="height" content="400">
                                    </div>
                                    <meta itemprop="name" content="Planksoft">
                                </div>

                                <meta itemprop="author" content="Eldar Zeynallı">
                                <meta itemprop="description" content="description text">
                                <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                            </article>

                        </div>
                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <article class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="photo-view.php" title="title" itemprop="url">

                                    <figure class="thumb hover"  itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <i class="icon-photo-gallery"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300">

                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="500">
                                        <meta itemprop="height" content="300">

                                    </figure>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-photo"></i></span>

                                        <span class="m-r-15" itemprop="genre">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun</time>

                                    </div>

                                </a>

                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                        <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                        <meta itemprop="width" content="400">
                                        <meta itemprop="height" content="400">
                                    </div>
                                    <meta itemprop="name" content="Planksoft">
                                </div>

                                <meta itemprop="author" content="Eldar Zeynallı">
                                <meta itemprop="description" content="description text">
                                <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                            </article>

                        </div>

                    </div>

                    <nav class="pagination-col bg-white shadow p-15 m-b-20">
                        <a href="#" class="btn br float-left transition-3s disabled hidden-xs">ƏVVƏLKİ</a>

                        <div class="clearfix">
                            <a href="#" class="active">1</a>
                            <a href="#">2</a>
                            <a href="#">3</a>
                            <a href="#">4</a>
                            <a href="#">5</a>
                            <a>…</a>
                            <a href="#">10</a>
                            <a href="#">11</a>
                        </div>

                        <a href="#" class="btn br float-right transition-3s hidden-xs">SONRAKI</a>
                    </nav>

                </div>

            </main>

            <aside class="col-md-3 col-sm-12 col-xs-12" data-col="1" itemscope itemtype="http://schema.org/WPSideBar">

                <div class="fixed-wrap">

                    <div class="row-10">

                        <div class="col-lg-12 col-md-12 col-sm-4 hidden-xs m-b-20">
                            <div class="banner-100p shadow">
                                <div class="content flex-center"></div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 hidden-sm hidden-xs">

                            <div class="banner-20p m-b-10 shadow visible-xs">
                                <div class="content flex-center"></div>
                            </div>

                            <div class="time-line m-b-20 shadow border-bottom clear">

                                <h2 class="content-title m-0 p-15 border-bottom text-uppercase"><i class="icon-newspaper-2"></i> Xəbər lenti</h2>

                                <div class="content scrl">

                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-photo"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>

                                    <div class="item border-bottom visible-xs clear">
                                        <div class="banner-20p">
                                            <div class="content flex-center"></div>
                                        </div>
                                    </div>

                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>

                                    <div class="item border-bottom visible-xs clear">
                                        <div class="banner-20p">
                                            <div class="content flex-center"></div>
                                        </div>
                                    </div>

                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>

                                    <div class="item border-bottom visible-xs clear">
                                        <div class="banner-20p">
                                            <div class="content flex-center"></div>
                                        </div>
                                    </div>

                                    <a href="#" class="item block text-center bg-gray clear">Daha çox göstər</a>

                                </div>

                            </div>

                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-4 hidden-xs m-b-20">
                            <div class="banner-100p shadow">
                                <div class="content flex-center"></div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-4 hidden-xs">
                            <div class="banner-100p shadow">
                                <div class="content flex-center"></div>
                            </div>
                        </div>

                    </div>

                </div>

            </aside>

        </div>

    </div>

</div>

<?php include("footer.php") ?>