<?php include("header.php") ?>

<div class="page-wrapper">

    <div class="container p-l-20 p-r-20">

        <div class="row-15">

            <div class="main-side col-md-12 col-sm-12 col-xs-12">

                <div class="static-pages m-t-20">

                    <nav class="bg-white shadow p-15 m-b-20">

                        <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="index.php" title="goldnews.az - Ana səhifə" itemprop="item">
                                    <span itemprop="name">Ana səhifə</span>
                                </a>
                                <meta itemprop="position" content="1">
                            </li>
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="category.php" title="Haqqımızda" itemprop="item">
                                    <span itemprop="name">Haqqımızda</span>
                                </a>
                                <meta itemprop="position" content="2">
                            </li>
                        </ol>

                    </nav>

                    <div class="row-10">

                        <div class="col-md-3 col-sm-12 col-xs-12 m-b-20">

                            <div class="static-side-menu bg-white shadow p-t-10 p-b-10 clear">

                                <a href="about.php" class="tr-3s current-page"><i class="icon-info-circle-thin"></i> Haqqımızda</a>
                                <a href="contact.php" class="tr-3s"><i class="icon-envelope-o"></i> Bizimlə əlaqə</a>
                                <a href="ads.php" class="tr-3s"><i class="icon-ads"></i> Saytda reklam</a>
                                <a href="weather.php" class="tr-3s"><i class="wi wi-day-cloudy"></i> Hava</a>
                                <a href="currency.php" class="tr-3s"><i class="icon-currency-azn"></i> Valyuta</a>

                            </div>

                        </div>

                        <div class="col-md-9 col-sm-12 col-xs-12 m-b-20">

                            <div class="static-page-wrapper shadow bg-white clear">
                                <h1 class="content-title p-20 m-0 border-bottom"><strong><i class="icon-info-circle-thin"></i> HAQQIMIZDA</strong></h1>

                                <div class="clear p-20">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi culpa libero possimus quasi quidem quo quod sit? Ad corporis dicta dolorem ex, fugit ipsum molestias non recusandae sit vel vero?</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi culpa libero possimus quasi quidem quo quod sit? Ad corporis dicta dolorem ex, fugit ipsum molestias non recusandae sit vel vero?</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi culpa libero possimus quasi quidem quo quod sit? Ad corporis dicta dolorem ex, fugit ipsum molestias non recusandae sit vel vero?</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi culpa libero possimus quasi quidem quo quod sit? Ad corporis dicta dolorem ex, fugit ipsum molestias non recusandae sit vel vero?</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi culpa libero possimus quasi quidem quo quod sit? Ad corporis dicta dolorem ex, fugit ipsum molestias non recusandae sit vel vero?</p>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<?php include("footer.php") ?>