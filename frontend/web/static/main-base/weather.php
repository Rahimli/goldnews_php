<?php include("header.php") ?>

<div class="page-wrapper">

    <div class="container p-l-20 p-r-20">

        <div class="row-15">

            <div class="main-side col-md-12 col-sm-12 col-xs-12">

                <div class="static-pages m-t-20">

                    <nav class="bg-white shadow p-15 m-b-20">

                        <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="index.php" title="goldnews.az - Ana səhifə" itemprop="item">
                                    <span itemprop="name">Ana səhifə</span>
                                </a>
                                <meta itemprop="position" content="1">
                            </li>
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="category.php" title="Hava" itemprop="item">
                                    <span itemprop="name">Hava</span>
                                </a>
                                <meta itemprop="position" content="2">
                            </li>
                        </ol>

                    </nav>

                    <div class="row-10">

                        <div class="col-md-3 col-sm-12 col-xs-12 m-b-20">

                            <div class="static-side-menu shadow bg-white p-t-10 p-b-10 clear">

                                <a href="about.php" class="tr-3s"><i class="icon-info-circle-thin"></i> Haqqımızda</a>
                                <a href="contact.php" class="tr-3s"><i class="icon-envelope-o"></i> Bizimlə əlaqə</a>
                                <a href="ads.php" class="tr-3s"><i class="icon-ads"></i> Saytda reklam</a>
                                <a href="weather.php" class="tr-3s current-page"><i class="wi wi-day-cloudy"></i> Hava</a>
                                <a href="currency.php" class="tr-3s"><i class="icon-currency-azn"></i> Valyuta</a>

                            </div>

                        </div>

                        <div class="col-md-9 col-sm-12 col-xs-12 m-b-20">

                            <div class="static-page-wrapper shadow bg-white clear">

                                <h1 class="content-title p-20 m-0 border-bottom"><strong><i class="wi wi-day-cloudy"></i> HAVA PROQNOZU</strong></h1>

                                <div class="weather">

                                    <div class="header relative" style="background-image: url(http://www.travelwings.com/special-offers/images/Baku-1.jpg)">

                                        <div class="days row-0">

                                            <div class="day cunnet-day col-md-5 col-sm-5 col-xs-12">
                                                <div class="day-name p-10">
                                                    Bazar ertəsi
                                                </div>
                                                <div class="info-weather p-30">

                                                    <label class="city-name">Bakı</label>

                                                    <h2 class="deg"><b>28<sup>o</sup>C</b> <i class="wi wi-day-cloudy"></i></h2>

                                                    <span class="m-r-15"><i class="wi wi-moonrise"></i> 20 <sup>o</sup>C</span>
                                                    <span class="m-r-15"><i class="wi wi-strong-wind"></i> 11 m/s</span>
                                                    <span><i class="wi wi-humidity"></i> 40%</span>

                                                </div>

                                            </div>

                                            <div class="col-md-7 col-sm-7 col-xs-12">

                                                <div class="row-0">

                                                    <div class="day next-day col-md-2 col-sm-2 col-xs-4">

                                                        <div class="day-name p-10 text-center">Ç.a</div>

                                                        <div class="info-weather flex-center">

                                                            <span class="icon m-b-20"><i class="wi wi-day-cloudy"></i></span>

                                                            <strong class="deg m-b-20"><b>28<sup>o</sup>C</b></strong>

                                                            <span><i class="wi wi-moonrise"></i> 20 <sup>o</sup>C</span>

                                                        </div>

                                                    </div>

                                                    <div class="day next-day col-md-2 col-sm-2 col-xs-4">

                                                        <div class="day-name p-10 text-center">Ç</div>

                                                        <div class="info-weather flex-center">

                                                            <span class="icon m-b-20"><i class="wi wi-day-cloudy"></i></span>

                                                            <strong class="deg m-b-20"><b>28<sup>o</sup>C</b></strong>

                                                            <span><i class="wi wi-moonrise"></i> 20 <sup>o</sup>C</span>

                                                        </div>

                                                    </div>

                                                    <div class="day next-day col-md-2 col-sm-2 col-xs-4">

                                                        <div class="day-name p-10 text-center">C.a</div>

                                                        <div class="info-weather flex-center">

                                                            <span class="icon m-b-20"><i class="wi wi-day-cloudy"></i></span>

                                                            <strong class="deg m-b-20"><b>28<sup>o</sup>C</b></strong>

                                                            <span><i class="wi wi-moonrise"></i> 20 <sup>o</sup>C</span>

                                                        </div>

                                                    </div>

                                                    <div class="day next-day col-md-2 col-sm-2 col-xs-4">

                                                        <div class="day-name p-10 text-center">C</div>

                                                        <div class="info-weather flex-center">

                                                            <span class="icon m-b-20"><i class="wi wi-day-cloudy"></i></span>

                                                            <strong class="deg m-b-20"><b>28<sup>o</sup>C</b></strong>

                                                            <span><i class="wi wi-moonrise"></i> 20 <sup>o</sup>C</span>

                                                        </div>

                                                    </div>

                                                    <div class="day next-day col-md-2 col-sm-2 col-xs-4">

                                                        <div class="day-name p-10 text-center">Ş</div>

                                                        <div class="info-weather flex-center">

                                                            <span class="icon m-b-20"><i class="wi wi-day-cloudy"></i></span>

                                                            <strong class="deg m-b-20"><b>28<sup>o</sup>C</b></strong>

                                                            <span><i class="wi wi-moonrise"></i> 20 <sup>o</sup>C</span>

                                                        </div>

                                                    </div>

                                                    <div class="day next-day col-md-2 col-sm-2 col-xs-4">

                                                        <div class="day-name p-10 text-center">B</div>

                                                        <div class="info-weather flex-center">

                                                            <span class="icon m-b-20"><i class="wi wi-day-cloudy"></i></span>

                                                            <strong class="deg m-b-20"><b>28<sup>o</sup>C</b></strong>

                                                            <span><i class="wi wi-moonrise"></i> 20 <sup>o</sup>C</span>

                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="clear p-20">

                                        <h4 class="m-t-0 m-b-20"><b>Digər şəhərlər</b></h4>

                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover">

                                                <thead>

                                                <tr>
                                                    <th>Şəhər</th>
                                                    <th>Atmosfer hadisələri</th>
                                                    <th>Temperatur <sup>o</sup>C</th>
                                                    <th>Külək m/s</th>
                                                    <th>Rütubət %</th>
                                                </tr>

                                                </thead>

                                                <tbody>

                                                <tr>
                                                    <td><a href="#"><b>Bakı</b></a></td>
                                                    <td><i class="wi wi-day-cloudy"></i> Parçalı buludlu</td>
                                                    <td><b>25 / 20</b></td>
                                                    <td><i class="wi wi-strong-wind"></i> 45</td>
                                                    <td>40</td>
                                                </tr>
                                                <tr>
                                                    <td><a href="#"><b>Bakı</b></a></td>
                                                    <td><i class="wi wi-day-cloudy"></i> Parçalı buludlu</td>
                                                    <td><b>25 / 20</b></td>
                                                    <td><i class="wi wi-strong-wind"></i> 45</td>
                                                    <td>40</td>
                                                </tr>
                                                <tr>
                                                    <td><a href="#"><b>Bakı</b></a></td>
                                                    <td><i class="wi wi-day-cloudy"></i> Parçalı buludlu</td>
                                                    <td><b>25 / 20</b></td>
                                                    <td><i class="wi wi-strong-wind"></i> 45</td>
                                                    <td>40</td>
                                                </tr>
                                                <tr>
                                                    <td><a href="#"><b>Bakı</b></a></td>
                                                    <td><i class="wi wi-day-cloudy"></i> Parçalı buludlu</td>
                                                    <td><b>25 / 20</b></td>
                                                    <td><i class="wi wi-strong-wind"></i> 45</td>
                                                    <td>40</td>
                                                </tr>
                                                <tr>
                                                    <td><a href="#"><b>Bakı</b></a></td>
                                                    <td><i class="wi wi-day-cloudy"></i> Parçalı buludlu</td>
                                                    <td><b>25 / 20</b></td>
                                                    <td><i class="wi wi-strong-wind"></i> 45</td>
                                                    <td>40</td>
                                                </tr>
                                                <tr>
                                                    <td><a href="#"><b>Bakı</b></a></td>
                                                    <td><i class="wi wi-day-cloudy"></i> Parçalı buludlu</td>
                                                    <td><b>25 / 20</b></td>
                                                    <td><i class="wi wi-strong-wind"></i> 45</td>
                                                    <td>40</td>
                                                </tr>
                                                <tr>
                                                    <td><a href="#"><b>Bakı</b></a></td>
                                                    <td><i class="wi wi-day-cloudy"></i> Parçalı buludlu</td>
                                                    <td><b>25 / 20</b></td>
                                                    <td><i class="wi wi-strong-wind"></i> 45</td>
                                                    <td>40</td>
                                                </tr>
                                                <tr>
                                                    <td><a href="#"><b>Bakı</b></a></td>
                                                    <td><i class="wi wi-day-cloudy"></i> Parçalı buludlu</td>
                                                    <td><b>25 / 20</b></td>
                                                    <td><i class="wi wi-strong-wind"></i> 45</td>
                                                    <td>40</td>
                                                </tr>
                                                <tr>
                                                    <td><a href="#"><b>Bakı</b></a></td>
                                                    <td><i class="wi wi-day-cloudy"></i> Parçalı buludlu</td>
                                                    <td><b>25 / 20</b></td>
                                                    <td><i class="wi wi-strong-wind"></i> 45</td>
                                                    <td>40</td>
                                                </tr>
                                                <tr>
                                                    <td><a href="#"><b>Bakı</b></a></td>
                                                    <td><i class="wi wi-day-cloudy"></i> Parçalı buludlu</td>
                                                    <td><b>25 / 20</b></td>
                                                    <td><i class="wi wi-strong-wind"></i> 45</td>
                                                    <td>40</td>
                                                </tr>

                                                </tbody>

                                            </table>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<?php include("footer.php") ?>