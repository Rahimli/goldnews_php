<?php include("header.php") ?>

<div class="page-wrapper">

    <div class="container p-20">

        <div class="row-15">

            <main class="main-side col-md-12 col-sm-12 col-xs-12">

                <div class="news-list">

                    <nav class="bg-white shadow p-15 m-b-20">

                        <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="index.php" title="goldnews.az - Ana səhifə" itemprop="item">
                                    <span itemprop="name">Ana səhifə</span>
                                </a>
                                <meta itemprop="position" content="1">
                            </li>
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="category.php" title="Kateqoriya" itemprop="item">
                                    <span itemprop="name">Kateqoriya</span>
                                </a>
                                <meta itemprop="position" content="2">
                            </li>
                        </ol>

                    </nav>

                    <div class="row-7">

                        <div class="item-col col-md-3 col-sm-4 col-xs-12">

                            <div class="banner-100p shadow m-b-20">
                                <div class="content flex-center"></div>
                            </div>

                        </div>

                        <div class="item-col col-md-3 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>
                        <div class="item-col col-md-3 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>
                        <div class="item-col col-md-3 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>
                        <div class="item-col col-md-3 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>
                        <div class="item-col col-md-3 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>
                        <div class="item-col col-md-3 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>
                        <div class="item-col col-md-3 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>
                        <div class="item-col col-md-3 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>
                        <div class="item-col col-md-3 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>
                        <div class="item-col col-md-3 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>

                        <div class="item-col col-md-3 col-sm-4 col-xs-12">

                            <div class="banner-100p shadow m-b-20">
                                <div class="content flex-center"></div>
                            </div>

                        </div>

                        <div class="item-col col-md-3 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>
                        <div class="item-col col-md-3 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>
                        <div class="item-col col-md-3 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>
                        <div class="item-col col-md-3 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>
                        <div class="item-col col-md-3 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>
                        <div class="item-col col-md-3 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>
                        <div class="item-col col-md-3 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>
                        <div class="item-col col-md-3 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>

                        <div class="item-col col-md-3 col-sm-4 col-xs-12">

                            <div class="banner-100p shadow m-b-20">
                                <div class="content flex-center"></div>
                            </div>

                        </div>

                        <div class="item-col col-md-3 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>
                        <div class="item-col col-md-3 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>
                        <div class="item-col col-md-3 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>

                    </div>

                    <nav class="pagination-col bg-white shadow p-15 m-b-20">
                        <a href="#" class="btn br float-left transition-3s disabled hidden-xs">ƏVVƏLKİ</a>

                        <div class="clearfix">
                            <a href="#" class="active">1</a>
                            <a href="#">2</a>
                            <a href="#">3</a>
                            <a href="#">4</a>
                            <a href="#">5</a>
                            <a>…</a>
                            <a href="#">10</a>
                            <a href="#">11</a>
                        </div>

                        <a href="#" class="btn br float-right transition-3s hidden-xs">SONRAKI</a>
                    </nav>

                </div>

            </main>

        </div>

    </div>

</div>

<?php include("footer.php") ?>