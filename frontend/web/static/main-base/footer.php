
</section>

<div id="back-top" class="back-top">⇪</div>

<div class="overlay tr-3s"></div>

<div class="modal newsletter-modal" data-modal="newsletter-modal">
    <div class="flex-col flex-center">
        <div class="modal-content xs zoomIn">

            <span class="close" data-close="modal">✖</span>

            <div class="newsletter-cover"></div>

            <div class="bg-white p-20">
                <h4 class="newsletter-title m-t-0 m-b-20 text-center">Ən son xəbərləri ilk siz görün!</h4>

                <form action="newslatter.php">

                    <div class="input-group">

                        <input type="email" class="input" placeholder="E-poçt ünvanınızı yazın">

                        <span class="input-group-btn">
                            <button class="btn" type="submit"><i class="icon-send"></i></button>
                        </span>

                    </div>

                </form>

            </div>

        </div>
    </div>
</div>

<footer class="main-footer hidden-xs p-t-20 p-b-20 border-top clear" itemscope itemtype="http://schema.org/WPFooter">

    <div class="copyright col-md-4 col-sm-6 col-xs-12">
        <span class="m-r-15">GoldNews.az © <span itemprop="copyrightYear"></span></span>
        <span>Bütün haqqları qorunur</span>
    </div>

    <nav class="footer-menu col-md-4 hidden-sm text-center" rel="menu">

        <a href="about.php" title="Haqqımızda">Haqqımızda</a>
        <a href="contact.php" title="Əlaqə">Əlqə</a>
        <a href="ads.php" title="Saytda reklam">Saytda reklam</a>
        <a href="weather.php" title="Hava">Hava</a>
        <a href="currency.php" title="Valyuta">Valyuta</a>

    </nav>

    <div class="creator col-md-4 col-sm-6 text-right" itemprop="copyrightHolder" itemscope itemtype="http://schema.org/Organization">
        <a href="#" itemprop="url"><strong itemprop="name">Creator name</strong></a>
    </div>

</footer>

</body>
</html>