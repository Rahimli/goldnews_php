<?php include("header.php") ?>

<div class="page-wrapper">

    <div class="container p-20">

        <div class="row-15">

            <main class="main-side col-md-9 col-sm-12 col-xs-12 float-left" data-main-col="1">

                <nav class="bg-white shadow border-bottom p-15 m-b-20">

                    <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">

                        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <a href="index.php" title="goldnews.az - Ana səhifə" itemprop="item">
                                <span itemprop="name">Ana səhifə</span>
                            </a>
                            <meta itemprop="position" content="1">
                        </li>

                        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <a href="category.php" title="Kateqoriya" itemprop="item">
                                <span itemprop="name">kateqoriya</span>
                            </a>
                            <meta itemprop="position" content="2">
                        </li>

                        <li class="hidden-xs" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <a href="read.php" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit." itemprop="item">
                                <span itemprop="name">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
                            </a>
                            <meta itemprop="position" content="3">
                        </li>

                    </ol>
                </nav>

                <article class="article shadow bg-white m-b-20 clear" itemscope itemtype="http://schema.org/NewsArticle">

                    <div class="article-right-side" itemprop="mainEntityOfPage">

                        <header class="article-header">

                            <figure class="thumb" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">

                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="850" height="500">

                                <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                <meta itemprop="width" content="850">
                                <meta itemprop="height" content="500">

                            </figure>

                            <div class="article-info p-r-20">

                                <span class="m-r-15"><i class="icon-play-thin"></i></span>

                                <a href="category.php" class="m-r-15" title="Kateqoriya" itemprop="genre">Kateqoriya</a>

                                <span><i class="icon-eye"></i> 324</span>

                            </div>

                            <h1 class="title m-t-20 m-b-20" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h1>

                            <h2 class="description m-t-20 m-b-20" itemprop="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Accusantium aliquam aut cumque excepturi fugit id maxime minima nam non tempora.
                                Animi dolore impedit minus natus nobis officiis recusandae sapiente voluptates!</h2>

                        </header>

                        <div class="article-body m-b-20" itemprop="articleBody">

                            <div class="article-content">

                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Accusantium aliquam aut cumque excepturi fugit id maxime minima nam non tempora.
                                    Animi dolore impedit minus natus nobis officiis recusandae sapiente voluptates!</p>

                                <p><img src="assets/images/bg_weather.jpg" alt="alt" width="850" height="600"></p>

                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Accusantium aliquam aut cumque excepturi fugit id maxime minima nam non tempora.
                                    Animi dolore impedit minus natus nobis officiis recusandae sapiente voluptates!</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Accusantium aliquam aut cumque excepturi fugit id maxime minima nam non tempora.
                                    Animi dolore impedit minus natus nobis officiis recusandae sapiente voluptates!</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Accusantium aliquam aut cumque excepturi fugit id maxime minima nam non tempora.
                                    Animi dolore impedit minus natus nobis officiis recusandae sapiente voluptates!</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Accusantium aliquam aut cumque excepturi fugit id maxime minima nam non tempora.
                                    Animi dolore impedit minus natus nobis officiis recusandae sapiente voluptates!</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Accusantium aliquam aut cumque excepturi fugit id maxime minima nam non tempora.
                                    Animi dolore impedit minus natus nobis officiis recusandae sapiente voluptates!</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Accusantium aliquam aut cumque excepturi fugit id maxime minima nam non tempora.
                                    Animi dolore impedit minus natus nobis officiis recusandae sapiente voluptates!</p>

                            </div>

                        </div>

                        <footer class="article-footer p-t-20 p-b-20">

                            <div class="article-tags clear m-b-20" itemprop="keywords">
                                <span><i class="icon-tags"></i> Açar sözlər:</span>
                                <a href="#" class="tr-3s" rel="tag">#tag name</a>
                                <a href="#" class="tr-3s" rel="tag">#tag name</a>
                                <a href="#" class="tr-3s" rel="tag">#tag name</a>
                            </div>

                            <div class="bg-gray p-10 clear">

                                <div class="row-7">

                                    <div class="article-author col-md-6 col-sm-6 col-xs-12 clear" itemprop="publisher" itemscope itemtype="https://schema.org/Organization">

                                        <div class="thumb" itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                            <img src="assets/images/favicon.png" alt="user image" width="400" height="400">

                                            <link itemprop="url" href="https://goldnews.az/images/favicon.png">
                                            <meta itemprop="width" content="400">
                                            <meta itemprop="height" content="400">

                                        </div>

                                        <strong class="name" itemprop="name">Goldnews.az</strong>

                                    </div>

                                    <div class="col-md-4 col-sm-5 col-xs-12 clear float-right">
                                        <button class="btn btn-block btn-lg bg-info follow-button" data-target-modal="newsletter-modal">Abonə olun</button>
                                    </div>

                                </div>

                            </div>

                            <meta itemprop="author" content="Eldar Zeynallı">
                            <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                            <meta itemprop="isFamilyFriendly" content="true">

                        </footer>

                    </div>

                    <div class="article-left-side p-20 p-b-15">
                        <div class="fixed-side">
                            <div class="share-button clear">
                                <label class="m-b-15 hidden-sm hidden-xs"><i class="icon-share"></i> Paylaş</label>
                                <a href="#" class="fb tr-3s"><i class="icon-social-facebook"></i></a>
                                <a href="#" class="tw tr-3s"><i class="icon-social-twitter"></i></a>
                                <a href="#" class="gp tr-3s"><i class="icon-social-google-plus"></i></a>
                                <a href="#" class="wp tr-3s visible-xs"><i class="icon-social-whatsapp"></i></a>
                                <a href="#" class="email tr-3s"><i class="icon-envelope"></i></a>
                                <a href="#" class="print tr-3s hidden-xs"><i class="icon-print-1"></i></a>
                                <a href="#" class="rss tr-3s hidden-xs"><i class="icon-rss"></i></a>
                            </div>
                        </div>
                    </div>

                </article>

                <div class="comment-panel bg-white shadow m-b-20">

                    <h2 class="content-title p-15 m-0 border-bottom"><i class="icon-comment"></i> Xəbərlə bağlı rəy bildirin.</h2>

                    <div class="p-20">
                        <img src="assets/images/comment.jpg" alt="" style="width:100%;">
                    </div>

                </div>

                <div class="news-list">

                    <h2 class="content-title shadow bg-white p-15 m-t-0 m-b-20">
                        <a href="category.php" title="Digər foto xəbərlər">
                            <i class="icon-newspaper-2"></i>
                            Bənzər xəbərlər
                            <i class="icon-circle-right float-right"></i>
                        </a>
                    </h2>

                    <div class="row-7">

                        <div class="item-col col-md-4 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>
                        <div class="item-col col-md-4 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>
                        <div class="item-col col-md-4 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>
                        <div class="item-col col-md-4 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>
                        <div class="item-col col-md-4 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>
                        <div class="item-col col-md-4 col-sm-4 col-xs-12">

                            <article class="item bg-white shadow m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                <a href="read.php" title="title" itemprop="url">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="640" height="360">
                                        <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                        <meta itemprop="width" content="640">
                                        <meta itemprop="height" content="360">
                                    </div>

                                    <div class="info p-15">

                                        <div class="meta-data line-camp line-1">
                                            <i class="icon-photo-gallery type m-r-5"></i>

                                            <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun, 16:40</time>
                                        </div>

                                        <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, eligendi</h3>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Eldar Zeynallı">
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                        <meta itemprop="isFamilyFriendly" content="true">

                                    </div>

                                </a>

                            </article>

                        </div>

                    </div>

                    <a href="category.php" class="show-more shadow bg-white text-center block p-15 m-b-20">Daha çox göstər</a>

                </div>

            </main>

            <aside class="col-md-3 col-sm-12 col-xs-12" data-col="1" itemscope itemtype="http://schema.org/WPSideBar">

                <div class="fixed-wrap">

                    <div class="row-10">

                        <div class="col-lg-12 col-md-12 col-sm-4 hidden-xs m-b-20">
                            <div class="banner-100p shadow">
                                <div class="content flex-center"></div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 hidden-sm hidden-xs">

                            <div class="banner-20p m-b-10 shadow visible-xs">
                                <div class="content flex-center"></div>
                            </div>

                            <div class="time-line m-b-20 shadow border-bottom clear">

                                <h2 class="content-title m-0 p-15 border-bottom text-uppercase"><i class="icon-newspaper-2"></i> Xəbər lenti</h2>

                                <div class="content scrl">

                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-photo"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>

                                    <div class="item border-bottom visible-xs clear">
                                        <div class="banner-20p">
                                            <div class="content flex-center"></div>
                                        </div>
                                    </div>

                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>

                                    <div class="item border-bottom visible-xs clear">
                                        <div class="banner-20p">
                                            <div class="content flex-center"></div>
                                        </div>
                                    </div>

                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>

                                    <div class="item border-bottom visible-xs clear">
                                        <div class="banner-20p">
                                            <div class="content flex-center"></div>
                                        </div>
                                    </div>

                                    <a href="#" class="item block text-center bg-gray clear">Daha çox göstər</a>

                                </div>

                            </div>

                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-4 hidden-xs m-b-20">
                            <div class="banner-100p shadow">
                                <div class="content flex-center"></div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-4 hidden-xs">
                            <div class="banner-100p shadow">
                                <div class="content flex-center"></div>
                            </div>
                        </div>

                    </div>

                </div>

            </aside>

        </div>

    </div>

</div>

<?php include("footer.php") ?>