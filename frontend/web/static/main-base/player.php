<?php include("header.php") ?>

<div class="page-wrapper">

    <div class="gallery-post-col shadow relative clear">

        <div class="top-wrapper clear p-t-20">

            <nav class="clear">

                <ol class="breadcrumb white m-b-15" itemscope itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <a href="index.php" title="goldnews.az - Ana səhifə" itemprop="item">
                            <span itemprop="name">Ana səhifə</span>
                        </a>
                        <meta itemprop="position" content="1">
                    </li>
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <a href="videos.php" title="Video xəbərlər" itemprop="item">
                            <span itemprop="name">Video xəbərlər</span>
                        </a>
                        <meta itemprop="position" content="2">
                    </li>
                    <li class="hidden-xs" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <a href="player.php" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit." itemprop="item">
                            <span itemprop="name">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
                        </a>
                        <meta itemprop="position" content="3">
                    </li>
                </ol>

            </nav>

            <div class="row-0 m-b-30" itemscope itemtype="http://schema.org/VideoObject">

                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" allowfullscreen src="http://www.youtube.com/embed/s43o5vLk3jQ" itemprop="embedUrl"></iframe>
                    </div>
                </div>

                <div class="info col-md-4 col-sm-12 col-xs-12">

                    <div class="p-20">

                        <h1 class="title m-t-0 m-b-10" itemprop="name">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h1>

                        <div class="description" itemprop="description">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                A ab atque consequuntur deserunt ea earum eligendi error illum minus molestiae mollitia nisi quasi,
                                recusandae rerum similique suscipit tempora vero voluptatum.</p>
                        </div>

                        <div class="clear m-b-10">
                            <span class="m-r-15">Kateqoriya</span>
                            <time datetime="2017-07-02T14:00" class="m-r-15">2 İyul</time>
                            <span><i class="icon-eye"></i> 3254</span>
                        </div>

                        <div class="gallery-tags clear m-b-15 p-t-15" itemprop="keywords">
                            <span class="hidden-xs"><i class="icon-tags"></i> Açar sözlər:</span>
                            <a href="#" class="tr-3s" rel="tag">#tag name</a>
                            <a href="#" class="tr-3s" rel="tag">#tag name</a>
                            <a href="#" class="tr-3s" rel="tag">#tag name</a>
                        </div>

                        <div class="share-button clear">
                            <a href="#" class="fb tr-3s"><i class="icon-social-facebook"></i></a>
                            <a href="#" class="tw tr-3s"><i class="icon-social-twitter"></i></a>
                            <a href="#" class="gp tr-3s"><i class="icon-social-google-plus"></i></a>
                            <a href="#" class="wp tr-3s visible-xs"><i class="icon-social-whatsapp"></i></a>
                            <a href="#" class="email tr-3s"><i class="icon-envelope"></i></a>
                            <a href="#" class="rss tr-3s hidden-xs"><i class="icon-rss"></i></a>
                        </div>

                        <link itemprop="thumbnailUrl" href="http://img.youtube.com/vi/s43o5vLk3jQ/0.jpg">
                        <meta itemprop="isFamilyFriendly" content="true">
                        <meta itemprop="datePublished" content="2017-01-01T12:40">
                        <meta itemprop="uploadDate" content="2017-01-01T12:40">
                        <meta itemprop="interactionCount" content="2347" />
                        <meta itemprop="duration" content="PT6M58S">

                    </div>

                </div>

            </div>
            
        </div>

        <div class="next-items-carousel owl-carousel owl-theme m-b-30">

            <div class="item">
                <a href="player.php" title="title">
                    <div class="thumb tr-3s">
                        <i class="icon-player-play"></i>
                        <img src="http://img.youtube.com/vi/s43o5vLk3jQ/0.jpg" alt="alt" width="" height="">
                    </div>
                </a>
            </div>

        </div>

    </div>

    <div class="container p-20">

        <div class="row-15">

            <main class="main-side col-md-9 col-sm-12 col-xs-12" data-main-col="1">

                <div class="comment-panel shadow bg-white m-b-20">

                    <h2 class="content-title p-15 m-0 border-bottom"><i class="icon-comment"></i> Xəbərlə bağlı rəy bildirin.</h2>

                    <div class="p-20">
                        <img src="assets/images/comment.jpg" alt="" style="width:100%;">
                    </div>

                </div>

                <div class="gallery-list">

                    <h2 class="content-title shadow bg-white p-15 m-t-0 m-b-20">
                        <a href="videos.php" title="Digər video xəbərlər">
                            <i class="icon-play-thin"></i>
                            Digər video xəbərlər
                            <i class="icon-circle-right float-right"></i>
                        </a>
                    </h2>

                    <div class="row-10">

                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <div class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/VideoObject">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/videos/video-url">

                                <a href="player.php" title="title" itemprop="url">

                                    <div class="thumb hover">
                                        <i class="icon-player-play"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300" itemprop="thumbnailUrl">
                                    </div>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="name">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-play-thin"></i></span>

                                        <span class="m-r-15">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40">26 İyun</time>

                                    </div>

                                </a>

                                <meta itemprop="description" content="description text">

                                <meta itemprop="isFamilyFriendly" content="true">
                                <meta itemprop="datePublished" content="2017-01-01T12:40">
                                <meta itemprop="uploadDate" content="2017-01-01T12:40">
                                <meta itemprop="interactionCount" content="2347" />
                                <meta itemprop="duration" content="PT6M58S">

                            </div>

                        </div>
                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <div class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/VideoObject">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/videos/video-url">

                                <a href="player.php" title="title" itemprop="url">

                                    <div class="thumb hover">
                                        <i class="icon-player-play"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300" itemprop="thumbnailUrl">
                                    </div>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="name">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-play-thin"></i></span>

                                        <span class="m-r-15">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40">26 İyun</time>

                                    </div>

                                </a>

                                <meta itemprop="description" content="description text">

                                <meta itemprop="isFamilyFriendly" content="true">
                                <meta itemprop="datePublished" content="2017-01-01T12:40">
                                <meta itemprop="uploadDate" content="2017-01-01T12:40">
                                <meta itemprop="interactionCount" content="2347" />
                                <meta itemprop="duration" content="PT6M58S">

                            </div>

                        </div>
                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <div class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/VideoObject">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/videos/video-url">

                                <a href="player.php" title="title" itemprop="url">

                                    <div class="thumb hover">
                                        <i class="icon-player-play"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300" itemprop="thumbnailUrl">
                                    </div>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="name">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-play-thin"></i></span>

                                        <span class="m-r-15">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40">26 İyun</time>

                                    </div>

                                </a>

                                <meta itemprop="description" content="description text">

                                <meta itemprop="isFamilyFriendly" content="true">
                                <meta itemprop="datePublished" content="2017-01-01T12:40">
                                <meta itemprop="uploadDate" content="2017-01-01T12:40">
                                <meta itemprop="interactionCount" content="2347" />
                                <meta itemprop="duration" content="PT6M58S">

                            </div>

                        </div>
                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <div class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/VideoObject">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/videos/video-url">

                                <a href="player.php" title="title" itemprop="url">

                                    <div class="thumb hover">
                                        <i class="icon-player-play"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300" itemprop="thumbnailUrl">
                                    </div>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="name">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-play-thin"></i></span>

                                        <span class="m-r-15">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40">26 İyun</time>

                                    </div>

                                </a>

                                <meta itemprop="description" content="description text">

                                <meta itemprop="isFamilyFriendly" content="true">
                                <meta itemprop="datePublished" content="2017-01-01T12:40">
                                <meta itemprop="uploadDate" content="2017-01-01T12:40">
                                <meta itemprop="interactionCount" content="2347" />
                                <meta itemprop="duration" content="PT6M58S">

                            </div>

                        </div>
                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <div class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/VideoObject">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/videos/video-url">

                                <a href="player.php" title="title" itemprop="url">

                                    <div class="thumb hover">
                                        <i class="icon-player-play"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300" itemprop="thumbnailUrl">
                                    </div>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="name">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-play-thin"></i></span>

                                        <span class="m-r-15">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40">26 İyun</time>

                                    </div>

                                </a>

                                <meta itemprop="description" content="description text">

                                <meta itemprop="isFamilyFriendly" content="true">
                                <meta itemprop="datePublished" content="2017-01-01T12:40">
                                <meta itemprop="uploadDate" content="2017-01-01T12:40">
                                <meta itemprop="interactionCount" content="2347" />
                                <meta itemprop="duration" content="PT6M58S">

                            </div>

                        </div>
                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <div class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/VideoObject">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/videos/video-url">

                                <a href="player.php" title="title" itemprop="url">

                                    <div class="thumb hover">
                                        <i class="icon-player-play"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300" itemprop="thumbnailUrl">
                                    </div>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="name">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-play-thin"></i></span>

                                        <span class="m-r-15">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40">26 İyun</time>

                                    </div>

                                </a>

                                <meta itemprop="description" content="description text">

                                <meta itemprop="isFamilyFriendly" content="true">
                                <meta itemprop="datePublished" content="2017-01-01T12:40">
                                <meta itemprop="uploadDate" content="2017-01-01T12:40">
                                <meta itemprop="interactionCount" content="2347" />
                                <meta itemprop="duration" content="PT6M58S">

                            </div>

                        </div>
                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <div class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/VideoObject">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/videos/video-url">

                                <a href="player.php" title="title" itemprop="url">

                                    <div class="thumb hover">
                                        <i class="icon-player-play"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300" itemprop="thumbnailUrl">
                                    </div>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="name">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-play-thin"></i></span>

                                        <span class="m-r-15">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40">26 İyun</time>

                                    </div>

                                </a>

                                <meta itemprop="description" content="description text">

                                <meta itemprop="isFamilyFriendly" content="true">
                                <meta itemprop="datePublished" content="2017-01-01T12:40">
                                <meta itemprop="uploadDate" content="2017-01-01T12:40">
                                <meta itemprop="interactionCount" content="2347" />
                                <meta itemprop="duration" content="PT6M58S">

                            </div>

                        </div>
                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <div class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/VideoObject">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/videos/video-url">

                                <a href="player.php" title="title" itemprop="url">

                                    <div class="thumb hover">
                                        <i class="icon-player-play"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300" itemprop="thumbnailUrl">
                                    </div>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="name">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-play-thin"></i></span>

                                        <span class="m-r-15">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40">26 İyun</time>

                                    </div>

                                </a>

                                <meta itemprop="description" content="description text">

                                <meta itemprop="isFamilyFriendly" content="true">
                                <meta itemprop="datePublished" content="2017-01-01T12:40">
                                <meta itemprop="uploadDate" content="2017-01-01T12:40">
                                <meta itemprop="interactionCount" content="2347" />
                                <meta itemprop="duration" content="PT6M58S">

                            </div>

                        </div>
                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <div class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/VideoObject">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/videos/video-url">

                                <a href="player.php" title="title" itemprop="url">

                                    <div class="thumb hover">
                                        <i class="icon-player-play"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300" itemprop="thumbnailUrl">
                                    </div>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="name">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-play-thin"></i></span>

                                        <span class="m-r-15">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40">26 İyun</time>

                                    </div>

                                </a>

                                <meta itemprop="description" content="description text">

                                <meta itemprop="isFamilyFriendly" content="true">
                                <meta itemprop="datePublished" content="2017-01-01T12:40">
                                <meta itemprop="uploadDate" content="2017-01-01T12:40">
                                <meta itemprop="interactionCount" content="2347" />
                                <meta itemprop="duration" content="PT6M58S">

                            </div>

                        </div>
                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <div class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/VideoObject">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/videos/video-url">

                                <a href="player.php" title="title" itemprop="url">

                                    <div class="thumb hover">
                                        <i class="icon-player-play"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300" itemprop="thumbnailUrl">
                                    </div>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="name">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-play-thin"></i></span>

                                        <span class="m-r-15">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40">26 İyun</time>

                                    </div>

                                </a>

                                <meta itemprop="description" content="description text">

                                <meta itemprop="isFamilyFriendly" content="true">
                                <meta itemprop="datePublished" content="2017-01-01T12:40">
                                <meta itemprop="uploadDate" content="2017-01-01T12:40">
                                <meta itemprop="interactionCount" content="2347" />
                                <meta itemprop="duration" content="PT6M58S">

                            </div>

                        </div>
                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <div class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/VideoObject">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/videos/video-url">

                                <a href="player.php" title="title" itemprop="url">

                                    <div class="thumb hover">
                                        <i class="icon-player-play"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300" itemprop="thumbnailUrl">
                                    </div>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="name">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-play-thin"></i></span>

                                        <span class="m-r-15">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40">26 İyun</time>

                                    </div>

                                </a>

                                <meta itemprop="description" content="description text">

                                <meta itemprop="isFamilyFriendly" content="true">
                                <meta itemprop="datePublished" content="2017-01-01T12:40">
                                <meta itemprop="uploadDate" content="2017-01-01T12:40">
                                <meta itemprop="interactionCount" content="2347" />
                                <meta itemprop="duration" content="PT6M58S">

                            </div>

                        </div>
                        <div class="item-col col-md-6 col-sm-6 col-xs-12">

                            <div class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/VideoObject">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/videos/video-url">

                                <a href="player.php" title="title" itemprop="url">

                                    <div class="thumb hover">
                                        <i class="icon-player-play"></i>
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="500" height="300" itemprop="thumbnailUrl">
                                    </div>

                                    <div class="caption caption-bg p-15">

                                        <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="name">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                                        <span class="m-r-15">2 <i class="icon-play-thin"></i></span>

                                        <span class="m-r-15">Kateqoriya</span>

                                        <time datetime="2017-06-26T16:40">26 İyun</time>

                                    </div>

                                </a>

                                <meta itemprop="description" content="description text">

                                <meta itemprop="isFamilyFriendly" content="true">
                                <meta itemprop="datePublished" content="2017-01-01T12:40">
                                <meta itemprop="uploadDate" content="2017-01-01T12:40">
                                <meta itemprop="interactionCount" content="2347" />
                                <meta itemprop="duration" content="PT6M58S">

                            </div>

                        </div>

                    </div>

                    <a href="videos.php" class="show-more shadow bg-white text-center block p-15 m-b-20">Daha çox göstər</a>

                </div>

            </main>

            <aside class="col-md-3 col-sm-12 col-xs-12" data-col="1" itemscope itemtype="http://schema.org/WPSideBar">

                <div class="fixed-wrap">

                    <div class="row-10">

                        <div class="col-lg-12 col-md-12 col-sm-4 hidden-xs m-b-20">
                            <div class="banner-100p shadow">
                                <div class="content flex-center"></div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 hidden-sm hidden-xs">

                            <div class="banner-20p m-b-10 shadow visible-xs">
                                <div class="content flex-center"></div>
                            </div>

                            <div class="time-line m-b-20 shadow border-bottom clear">

                                <h2 class="content-title m-0 p-15 border-bottom text-uppercase"><i class="icon-newspaper-2"></i> Xəbər lenti</h2>

                                <div class="content scrl">

                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-photo"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>

                                    <div class="item border-bottom visible-xs clear">
                                        <div class="banner-20p">
                                            <div class="content flex-center"></div>
                                        </div>
                                    </div>

                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>

                                    <div class="item border-bottom visible-xs clear">
                                        <div class="banner-20p">
                                            <div class="content flex-center"></div>
                                        </div>
                                    </div>

                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>

                                    <div class="item border-bottom visible-xs clear">
                                        <div class="banner-20p">
                                            <div class="content flex-center"></div>
                                        </div>
                                    </div>

                                    <a href="#" class="item block text-center bg-gray clear">Daha çox göstər</a>

                                </div>

                            </div>

                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-4 hidden-xs m-b-20">
                            <div class="banner-100p shadow">
                                <div class="content flex-center"></div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-4 hidden-xs">
                            <div class="banner-100p shadow">
                                <div class="content flex-center"></div>
                            </div>
                        </div>

                    </div>

                </div>

            </aside>

        </div>

    </div>

</div>

<?php include("footer.php") ?>