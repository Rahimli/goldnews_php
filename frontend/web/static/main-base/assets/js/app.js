$(document).ready(function () {

    /*====================================================================================================================*/
    /*====================================================================================================================*/

    if ($(window).width() > 991) {

        var banner_wrapper =  $('.banner_wrapper');
        var fixed_margin =  $('.fixed-margin');
        var main_nav =  $('.main-nav');
        var ost = 0;

        $(window).scroll(function () {

            if ($(window).scrollTop() > banner_wrapper.height()) {
                $('body').addClass('nav-fixed');
                fixed_margin.css('margin-bottom', 115)
            }
            else {
                $('body').removeClass('nav-fixed');
                fixed_margin.css('margin-bottom', '')
            }


            var cOst = $(this).scrollTop();

            if (cOst > ost && $(window).scrollTop() > banner_wrapper.height() + main_nav.height()) {

                $('.main-nav').addClass('up');
            }
            else {
                $('.main-nav').removeClass('up');
            }

            ost = cOst;

        });

     }

    /*====================================================================================================================*/
    /*====================================================================================================================*/

    var body = $('body');

    $('.nav-toggle').on('click', function () {
        body.toggleClass('nav-shown');
    });

    /*====================================================================================================================*/
    /*====================================================================================================================*/

    $('.search-toggle').on('click', function () {

        body.toggleClass('search-shown');

        if($(window).width() < 992){
            $('.search-box input').focus();
        }

    });

    $('.search-close').on('click', function () {
        body.removeClass('search-shown');
    });

    /*====================================================================================================================*/
    /*====================================================================================================================*/

    $('.side-toggle').on('click', function () {
        body.toggleClass('side-shown');
    });

    /*====================================================================================================================*/
    /*====================================================================================================================*/

    $('*').click(function (e) {

        if (!$(e.target).is('.nav-toggle') && !$(e.target).is('.nav-toggle *') && !$(e.target).is('.main-nav') && !$(e.target).is('.main-nav *')) {
            body.removeClass('nav-shown');
        }

        if (!$(e.target).is('.search-toggle') && !$(e.target).is('.search-toggle *') && !$(e.target).is('.search-box') && !$(e.target).is('.search-box *')) {
            body.removeClass('search-shown');
        }

        if (!$(e.target).is('.side-toggle') && !$(e.target).is('.side-toggle *') && !$(e.target).is('.right-side') && !$(e.target).is('.right-side *')) {
            body.removeClass('side-shown');
        }

    });

    /*====================================================================================================================*/
    /*====================================================================================================================*/



    /*====================================================================================================================*/
    /*====================================================================================================================*/

    var right_side = $('.page-wrapper .right-side');
    var left_side = $('.page-wrapper .static-wrapper');

    var main_header = $('.main-header');

    $(window).scroll(function () {

        if ($(window).width() > 991) {

            $('[data-col]').each(function () {

                var $attr = $(this).attr('data-col');

                $(this).css('min-height', $('[data-main-col="'+ $attr +'"]').height() - 20);

            });

            right_side.css('min-height', left_side.height());

            $('.fixed-wrap').stick_in_parent({offset_top: 85});

        }

        if ($(window).width() > 767) {

            var article_right_side = $('.article .article-right-side');
            var article_left_side = $('.article .article-left-side');

            article_left_side.css('min-height', article_right_side.height());

            $('.article .fixed-side').stick_in_parent({offset_top: 75});

        }

        if($(window).width() < 768){

            var article = $('.article-right-side');

            if ($(this).scrollTop() < article.height() - 430) {
                $('.article-left-side').addClass('fixed');
            } else {
                $('.article-left-side').removeClass('fixed');
            }

        }

    });

    /*====================================================================================================================*/
    /*====================================================================================================================*/

    $('.article-content iframe').each(function () {
        $(this).parent().attr('class', 'embed-responsive embed-responsive-16by9');
        $(this).attr('class', 'embed-responsive-item');
    });

    /*====================================================================================================================*/
    /*====================================================================================================================*/

    $('.weather_currency .carousel').owlCarousel({
        autoplay: true,
        autoplayTimeout: 4000,
        items: 1,
        dots: false,
        nav: false,
        navText:false
    });

    /*====================================================================================================================*/
    /*====================================================================================================================*/

    $('.main-video-slider').owlCarousel({
        loop: true,
        autoplay: true,
        autoplayTimeout: 4000,
        dots: false,
        nav: true,
        navText: ["❮", "❯"],
        responsive: {
            0: {
                items: 1
            },
            500: {
                items: 2
            },
            769: {
                items: 3
            }
        }
    });

    /*====================================================================================================================*/
    /*====================================================================================================================*/

    $('.next-items-carousel').owlCarousel({
        loop: true,
        dots: false,
        center: true,
        nav: true,
        navText: ["❮", "❯"],
        responsive: {
            0: {
                items: 2,
                margin:5
            },
            500: {
                items: 3,
                margin:5
            },
            769: {
                items: 5,
                margin:10
            }
        }
    });

    /*====================================================================================================================*/
    /*====================================================================================================================*/

    $(".back-top").hide();

    $(window).scroll(function () {
        if ($(this).scrollTop() > 400) {
            $('.back-top').fadeIn();
            $('.onesignal').addClass('up');
        } else {
            $('.back-top').fadeOut();
            $('.onesignal').removeClass('up');
        }
    });

    $('.back-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });

    /*====================================================================================================================*/
    /*====================================================================================================================*/

    // Dropdown
    $('.drop.select').each(function () {
        $(this).find('.drop-toggle').html($(this).find('.drop-menu a.selected span').html());
    });

    $('.drop').on("click", function () {

        if ($(this).hasClass('open')) {
            $('.drop').removeClass('open');
        }
        else {
            $('.drop').removeClass('open');
            $(this).addClass('open');
        }

        if ($('.drop').hasClass('no-url')) {
            return false;
        }

    });

    $('.drop.select .drop-menu a').on("click", function () {
        $(this).parent().parent().find('.drop-menu a').removeClass('selected');
        $(this).addClass('selected');
        $(this).parent().parent().find('.drop-toggle').html($(this).find('span').html());
    });

    $('*').click(function (e) {
        if (!$(e.target).is('.drop') && !$(e.target).is('.drop *')) {
            $('.drop').removeClass('open');
        }
    });

    /*================================================================================================================*/
    /*================================================================================================================*/

    // Tab
    $('.tab').each(function () {

        var $this = $(this);

        if ($this.find('.tab-nav').hasClass('hover')) {
            $this.find($('a[data-target="tab"]')).hover(function (e) {
                var currentAttrValue = $(this).attr('href');
                $('.tab ' + currentAttrValue).show().siblings('.tab-panel').hide();
                $(this).parent('li').addClass('active').siblings().removeClass('active');
                e.preventDefault();
                return false;
            });
        }
        else {
            $this.find($('a[data-target="tab"]')).click(function (e) {
                var currentAttrValue = $(this).attr('href');
                $('.tab ' + currentAttrValue).show().siblings('.tab-panel').hide();
                $(this).parent('li').addClass('active').siblings().removeClass('active');
                e.preventDefault();
            });
        }
    });

    /*====================================================================================================================*/
    /*====================================================================================================================*/

    // Modal
    $('[data-target-modal]').each(function () {

        var $this = $(this);
        var target_modal = $this.attr('data-target-modal');

        var modal = $('[data-modal="' + target_modal + '"]');

        $this.on('click', function () {
            modal.addClass('open');
        });

        modal.find('[data-close="modal"]').on('click', function () {
            modal.removeClass('open');
        });

    });

    /*====================================================================================================================*/
    /*====================================================================================================================*/
    /*====================================================================================================================*/
    /*====================================================================================================================*/
    /*====================================================================================================================*/
    /*====================================================================================================================*/
    /*====================================================================================================================*/

        var ready = true;
        var pageNo = 1;
        var IsPageLoad = true;
        // var ajaxSpinnerMain = $('#ajax-spinner-main');
        var page_count_limit =  10 ;
        var scrool_div = $('.scrl');
        var scrl_div_loader = $('#scrl-div-loader');
        var ajax_newsletter_url = $('#ajax-newsletter-url');
        scrool_div.scroll(function () {
            // alert('salam');
            console.log(page_count_limit + ' - ' + pageNo);
            var isNextPageAvailable = pageNo <= page_count_limit;
//            {#                console.log(isNextPageAvailable + ' - ' + pageNo + ' - ' + page_count_limit);
            if(($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight)){

// alert('asasa');
                if (ready && isNextPageAvailable) {
                    ready = false;
                    if (IsPageLoad) {
                        scrl_div_loader.show();
                        // ajaxSpinnerMain.append('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>');
                        setTimeout(function () {
                            $.ajax({
                                dataType: "json",
                                type: "GET",
                                url: ajax_newsletter_url.val(),
                                data: {page: pageNo},
                                success: function (data) {
                                    scrl_div_loader.hide();
                                    ready = true;
                                    // alert('succcess');
                                    if (data['message_code'] == 1) {
                                        scrool_div.append(data['content']);
                                        pageNo++;
                                        // alert('if');
                                    }
                                    else {
                                        IsPageLoad = false;
                                        ready = false;
                                    }

                                    // ajaxSpinnerMain.empty();
                                },
                                error: function (data) {
                                    scrl_div_loader.hide();
                                    // if ($.trim(data['message_code']) == 0 || $.trim(data) == '') {
                                    //     IsPageLoad = false;
                                    //     ready = false;
                                    // }
                                    ready = true;
                                    // ajaxSpinnerMain.empty();
                                    console.log('Error !!!');
                                }
                            });
                        }, 1000);

                    }

                }
            }

        });

    /*====================================================================================================================*/

});
