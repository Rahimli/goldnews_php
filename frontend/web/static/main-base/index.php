<?php include("header.php") ?>

<div class="main-wrapper">

    <div class="container p-20">

        <div class="row-15">

            <main class="col-md-9 col-sm-12 hidden-xs" rel="main" data-main-col="1">

                <div class="news-section style-top m-b-20">

                    <div class="row-7">

                        <div class="item-col m-b-15 col-md-3 col-sm-3 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo m-r-5"></i>

                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <p class="description line-camp line-4 hidden-xs" itemprop="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            Accusamus at aut blanditiis, corporis dolorem doloremque doloribus excepturi fugiat
                                            impedit iusto molestias nobis officiis porro quae quaerat quod tempore temporibus voluptates?</p>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00:00+08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20:00+08:00"/>

                                    </div>
                                </a>
                            </article>
                        </div>

                        <div class="item-col m-b-15 col-md-3 col-sm-3 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo m-r-5"></i>

                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <p class="description line-camp line-4 hidden-xs" itemprop="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            Accusamus at aut blanditiis, corporis dolorem doloremque doloribus excepturi fugiat
                                            impedit iusto molestias nobis officiis porro quae quaerat quod tempore temporibus voluptates?</p>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00:00+08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20:00+08:00"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-15 col-md-3 col-sm-3 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo m-r-5"></i>

                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <p class="description line-camp line-4 hidden-xs" itemprop="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            Accusamus at aut blanditiis, corporis dolorem doloremque doloribus excepturi fugiat
                                            impedit iusto molestias nobis officiis porro quae quaerat quod tempore temporibus voluptates?</p>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00:00+08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20:00+08:00"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-15 col-md-3 col-sm-3 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo m-r-5"></i>

                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <p class="description line-camp line-4 hidden-xs" itemprop="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            Accusamus at aut blanditiis, corporis dolorem doloremque doloribus excepturi fugiat
                                            impedit iusto molestias nobis officiis porro quae quaerat quod tempore temporibus voluptates?</p>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00:00+08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20:00+08:00"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-15 col-md-3 col-sm-3 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo m-r-5"></i>

                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <p class="description line-camp line-4 hidden-xs" itemprop="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            Accusamus at aut blanditiis, corporis dolorem doloremque doloribus excepturi fugiat
                                            impedit iusto molestias nobis officiis porro quae quaerat quod tempore temporibus voluptates?</p>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00:00+08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20:00+08:00"/>

                                    </div>
                                </a>
                            </article>
                        </div>

                    </div>

                </div>

                <div class="banner-12p shadow m-b-30">
                    <div class="content flex-center"></div>
                </div>

                <div class="news-section style-1 m-b-15">

                    <h2 class="content-title m-t-0 m-b-20">Siyasət</h2>

                    <div class="row-10">

                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>

                    </div>

                </div>

                <div class="banner-12p shadow m-b-30">
                    <div class="content flex-center"></div>
                </div>

                <div class="news-section style-1 m-b-15">

                    <h2 class="content-title m-t-0 m-b-20">Siyasət</h2>

                    <div class="row-10">

                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>

                    </div>

                </div>

                <div class="banner-12p shadow m-b-30">
                    <div class="content flex-center"></div>
                </div>

                <div class="news-section style-1 m-b-15">

                    <h2 class="content-title m-t-0 m-b-20">Siyasət</h2>

                    <div class="row-10">

                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>

                    </div>

                </div>

                <div class="banner-12p shadow m-b-30">
                    <div class="content flex-center"></div>
                </div>

                <div class="news-section style-1 m-b-15">

                    <h2 class="content-title m-t-0 m-b-20">Siyasət</h2>

                    <div class="row-10">

                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>

                    </div>

                </div>

                <div class="banner-12p shadow m-b-30">
                    <div class="content flex-center"></div>
                </div>

                <div class="news-section style-1 m-b-15">

                    <h2 class="content-title m-t-0 m-b-20">Siyasət</h2>

                    <div class="row-10">

                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>

                    </div>

                </div>

                <div class="banner-12p m-b-30">
                    <div class="content flex-center"></div>
                </div>

                <div class="news-section style-1 m-b-15">

                    <h2 class="content-title m-t-0 m-b-20">Siyasət</h2>

                    <div class="row-10">

                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>

                    </div>

                </div>

                <div class="banner-12p shadow m-b-30">
                    <div class="content flex-center"></div>
                </div>

                <div class="news-section style-1 m-b-15">

                    <h2 class="content-title m-t-0 m-b-20">Siyasət</h2>

                    <div class="row-10">

                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>

                    </div>

                </div>

                <div class="banner-12p shadow m-b-20">
                    <div class="content flex-center"></div>
                </div>

                <div class="news-section style-1 m-b-15">

                    <h2 class="content-title m-t-0 m-b-20">Siyasət</h2>

                    <div class="row-10">

                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>
                        <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                            <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">
                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                                <a href="read.php" title="Title">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="800" height="600" itemprop="url">
                                        <meta itemprop="width" content="800">
                                        <meta itemprop="height" content="600">
                                    </div>

                                    <div class="info">

                                        <div class="meta-data">
                                            <i class="icon-photo"></i>
                                            <span itemprop="genre" title="Category Name">Category Name</span>
                                        </div>

                                        <h2 class="title line-camp line-3 m-0" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit accusantium beatae cupiditate delectus.</h2>

                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                        <meta itemprop="author" content="Author">
                                        <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                        <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                    </div>
                                </a>
                            </article>
                        </div>

                    </div>

                </div>

                <div class="banner-12p shadow m-b-20">
                    <div class="content flex-center"></div>
                </div>

            </main>

            <aside class="col-md-3 col-sm-12 col-xs-12" data-col="1" itemscope itemtype="http://schema.org/WPSideBar">

                <div class="fixed-wrap">

                    <div class="row-10">

                        <div class="col-lg-12 col-md-12 col-sm-4 hidden-xs m-b-20">
                            <div class="banner-100p shadow">
                                <div class="content flex-center"></div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 hidden-sm col-xs-12">

                            <div class="banner-20p m-b-10 shadow visible-xs">
                                <div class="content flex-center"></div>
                            </div>

                            <div class="time-line m-b-20 shadow border-bottom clear">

                                <h2 class="content-title m-0 p-15 border-bottom text-uppercase"><i class="icon-newspaper-2"></i> Xəbər lenti</h2>

                                <div class="content scrl">

                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-photo"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>

                                    <div class="item border-bottom visible-xs clear">
                                        <div class="banner-20p">
                                            <div class="content flex-center"></div>
                                        </div>
                                    </div>

                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>

                                    <div class="item border-bottom visible-xs clear">
                                        <div class="banner-20p">
                                            <div class="content flex-center"></div>
                                        </div>
                                    </div>

                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>
                                    <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                        <a href="read.php" title="Title">

                                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                                <i class="type xs icon-play-thin"></i>
                                                <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="200" height="200" itemprop="url">
                                                <meta itemprop="width" content="200">
                                                <meta itemprop="height" content="200">
                                            </div>

                                            <div class="info">

                                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>

                                                <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>
                                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

                                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                                <meta itemprop="author" content="Author">
                                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                        <meta itemprop="width" content="400">
                                                        <meta itemprop="height" content="400">
                                                    </div>
                                                    <meta itemprop="name" content="goldnews.az">
                                                </div>

                                            </div>

                                        </a>
                                    </article>

                                    <div class="item border-bottom visible-xs clear">
                                        <div class="banner-20p">
                                            <div class="content flex-center"></div>
                                        </div>
                                    </div>

                                    <a href="#" class="item block text-center bg-gray clear">Daha çox göstər</a>

                                </div>

                            </div>

                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-4 hidden-xs m-b-20">
                            <div class="banner-100p shadow">
                                <div class="content flex-center"></div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-4 hidden-xs">
                            <div class="banner-100p shadow">
                                <div class="content flex-center"></div>
                            </div>
                        </div>

                    </div>

                </div>

            </aside>

        </div>

    </div>

</div>

<?php include("footer.php") ?>