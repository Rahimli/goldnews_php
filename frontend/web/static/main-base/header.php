<!DOCTYPE html>
<html lang="az" itemscope itemtype="http://schema.org/WebSite">

<head>

    <title>GoldNews.az</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0; user-scalable=no; width=device-width">
    <meta name="MobileOptimized" content="width"/>
    <meta name="HandheldFriendly" content="true"/>
    <meta name="mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content=#ca891c">
    <meta name="msapplication-navbutton-color" content="#ca891c">
    <meta name="theme-color" content="#ca891c"/>
    <meta name="content-language" content="AZ"/>
    <meta name="medium" content="news"/>

    <meta name="robots"                                 content="all"/>
    <meta name="googlebot"                              content="index, follow, archive"/>
    <meta name="yahoobot"                               content="index, follow, archive"/>
    <meta name="alexabot"                               content="index, follow, archive"/>
    <meta name="msnbot"                                 content="index, follow, archive"/>
    <meta name="dmozbot"                                content="index, follow, archive"/>
    <meta name="revisit-after"                          content="1 days"/>

    <meta name="audience" content="all"/>
    <meta name="distribution" content="global"/>
    <meta name="rating" content="General"/>
    <meta name="language" content="Azerbaijani"/>
    <meta name="format-detection" content="telephone=no"/>
    <meta name="contact" content="goldnews@mail.ru"/>
    <meta name="reply-to" content="goldnews@mail.ru"/>
    <meta name="copyright" content="goldnews.az (c) 2017 Bütün hüquqlar qorunur"/>
    <meta name="generator" content="creator name"/>
    <meta name="designer" content="Kamal Balayev"/>

    <meta name="description" content="descripption">
    <meta name="keywords" content="keywords">

    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:site" content="@goldnews.az"/>
    <meta name="twitter:title" content="goldnews.az"/>
    <meta name="twitter:description" content="description"/>
    <meta name="twitter:url" content="url"/> <!--Url nədisə o da burda olmalidi-->
    <meta name="twitter:domain" content="goldnews.az"/>
    <meta name="twitter:creator" content="@goldnews.az"/>
    <meta name="twitter:image" content="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Hyperia.jpg/220px-Hyperia.jpg"/>
    <meta name="twitter:account_id" content="15651700"/>

    <meta name="twitter:player" content="https://www.youtube.az/embed/9sg-A-eS6Ig"><!-- player  sehifesinde-->

    <meta property="fb:app_id" content=""/><!--Facebook app id-->
    <meta property="fb:pages" content=""/><!--Facebook page id-->
    <meta property="fb:admins" content=""/> <!--Facebook admin account id-->

    <meta property="og:locale" content="az_AZ"/>
    <meta property="og:type" content="website"/><!--read sehifesinde article, player sehifesinde video-->
    <meta property="og:title" content="goldnews.az"/>
    <meta property="og:description" content="description"/>
    <meta property="og:url" content="http://goldnews.az"/>
    <meta property="og:site_name" content="goldnews.az"/>

    <meta property="og:image" content="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Hyperia.jpg/220px-Hyperia.jpg"/>
    <!--Burda Paylaşım üçün olan şəkil olacaq-->
    <meta property="og:image:secure_url" content="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Hyperia.jpg/220px-Hyperia.jpg"/>
    <!--Burda Paylaşım üçün olan şəkil olacaq-->
    <meta property="og:image:type" content="image/jpeg"/>
    <meta property="og:image:width" content="600"/>
    <meta property="og:image:height" content="600"/>


    <!--read sehifelerinde gorsenir -->
    <meta property="article:author" content="Eldar Zeynallı"/>
    <meta property="article:section" content="category"/> <!--blogun kateqoriyası-->
    <meta property="article:publisher" content="https://www.facebook.com/goldnews"/>
    <meta property="article:published_time" content="2013-09-17T05:59"/>
    <meta property="article:modified_time" content="2013-09-16T19:08"/>
    <meta property="article:tag" content="tag"/> <!--her taga aid coxalacaq bu meta-->

    <link rel="canonical" href="http://goldnews.az" itemprop="url">

    <link rel="shortcut icon" href="assets/images/favicon.png">
    <link rel="apple-touch-icon" href="assets/images/favicon.png">

    <link rel="stylesheet" href="assets/css/style-template.css">
    <link rel="stylesheet" href="assets/css/style-body.css">
    <link rel="stylesheet" href="assets/css/app.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.css">
    <link rel="stylesheet" href="assets/css/owl.theme.css">
    <link rel="stylesheet" href="assets/css/weather-icons.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/icons/style.css">

    <script src="assets/js/jquery-2.1.4.min.js"></script>
    <script src="assets/js/owl.carousel.js"></script>
    <script src="assets/js/app.js"></script>
    <script src="assets/js/sticky.js"></script>

</head>

<body  itemscope itemtype="http://schema.org/WebPage">

<div class="banner_wrapper hidden-sm hidden clear">

    <div class="banner-12p">
        <div class="content flex-center"></div>
    </div>

</div>

<div class="fixed-margin visible-lg visible-sm"></div>

<header class="main-header tr-3s" itemscope itemtype="http://schema.org/WPHeader">

    <div class="right-side tr-3s clear">

        <ul class="right-side-menu list-unstyled float-right">
            <li><a href="videos.php"><i class="icon-video-gallery"></i>Videolar</a></li>
            <li><a href="photos.php"><i class="icon-photo-gallery"></i>Fotolar</a></li>
            <li><a href="about.php"><i class="icon-info-circle-thin"></i>Haqqımızda</a></li>
            <li><a href="contact.php"><i class="icon-envelope-o"></i>Əlaqə</a></li>
            <li><a href="ads.php"><i class="icon-ads"></i>Saytda reklam</a></li>
            <li class="hidden-lg hidden-md"><a href="weather.php"><i class="wi wi-day-cloudy"></i>Hava</a></li>
            <li class="hidden-lg hidden-md"><a href="currency.php"><i class="icon-currency-azn"></i>Valyuta</a></li>
        </ul>

        <ul class="social-menu list-unstyled float-left">
            <li><a href="#" itemprop="sameAs"><i class="icon-social-facebook"></i><span class="hidden-lg hidden-md">Facebook</span></a></li>
            <li><a href="#" itemprop="sameAs"><i class="icon-social-twitter"></i><span class="hidden-lg hidden-md">Twitter</span></a></li>
            <li><a href="#" itemprop="sameAs"><i class="icon-social-google-plus"></i><span class="hidden-lg hidden-md">Google+</span></a></li>
            <li><a href="#" itemprop="sameAs"><i class="icon-social-youtube-button"></i><span class="hidden-lg hidden-md">Youtube</span></a></li>
        </ul>

        <div class="side-footer visible-sm visible-xs">
            <meta itemprop="telephone" content="+994-70-123-45-67"/>

            <small>Creator : <a href="#">Creator name</a></small>
            <small>goldnews.az © 2017</small>
            <small>Bütün haqqları qorunur.</small>
        </div>

    </div>

    <div class="middle-wrapper tr-3s clear" itemscope itemtype="http://schema.org/Organization">

        <span class="nav-toggle waves-effect visible-xs visible-sm"><i></i></span>

        <h1 class="logo-col col-lg-3 col-md-3 col-sm-2 col-xs-3 p-0 m-0">

            <a href="index.php" title="Gazetta.az - logo" class="logo" itemprop="url">
                <img src="assets/images/logo.png" alt="Goldnews.az" width="250" height="37" itemprop="logo">
                <span class="slogan">
                    <span itemprop="name">goldnews.az</span> -
                    <span itemprop="description">Aktual xəbərlərin ünvanı</span>
                </span>
            </a>

        </h1>

        <div class="search-box tr-3s col-lg-4 col-md-4 col-sm-12 col-xs-12">

            <form itemprop="potentialAction" itemscope itemtype="http://schema.org/SearchAction">

                <meta itemprop="target" content="https://query.goldnews.az/search?q={search_term_string}"/>

                <div class="input-group">

                    <input class="input" placeholder="Axtar" itemprop="query-input" type="search" name="search_term_string" required/>

                    <span class="input-group-btn">
                        <button class="btn" type="submit"><i class="icon-search"></i></button>
                        <button class="btn search-close hidden-lg hidden-md" type="reset"><i>×</i></button>
                    </span>

                </div>

            </form>

        </div>

        <div class="toggle-buttons float-right visible-sm visible-xs">
            <span class="toggle search-toggle icon-search"></span>
            <span class="toggle side-toggle icon-ellipsis-v"></span>
        </div>
        
        <div class="right col-lg-5 col-md-5 col-sm-7 hidden-xs clear">

            <div class="float-right weather_currency clear">

                <div class="currency clear float-left clear m-r-40">
                    <a href="currency.php" title="Valyuta məzənnəsi" class="icon icon-currency-azn"></a>
                    <div class="carousel owl-carousel owl-theme float-left">

                        <div class="item">
                            <div><i class="icon-currency-usd"></i></div>
                            <div><b>USD</b></div>
                            <div><b>1,70080</b></div>
                            <div><i class="up">▲</i></div>
                        </div>

                        <div class="item">
                            <div><i class="icon-currency-euro"></i></div>
                            <div><b>EURO</b></div>
                            <div><b>2,01490</b></div>
                            <div><i class="down">▼</i></div>
                        </div>

                        <div class="item">
                            <div><i class="icon-currency-rubl"></i></div>
                            <div><b>RUB</b></div>
                            <div><b>0,02810</b></div>
                            <div><i class="middle">●</i></div>
                        </div>

                    </div>

                </div>

                <div class="weather clear float-left clear">

                    <a href="weather.php" title="Hava proqnozu" class="icon wi wi-day-cloudy"></a>

                    <div class="float-left">

                        <div class="item">
                            <div><b>Bakı</b> </div>
                            <div><b>25<sup>o</sup> / 85<sup>o</sup> C</b></div>
                        </div>

                    </div>

                </div>

            </div>
        </div>

    </div>

    <nav class="main-nav tr-3s" itemscope="itemscope" itemtype="http://www.schema.org/SiteNavigationElement">
        <ul class="list-unstyled clear" role="menu">

            <li>
                <a href="index.php" itemprop="url">
                    <i class="icon-home-1 hidden-xs hidden-sm"></i>
                    <span class="visible-xs visible-sm" itemprop="name">Ana səhifə</span>
                </a>
            </li>
            <li>
                <a href="category.php" itemprop="url">
                    <span itemprop="name">Kateqoriya</span>
                </a>
            </li>
            <li>
                <a href="category.php" itemprop="url">
                    <span itemprop="name">Kateqoriya</span>
                </a>
            </li>
            <li>
                <a href="category.php" itemprop="url">
                    <span itemprop="name">Kateqoriya</span>
                </a>
            </li>
            <li>
                <a href="category.php" itemprop="url">
                    <span itemprop="name">Kateqoriya</span>
                </a>
            </li>
            <li>
                <a href="category.php" itemprop="url">
                    <span itemprop="name">Kateqoriya</span>
                </a>
            </li>
            <li>
                <a href="category.php" itemprop="url">
                    <span itemprop="name">Kateqoriya</span>
                </a>
            </li>
            <li>
                <a href="category.php" itemprop="url">
                    <span itemprop="name">Kateqoriya</span>
                </a>
            </li>
            <li>
                <a href="category.php" itemprop="url">
                    <span itemprop="name">Kateqoriya</span>
                </a>
            </li>
            <li>
                <a href="category.php" itemprop="url">
                    <span itemprop="name">Kateqoriya</span>
                </a>
            </li>
            <li>
                <a href="category.php" itemprop="url">
                    <span itemprop="name">Kateqoriya</span>
                </a>
            </li>

        </ul>
    </nav>

</header>

<section class="main-container relative">
