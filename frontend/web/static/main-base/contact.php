<?php include("header.php") ?>

<div class="page-wrapper">

    <div class="container p-l-20 p-r-20">

        <div class="row-15">

            <div class="main-side col-md-12 col-sm-12 col-xs-12">

                <div class="static-pages m-t-20">

                    <nav class="bg-white shadow p-15 m-b-20">

                        <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="index.php" title="goldnews.az - Ana səhifə" itemprop="item">
                                    <span itemprop="name">Ana səhifə</span>
                                </a>
                                <meta itemprop="position" content="1">
                            </li>
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="category.php" title="Bizimlə əlaqə" itemprop="item">
                                    <span itemprop="name">Bizimlə əlaqə</span>
                                </a>
                                <meta itemprop="position" content="2">
                            </li>
                        </ol>

                    </nav>

                    <div class="row-10">

                        <div class="col-md-3 col-sm-12 col-xs-12 m-b-20">

                            <div class="static-side-menu shadow bg-white p-t-10 p-b-10 clear">

                                <a href="about.php" class="tr-3s"><i class="icon-info-circle-thin"></i> Haqqımızda</a>
                                <a href="contact.php" class="tr-3s current-page"><i class="icon-envelope-o"></i> Bizimlə əlaqə</a>
                                <a href="ads.php" class="tr-3s"><i class="icon-ads"></i> Saytda reklam</a>
                                <a href="weather.php" class="tr-3s"><i class="wi wi-day-cloudy"></i> Hava</a>
                                <a href="currency.php" class="tr-3s"><i class="icon-currency-azn"></i> Valyuta</a>

                            </div>

                        </div>

                        <div class="col-md-9 col-sm-12 col-xs-12 m-b-20">

                            <div class="static-page-wrapper shadow bg-white clear">

                                <h1 class="content-title p-20 m-0 border-bottom"><strong><i class="icon-envelope-o"></i> BİZİMLƏ ƏLAQƏ</strong></h1>

                                <div class="map clear">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3039.7724612250163!2d49.80777315052918!3d40.36956937926939!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40307ddd2da5de55%3A0x3d0857429675caf!2zQXrJmXJiYXljYW4gbsmZxZ9yaXl5YXTEsQ!5e0!3m2!1saz!2s!4v1486174096283" allowfullscreen></iframe>
                                </div>

                                <div class="clear p-20">

                                    <div class="row-10">

                                        <div class="col-md-6 col-sm-12 col-xs-12 float-right">

                                            <div class="card shadow p-15">

                                                <table class="table mar-0">
                                                    <tbody>
                                                        <tr>
                                                            <td><strong>Baş redaktor:</strong></td>
                                                            <td>Eldar Zeynallı</td>
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Telefon:</strong></td>
                                                            <td><a href="tel:+994121234556">+(994 12) 123 45 56</a>
                                                                <a href="tel:+994701234556">+(994 70) 123 45 56</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><strong>E-poçt:</strong></td>
                                                            <td><a href="mailto:goldnews@mail.ru">goldnews@mail.ru</a></td>
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Ünvan:</strong></td>
                                                            <td><address>Bakı şəhəri, Yasamal rayonu, Mətbuat pr.529. Azərbaycan Nəşriyyatı, 7-ci mərtəbə</address></td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                            </div>

                                        </div>
                                        
                                        <div class="clear m-b-20 visible-sm visible-xs"></div>

                                        <div class="col-md-6 col-sm-12 col-xs-12 float-right">

                                            <div class="contact shadow p-15">

                                                <h4 class="m-t-0 m-b-20 p-b-10 border-bottom"><strong><i class="icon-pen-write-1"></i> Bizə mesaj yazın</strong></h4>

                                                <div class="row-7">

                                                    <div class="col-md-6 col-sm-6 col-xs-12 m-b-15">
                                                        <label for="user_name">Ad və soyad:</label>
                                                        <input type="text" class="input" id="user_name" placeholder="Adınızı yazın">
                                                    </div>

                                                    <div class="col-md-6 col-sm-6 col-xs-12 m-b-15">
                                                        <label for="user_email">E-poçt</label>
                                                        <input type="email" class="input" id="user_email" placeholder="E-poçt ünvanınızı yazın">
                                                    </div>

                                                    <div class="col-md-12 col-sm-12 col-xs-12 m-b-10">
                                                        <label for="user_text">Mesaj:</label>
                                                        <textarea class="input no-resize" id="user_text" placeholder="Mesajınızı yazın..."></textarea>
                                                        <small>Minimum: 20, maksimum 400 simvol</small>
                                                    </div>

                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <button class="btn bg-success">GÖNDƏR <i class="icon-send"></i></button>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<?php include("footer.php") ?>