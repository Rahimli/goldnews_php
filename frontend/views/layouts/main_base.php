<?php
/**
 * Created by PhpStorm.
 * User: rahim
 * Date: 7/18/2017
 * Time: 11:51
 */
use yii\helpers\Html;
use yii\helpers\Url;

$formatter = \Yii::$app->formatter;
$base_categories = Yii::$app->params["base_categories"];
$base_currencies = Yii::$app->params["base_currencies"];
$base_socials = Yii::$app->params["base_socials"];
$base_first_weather = Yii::$app->params["base_first_weather"]
?>
<!DOCTYPE html>
<html lang="az" itemscope itemtype="http://schema.org/WebSite">

<head>

    <title>GoldNews.az - <?= Html::encode($this->title) ?></title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no, width=device-width">
    <meta name="MobileOptimized" content="width"/>
    <meta name="HandheldFriendly" content="true"/>
    <meta name="mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content=#ca891c">
    <meta name="msapplication-navbutton-color" content="#ca891c">
    <meta name="theme-color" content="#ca891c"/>
    <meta name="content-language" content="AZ"/>
    <meta name="medium" content="news"/>

    <meta name="robots" content="all"/>
    <meta name="googlebot" content="index, follow, archive"/>
    <meta name="yahoobot" content="index, follow, archive"/>
    <meta name="alexabot" content="index, follow, archive"/>
    <meta name="msnbot" content="index, follow, archive"/>
    <meta name="dmozbot" content="index, follow, archive"/>
    <meta name="revisit-after" content="1 days"/>

    <meta name="audience" content="all"/>
    <meta name="distribution" content="global"/>
    <meta name="rating" content="General"/>
    <meta name="language" content="Azerbaijani"/>
    <meta name="format-detection" content="telephone=no"/>
    <meta name="contact" content="goldnews@mail.ru"/>
    <meta name="reply-to" content="goldnews@mail.ru"/>
    <meta name="copyright" content="goldnews.az (c) 2017 Bütün hüquqlar qorunur"/>
    <meta name="generator" content="e-Box"/>
    <meta name="designer" content="Kamal Balayev"/>

    <meta name="description" content="descripption">
    <meta name="keywords" content="keywords">

    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:site" content="@goldnews.az"/>
    <meta name="twitter:title" content="goldnews.az"/>
    <meta name="twitter:description" content="description"/>
    <meta name="twitter:url" content="url"/> <!--Url nədisə o da burda olmalidi-->
    <meta name="twitter:domain" content="goldnews.az"/>
    <meta name="twitter:creator" content="@goldnews.az"/>
    <meta name="twitter:image"
          content="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Hyperia.jpg/220px-Hyperia.jpg"/>
    <meta name="twitter:account_id" content="15651700"/>

    <meta name="twitter:player" content="https://www.youtube.az/embed/9sg-A-eS6Ig"><!-- player  sehifesinde-->

    <meta property="fb:app_id" content=""/><!--Facebook app id-->
    <meta property="fb:pages" content=""/><!--Facebook page id-->
    <meta property="fb:admins" content=""/> <!--Facebook admin account id-->

    <meta property="og:locale" content="az_AZ"/>
    <meta property="og:type" content="website"/><!--read sehifesinde article, player sehifesinde video-->
    <meta property="og:description" content="description"/>
    <meta property="og:site_name" content="goldnews.az"/>

    <meta property="og:image"
          content="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Hyperia.jpg/220px-Hyperia.jpg"/>
    <!--Burda Paylaşım üçün olan şəkil olacaq-->
    <meta property="og:image:secure_url"
          content="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Hyperia.jpg/220px-Hyperia.jpg"/>
    <!--Burda Paylaşım üçün olan şəkil olacaq-->
    <meta property="og:image:type" content="image/jpeg"/>
    <meta property="og:image:width" content="600"/>
    <meta property="og:image:height" content="600"/>


    <!--read sehifelerinde gorsenir -->


    <link rel="canonical" href="http://goldnews.az" itemprop="url">

    <link rel="shortcut icon" href="/static/main-base/assets/images/favicon.png">
    <link rel="apple-touch-icon" href="/static/main-base/assets/images/favicon.png">

    <link rel="stylesheet" href="/static/main-base/assets/css/style-template.css">
    <link rel="stylesheet" href="/static/main-base/assets/css/style-body.css">
    <link rel="stylesheet" href="/static/main-base/assets/css/app.css">
    <link rel="stylesheet" href="/static/main-base/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="/static/main-base/assets/css/owl.theme.css">
    <link rel="stylesheet" href="/static/main-base/assets/css/weather-icons.css">
    <link rel="stylesheet" href="/static/main-base/assets/css/animate.css">
    <link rel="stylesheet" href="/static/main-base/assets/icons/style.css">

    <script src="/static/main-base/assets/js/jquery-2.1.4.min.js"></script>
    <script src="/static/main-base/assets/js/owl.carousel.js"></script>
    <script src="/static/main-base/assets/js/app.js"></script>
    <script src="/static/main-base/assets/js/sticky.js"></script>

</head>

<body itemscope itemtype="http://schema.org/WebPage">

<div class="banner_wrapper hidden-sm hidden clear">

    <div class="banner-12p">
        <div class="content flex-center"></div>
    </div>

</div>

<div class="fixed-margin visible-lg visible-sm"></div>

<header class="main-header tr-3s" itemscope itemtype="http://schema.org/WPHeader">

    <div class="right-side tr-3s clear">

        <ul class="right-side-menu list-unstyled float-right">
            <li><a href="<?php echo Url::toRoute(['site/multimedias', 'm_slug' => 'video-xeber']) ?>"><i
                            class="icon-video-gallery"></i>Videolar</a></li>
            <li><a href="<?php echo Url::toRoute(['site/multimedias', 'm_slug' => 'foto-xeber']) ?>"><i
                            class="icon-photo-gallery"></i>Fotolar</a></li>
            <li><a href="<?php echo Url::toRoute(['site/page', 'p_slug' => 'haqqimizda']) ?>"><i
                            class="icon-info-circle-thin"></i>Haqqımızda</a></li>
            <!--            <li><a href="contact.php"><i class="icon-envelope-o"></i>Əlaqə</a></li>-->
            <li><a href="<?php echo Url::toRoute(['site/page', 'p_slug' => 'saytda-reklam']) ?>"><i
                            class="icon-ads"></i>Saytda reklam</a></li>
            <li class="hidden-lg hidden-md"><a href="<?php echo Url::toRoute(['site/weather']) ?>"><i
                            class="wi wi-day-cloudy"></i>Hava</a></li>
            <li class="hidden-lg hidden-md"><a href="<?php echo Url::toRoute(['site/currency']) ?>"><i
                            class="icon-currency-azn"></i>Valyuta</a></li>
        </ul>

        <ul class="social-menu list-unstyled float-left">
            <?php foreach ($base_socials as $base_social) { ?>
                <li>
                    <a target="_blank" href="<?= $base_social->url; ?>" itemprop="sameAs"><i
                                class="<?= $base_social->icon ?>">
                        </i><span class="hidden-lg hidden-md"><?= $base_social->name ?></span>
                    </a>
                </li>
            <?php } ?>
        </ul>

        <div class="side-footer visible-sm visible-xs">
            <meta itemprop="telephone" content="+994-70-123-45-67"/>

            <small>Creator : <a href="#">Creator name</a></small>
            <small>goldnews.az © 2017</small>
            <small>Bütün haqqları qorunur.</small>
        </div>

    </div>

    <div class="middle-wrapper tr-3s clear" itemscope itemtype="http://schema.org/Organization">

        <span class="nav-toggle waves-effect visible-xs visible-sm"><i></i></span>

        <h1 class="logo-col col-lg-3 col-md-3 col-sm-2 col-xs-3 p-0 m-0">

            <a href="<?php echo Url::toRoute(['site/index']) ?>" title="Gazetta.az - logo" class="logo" itemprop="url">
                <img src="/static/main-base/assets/images/logo.png" alt="Goldnews.az" width="250" height="37"
                     itemprop="logo">
                <span class="slogan">
                    <span itemprop="name">goldnews.az</span> -
                    <span itemprop="description">Aktual xəbərlərin ünvanı</span>
                </span>
            </a>

        </h1>

        <div class="search-box tr-3s col-lg-4 col-md-4 col-sm-12 col-xs-12">

            <form itemprop="potentialAction" method="get" action="<?php echo Url::toRoute(['site/search']) ?>" itemscope
                  itemtype="http://schema.org/SearchAction">

                <meta itemprop="target" content="https://query.goldnews.az/search/search?q={search_term_string}"/>

                <div class="input-group">

                    <input class="input" placeholder="Axtar" itemprop="query-input" type="search"
                           name="query"/>

                    <span class="input-group-btn">
                        <button class="btn" type="submit"><i class="icon-search"></i></button>
                        <button class="btn search-close hidden-lg hidden-md" type="reset"><i>×</i></button>
                    </span>

                </div>

            </form>

        </div>

        <div class="toggle-buttons float-right visible-sm visible-xs">
            <span class="toggle search-toggle icon-search"></span>
            <span class="toggle side-toggle icon-ellipsis-v"></span>
        </div>

        <div class="right col-lg-5 col-md-5 col-sm-7 hidden-xs clear">

            <div class="float-right weather_currency clear">

                <div class="currency clear float-left clear m-r-40">
                    <a href="<?php echo Url::toRoute(['site/currency']) ?>" title="Valyuta məzənnəsi"
                       class="icon icon-currency-azn"></a>
                    <div class="carousel owl-carousel owl-theme float-left">
                        <?php $i = 1 ?>
                        <?php foreach ($base_currencies as $base_currency) { ?>
                            <?php if ($i == 1) {
                                $val_s = 'usd';
                            } else if ($i == 2) {
                                $val_s = 'euro';
                            } else {
                                $val_s = 'rubl';
                            } ?>
                            <div class="item">
                                <div><i class="icon-currency-<?= $val_s ?>"></i></div>
                                <div><b><?= $base_currency->nominal ?></b></div>
                                <div><b><?= $base_currency->value ?></b></div>
                                <div>
                                    <i class="<?php if ($base_currency->status == 1) { ?>up<?php } else if ($base_currency->status == 2) { ?>down<?php } else if ($base_currency->status == 0) { ?>middle<?php } ?>">
                                        <?php if ($base_currency->status == 1) { ?>▲<?php } else if ($base_currency->status == 2) { ?>▼<?php } else if ($base_currency->status == 0) { ?>●<?php } ?>
                                    </i></div>
                            </div>
                            <?php $i += 1 ?>
                        <?php } ?>

                    </div>

                </div>

                <div class="weather clear float-left clear">

                    <a href="<?php echo Url::toRoute(['site/weather']) ?>" title="Hava proqnozu"
                       class="icon wi wi-day-cloudy"></a>

                    <div class="float-left">
                        <?php $degrees = explode("/", $base_first_weather->degrees );?>
                        <div class="item">
                            <div><b><?= $base_first_weather->city->name ?></b> </div>
                            <div><b><?= $degrees[0]?><sup>o</sup> / <?= $degrees[1]?><sup>o</sup> C</b></div>
                        </div>

                    </div>

                </div>

            </div>
        </div>

    </div>

    <nav class="main-nav tr-3s" itemscope="itemscope" itemtype="http://www.schema.org/SiteNavigationElement">
        <ul class="list-unstyled clear" role="menu">

            <li>
                <a href="<?php echo Url::toRoute(['site/index']) ?>" itemprop="url">
                    <i class="icon-home-1 hidden-xs hidden-sm"></i>
                    <span class="visible-xs visible-sm" itemprop="name">Ana səhifə</span>
                </a>
            </li>
            <?php foreach ($base_categories as $base_category) { ?>
                <li>
                    <a href="<?php echo Url::toRoute(['site/category', 'c_slug' => $base_category->slug]) ?>"
                       itemprop="url">
                        <span itemprop="name"><?= $base_category->name ?></span>
                    </a>
                </li>
            <?php } ?>

        </ul>
    </nav>

</header>

<section class="main-container relative">

    <?= $content ?>


</section>

<div id="back-top" class="back-top">⇪</div>

<div class="overlay tr-3s"></div>

<div class="modal newsletter-modal" data-modal="newsletter-modal">
    <div class="flex-col flex-center">
        <div class="modal-content xs zoomIn">

            <span class="close" data-close="modal">✖</span>

            <div class="newsletter-cover"></div>

            <div class="bg-white p-20">
                <h4 class="newsletter-title m-t-0 m-b-20 text-center">Ən son xəbərləri ilk siz görün!</h4>

                <form action="newslatter.php">

                    <div class="input-group">

                        <input type="email" class="input" placeholder="E-poçt ünvanınızı yazın">

                        <span class="input-group-btn">
                            <button class="btn" type="submit"><i class="icon-send"></i></button>
                        </span>

                    </div>

                </form>

            </div>

        </div>
    </div>
</div>

<footer class="main-footer hidden-xs p-t-20 p-b-20 border-top clear" itemscope itemtype="http://schema.org/WPFooter">

    <div class="copyright col-md-4 col-sm-6 col-xs-12">
        <span class="m-r-15">GoldNews.az © <span itemprop="copyrightYear"></span></span>
        <span>Bütün haqqları qorunur</span>
    </div>

    <nav class="footer-menu col-md-4 hidden-sm text-center" rel="menu">

        <a href="<?php echo Url::toRoute(['site/page', 'p_slug' => 'haqqimizda']) ?>" title="Haqqımızda">Haqqımızda</a>
        <!--        <a href="contact.php" title="Əlaqə">Əlqə</a>-->
        <a href="<?php echo Url::toRoute(['site/page', 'p_slug' => 'saytda-reklam']) ?>" title="Saytda reklam">Saytda
            reklam</a>
        <a href="<?php echo Url::toRoute(['site/weather']) ?>" title="Hava">Hava</a>
        <a href="<?php echo Url::toRoute(['site/currency']) ?>" title="Valyuta">Valyuta</a>

    </nav>

    <div class="creator col-md-4 col-sm-6 text-right" itemprop="copyrightHolder" itemscope
         itemtype="http://schema.org/Organization">
        <a href="http://www.e-box.az/" target="_blank" itemprop="url"><strong itemprop="name"> e-Box</strong></a>
    </div>

</footer>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/az_AZ/sdk.js#xfbml=1&version=v2.8&appId=1297304813676702";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>
