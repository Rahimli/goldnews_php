<?php
/**
 * Created by PhpStorm.
 * User: rahim
 * Date: 8/15/2017
 * Time: 1:20
 */

use yii\helpers\Url;

$this->title = 'Valyuta';
$this->registerMetaTag([
    'property' => 'og:url',
    'content' => Url::toRoute(['site/currency']),
]);
$this->registerMetaTag([
    'property' => 'og:title',
    'content' => 'Valyuta',
]);

$this->registerMetaTag([
    'property' => 'og:image',
    'content' => 'static/main-base/assets/images/logos/gold_news.png',
]);


$formatter = \Yii::$app->formatter;
?>
<div class="page-wrapper">

    <div class="container p-l-20 p-r-20">

        <div class="row-15">

            <div class="main-side col-md-12 col-sm-12 col-xs-12">

                <div class="static-pages m-t-20">

                    <nav class="bg-white shadow p-15 m-b-20">

                        <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="index.php" title="goldnews.az - Ana səhifə" itemprop="item">
                                    <span itemprop="name">Ana səhifə</span>
                                </a>
                                <meta itemprop="position" content="1">
                            </li>
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="category.php" title="Valyuta" itemprop="item">
                                    <span itemprop="name">Valyuta</span>
                                </a>
                                <meta itemprop="position" content="2">
                            </li>
                        </ol>

                    </nav>

                    <div class="row-10">

                        <div class="col-md-3 col-sm-12 col-xs-12 m-b-20">

                            <div class="static-side-menu shadow bg-white p-t-10 p-b-10 clear">

                                <a href="<?php echo Url::toRoute(['site/page', 'p_slug' => 'haqqimizda']) ?>" class="tr-3s"><i class="icon-info-circle-thin"></i> Haqqımızda</a>
                                <!--                                <a href="contact.php" class="tr-3s"><i class="icon-envelope-o"></i> Bizimlə əlaqə</a>-->
                                <a href="<?php echo Url::toRoute(['site/page', 'p_slug' => 'saytda-reklam']) ?>" class="tr-3s "><i class="icon-ads"></i> Saytda reklam</a>
                                <a href="<?php echo Url::toRoute(['site/weather']) ?>" class="tr-3s"><i class="wi wi-day-cloudy"></i> Hava</a>
                                <a href="<?php echo Url::toRoute(['site/currency']) ?>" class="tr-3s current-page"><i class="icon-currency-azn"></i> Valyuta</a>

                            </div>

                        </div>

                        <div class="col-md-9 col-sm-12 col-xs-12 m-b-20">

                            <div class="static-page-wrapper shadow bg-white clear">

                                <h1 class="content-title p-20 m-0 border-bottom"><strong><i
                                                class="icon-currency-azn"></i> VALYUTA</strong></h1>

                                <div class="currency">

<!--                                    <div class="convertor p-20 bg-gray">-->
<!---->
<!--                                        <div class="row-7">-->
<!---->
<!--                                            <div class="col-md-3 col-sm-3 col-xs-6">-->
<!--                                                <label>Məbləğ</label>-->
<!--                                                <input type="text" class="input" placeholder="məs: 200">-->
<!--                                            </div>-->
<!---->
<!--                                            <div class="col-md-3 col-sm-3 col-xs-6">-->
<!--                                                <label>Valyutadan</label>-->
<!--                                                <select class="input">-->
<!--                                                    <option value="azn">AZN</option>-->
<!--                                                    <option value="rub">RUB</option>-->
<!--                                                    <option value="gbp">GBP</option>-->
<!--                                                    <option value="eur">EUR</option>-->
<!--                                                    <option value="ytl">YTL</option>-->
<!--                                                </select>-->
<!--                                            </div>-->
<!---->
<!--                                            <div class="clear m-b-10 visible-xs"></div>-->
<!---->
<!--                                            <div class="col-md-3 col-sm-3 col-xs-6">-->
<!--                                                <label>Valyutaya</label>-->
<!--                                                <select class="input">-->
<!--                                                    <option value="azn">AZN</option>-->
<!--                                                    <option value="rub">RUB</option>-->
<!--                                                    <option value="gbp">GBP</option>-->
<!--                                                    <option value="eur">EUR</option>-->
<!--                                                    <option value="ytl">YTL</option>-->
<!--                                                </select>-->
<!--                                            </div>-->
<!---->
<!--                                            <div class="col-md-3 col-sm-3 col-xs-6">-->
<!--                                                <label>Nəticə</label>-->
<!--                                                <input type="text" class="input" placeholder="0,00" readonly>-->
<!--                                            </div>-->
<!---->
<!--                                        </div>-->
<!---->
<!--                                    </div>-->

                                    <div class="clear p-20">

                                        <h4 class="m-t-0 m-b-20"><b>AZN məzənnələri</b>
                                            <smal class="float-right"><?= date('d.m.Y')?></smal>
                                        </h4>

                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover">

                                                <thead>

                                                <tr>
                                                    <th colspan="2">Valyuta</th>
                                                    <th>Satış</th>
                                                    <th>Status</th>
                                                </tr>

                                                </thead>

                                                <tbody>
                                                <?php foreach ($currencies as $currency) { ?>
                                                    <tr>
                                                        <td><?= $currency->nominal?></td>
                                                        <td><b><?= $currency->name?></b></td>
                                                        <td><?= $currency->value?></td>
                                                        <td><i class="<?php if($currency->status==1){?>up<?php }else if($currency->status==2){?>down<?php }else if($currency->status==0){?>middle<?php }?>">
                                                                <?php if($currency->status==1){?>▲<?php }else if($currency->status==2){?>▼<?php }else if($currency->status==0){?>●<?php }?>
                                                            </i>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>

                                            </table>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

