<?php
/**
 * Created by PhpStorm.
 * User: rahim
 * Date: 8/14/2017
 * Time: 15:14
 */

use backend\models\News;
use yii\helpers\Url;

$formatter = \Yii::$app->formatter;
$base_categories = Yii::$app->params["base_categories"];


$this->title = 'Ana Səhifə';
$this->registerMetaTag([
    'property' => 'og:url',
    'content' => Url::toRoute(['site/index']),
]);
$this->registerMetaTag([
    'property' => 'og:title',
    'content' => 'Ana Səhifə',
]);

$this->registerMetaTag([
    'property' => 'og:image',
    'content' => 'static/main-base/assets/images/logos/gold_news.png',
]);
?>

<div class="main-wrapper">

    <div class="container p-20">

        <div class="row-15">

            <main class="col-md-9 col-sm-12 hidden-xs" rel="main" data-main-col="1">

                <div class="news-section style-top m-b-20">

                    <div class="row-7">

                        <?php foreach ($base_slider_newses as $news) { ?>
                            <div class="item-col m-b-15 col-md-3 col-sm-3 col-xs-12">
                                <article class="item shadow clear" itemscope itemtype="http://schema.org/NewsArticle">

                                    <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1"/>

                                    <a href="<?php echo Url::toRoute(['site/news', 'n_id' => $news->u_id, 'n_slug' => $news->slug]) ?>"
                                       title="<?= $news->first_name ?> <?= $news->second_name ?>">

                                        <div class="thumb hover" itemprop="image" itemscope
                                             itemtype="https://schema.org/ImageObject">
                                            <img src="<?= $news->image?>"
                                                 alt="alt" width="800" height="600" itemprop="url">
                                            <meta itemprop="width" content="800">
                                            <meta itemprop="height" content="600">
                                        </div>

                                        <div class="info">

                                            <div class="meta-data line-camp line-1">
                                                <i class="icon-photo-gallery type m-r-5"></i>

<!--                                                <span class="m-r-10 category" itemprop="genre">Kateqoriya</span>-->

                                                <time datetime="2017-06-26T16:40" itemprop="datePublished">
                                                    <?php echo $formatter->asDate($news->publish_start_date, 'php:d F  H:i');?>
                                                </time>
                                            </div>

                                            <h2 class="title line-camp line-3 m-0" itemprop="headline">
                                                <span class="prefix-name"><?= $news->first_name?></span>
                                                <span class="suffix-name"><?= $news->second_name?></span>
                                            </h2>

                                            <p class="description line-camp line-4 hidden-xs" itemprop="description">
                                            <?= $news->short_text; ?>
                                            </p>

                                            <div itemprop="publisher" itemscope
                                                 itemtype="https://schema.org/Organization">
                                                <div itemprop="logo" itemscope
                                                     itemtype="https://schema.org/ImageObject">
                                                    <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                    <meta itemprop="width" content="400">
                                                    <meta itemprop="height" content="400">
                                                </div>
                                                <meta itemprop="name" content="goldnews.az">
                                            </div>

                                            <meta itemprop="author" content="Author">
                                            <meta itemprop="datePublished" content="2015-02-05T08:00:00+08:00"/>
                                            <meta itemprop="dateModified" content="2015-02-05T09:20:00+08:00"/>

                                        </div>
                                    </a>
                                </article>
                            </div>
                        <?php } ?>

                    </div>

                </div>
                <div class="banner-12p shadow m-b-30">
                    <div class="content flex-center"></div>
                </div>
                <?php foreach ($base_categories as $base_category) { ?>
                    <?php $newses = News::find()->where(['category_id' => $base_category->id])
                        ->orderBy(['publish_start_date' => SORT_DESC])->limit(7)->all() ;
//                    die(var_dump($newses));
                    ?>

                    <?php if (!empty($newses)) { ?>
                        <div class="news-section style-1 m-b-15">

                            <h2 class="content-title m-t-0 m-b-20">
                                <?= $base_category->name; ?>
                            </h2>

                            <div class="row-10">
                                <?php foreach ($newses as $news) { ?>
                                    <div class="item-col m-b-20 col-md-6 col-sm-6 col-xs-12">
                                        <article class="item shadow clear" itemscope
                                                 itemtype="http://schema.org/NewsArticle">
                                            <link itemprop="mainEntityOfPage"
                                                  href="<?php echo Url::toRoute(['site/news', 'n_id' => $news->u_id, 'n_slug' => $news->slug]) ?>"/>
                                            <a href="<?php echo Url::toRoute(['site/news', 'n_id' => $news->u_id, 'n_slug' => $news->slug]) ?>"
                                               title="<?= $news->first_name ?> <?= $news->second_name ?>">

                                                <div class="thumb hover" itemprop="image" itemscope
                                                     itemtype="https://schema.org/ImageObject">
                                                    <img src="<?= $news->image ?>" alt="alt" width="800" height="600"
                                                         itemprop="url">
                                                    <meta itemprop="width" content="800">
                                                    <meta itemprop="height" content="600">
                                                </div>

                                                <div class="info">

                                                    <h2 class="title line-camp line-3 m-0" itemprop="headline">
                                                        <span class="prefix-name"><?= $news->first_name ?></span>
                                                        <span class="suffix-name"><?= $news->second_name ?></span>
                                                    </h2>
                                                    <div class="meta-data line-camp line-1">
                                                        <i class="icon-photo-gallery type m-r-5"></i>

                                                        <span class="m-r-10 category" itemprop="genre"><?= $news->category->name?></span>

                                                        <time datetime="2017-06-26T16:40" itemprop="datePublished">
                                                            <?php echo $formatter->asDate($news->publish_start_date, 'php:d F, H:i');?>
                                                        </time>
                                                    </div>
                                                    <div itemprop="publisher" itemscope
                                                         itemtype="https://schema.org/Organization">
                                                        <div itemprop="logo" itemscope
                                                             itemtype="https://schema.org/ImageObject">
                                                            <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                            <meta itemprop="width" content="400">
                                                            <meta itemprop="height" content="400">
                                                        </div>
                                                        <meta itemprop="name" content="goldnews.az">
                                                    </div>

                                                    <meta itemprop="author" content="Author">
                                                    <meta itemprop="datePublished" content="2015-02-05T08:00"/>
                                                    <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                                </div>
                                            </a>
                                        </article>
                                    </div>
                                <?php } ?>
                            </div>

                        </div>
                        <div class="banner-12p shadow m-b-30">
                            <div class="content flex-center"></div>
                        </div>
                    <?php } ?>
                <?php } ?>
            </main>
            <?php include "include/_right_bar.php" ?>
        </div>

    </div>

</div>

