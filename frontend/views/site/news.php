<?php
/**
 * Created by PhpStorm.
 * User: rahim
 * Date: 8/14/2017
 * Time: 15:38
 */
use yii\helpers\Url;

$formatter = \Yii::$app->formatter;
$this->title = $news->first_name .' '. $news->second_name;
$this->registerMetaTag([
    'property' => 'og:url',
    'content' => Url::toRoute(['site/news', 'n_id' => $news->u_id, 'n_slug' => $news->slug]),
]);
$this->registerMetaTag([
    'property' => 'og:title',
    'content' => $news->first_name .' '. $news->second_name,
]);

$this->registerMetaTag([
    'property' => 'og:image',
    'content' => $news->image,
]);

$this->registerMetaTag([
    'property' => 'article:author',
    'content' => $news->author0->first_name .' '. $news->author0->last_name,
]);

$this->registerMetaTag([
    'property' => 'article:publisher',
    'content' => 'https://www.facebook.com/goldnews',
]);

$this->registerMetaTag([
    'property' => 'article:published_time',
    'content' => $formatter->asDate($news->publish_start_date, 'php:Y-m-dTH:i'),
]);
foreach ($news->tags as $news_tag) {
    $this->registerMetaTag([
        'property' => 'article:tag',
        'content' => $news_tag->name,
    ]);
}

?>

<div class="page-wrapper">

    <div class="container p-20">

        <div class="row-15">

            <main class="main-side col-md-9 col-sm-12 col-xs-12 float-left" data-main-col="1">

                <nav class="bg-white shadow border-bottom p-15 m-b-20">

                    <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">

                        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <a href="<?php echo Url::toRoute(['site/index']) ?>" title="goldnews.az - Ana səhifə"
                               itemprop="item">
                                <span itemprop="name">Ana səhifə</span>
                            </a>
                            <meta itemprop="position" content="1">
                        </li>

                        <!--                        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">-->
                        <!--                            <a href="category.php" title="Kateqoriya" itemprop="item">-->
                        <!--                                <span itemprop="name">kateqoriya</span>-->
                        <!--                            </a>-->
                        <!--                            <meta itemprop="position" content="2">-->
                        <!--                        </li>-->

                        <li class="hidden-xs" itemprop="itemListElement" itemscope
                            itemtype="http://schema.org/ListItem">
                            <a href="<?php echo Url::toRoute(['site/news', 'n_id' => $news->u_id, 'n_slug' => $news->slug]) ?>"
                               title="<?= $news->first_name ?> <?= $news->second_name ?>" itemprop="item">
                                <span itemprop="name">
                                    <?= $news->first_name ?> <?= $news->second_name ?>
                                </span>
                            </a>
                            <meta itemprop="position" content="3">
                        </li>

                    </ol>
                </nav>

                <article class="article shadow bg-white m-b-20 clear" itemscope
                         itemtype="http://schema.org/NewsArticle">

                    <div class="article-right-side" itemprop="mainEntityOfPage">

                        <header class="article-header">
                            <h1 class="title m-t-20 m-b-20" itemprop="headline">
                                <span class="prefix-name"><?= $news->first_name ?></span>
                                <span class="suffix-name"><?= $news->second_name ?></span>
                            </h1>

                            <figure class="thumb" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">

                                <img src="<?= $news->image; ?>" alt="alt" width="850" height="500">

                                <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                <meta itemprop="width" content="850">
                                <meta itemprop="height" content="500">

                            </figure>

                            <div class="article-info p-r-20">

                                <span class="m-r-15"><i class="icon-play-thin"></i></span>

                                <a href="category.php" class="m-r-15" title="<?= $news->category->name ?>"
                                   itemprop="genre"><?= $news->category->name ?></a>

                                <span><i class="icon-eye"></i> <?= $news->read_count ?></span>

                            </div>
                            </br>
                            <?php if (!empty($news->short_text)) { ?>
                                <h2 class="description m-t-20 m-b-20" itemprop="description">
                                    <?= $news->short_text; ?>
                                </h2>
                            <?php } ?>

                        </header>

                        <div class="article-body m-b-20" itemprop="articleBody">

                            <div class="article-content">
                                <?= $news->full_text ?>
                            </div>

                        </div>

                        <footer class="article-footer p-t-20 p-b-20">

                            <div class="article-tags clear m-b-20" itemprop="keywords">
                                <span><i class="icon-tags"></i> Açar sözlər:</span>
                                <?php foreach ($news->tags as $news_tag) { ?>
                                    <a href="<?php echo Url::toRoute(['site/tags', 't_id' => $news_tag->id, 't_name' => $news_tag->name]) ?>"
                                       class="tr-3s" rel="tag">
                                        #<?= $news_tag->name ?>
                                    </a>
                                <?php } ?>
                            </div>

                            <div class="bg-gray p-10 clear">

                                <div class="row-7">

                                    <div class="article-author col-md-6 col-sm-6 col-xs-12 clear" itemprop="publisher"
                                         itemscope itemtype="https://schema.org/Organization">

                                        <div class="thumb" itemprop="logo" itemscope
                                             itemtype="https://schema.org/ImageObject">
                                            <img src="/static/main-base/assets/images/favicon.png" alt="user image"
                                                 width="400" height="400">

                                            <link itemprop="url" href="https://goldnews.az/images/favicon.png">
                                            <meta itemprop="width" content="400">
                                            <meta itemprop="height" content="400">

                                        </div>

                                        <strong class="name" itemprop="name">Goldnews.az</strong>

                                    </div>

                                    <!--                                    <div class="col-md-4 col-sm-5 col-xs-12 clear float-right">-->
                                    <!--                                        <button class="btn btn-block btn-lg bg-info follow-button" data-target-modal="newsletter-modal">Abonə olun</button>-->
                                    <!--                                    </div>-->

                                </div>

                            </div>

                            <meta itemprop="author" content="Eldar Zeynallı">
                            <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                            <meta itemprop="isFamilyFriendly" content="true">

                        </footer>

                    </div>

                    <div class="article-left-side p-20 p-b-15">
                        <div class="fixed-side">
                            <div class="share-button clear">
                                <label class="m-b-15 hidden-sm hidden-xs"><i class="icon-share"></i> Paylaş</label>
                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $_SERVER['SERVER_NAME'] ?><?php echo Url::toRoute(['site/news', 'n_id' => $news->u_id, 'n_slug' => $news->slug]) ?>"
                                   class="fb tr-3s"><i class="icon-social-facebook"></i></a>
                                <a href="https://twitter.com/home?status=<?= $news->first_name ?> <?= $news->second_name ?>"
                                   class="tw tr-3s"><i class="icon-social-twitter"></i></a>
                                <a href="https://plus.google.com/share?url=<?= $_SERVER['SERVER_NAME'] ?><?php echo Url::toRoute(['site/news', 'n_id' => $news->u_id, 'n_slug' => $news->slug]) ?>"
                                   class="gp tr-3s"><i class="icon-social-google-plus"></i></a>
                                <a href="<?= $_SERVER['SERVER_NAME'] ?><?php echo Url::toRoute(['site/news', 'n_id' => $news->u_id, 'n_slug' => $news->slug]) ?>"
                                   class="wp tr-3s visible-xs"><i class="icon-social-whatsapp"></i></a>
                                <!--                                <a href="#" class="email tr-3s"><i class="icon-envelope"></i></a>-->
                                <!--                                <a href="#" class="print tr-3s hidden-xs"><i class="icon-print-1"></i></a>-->
                            </div>
                        </div>
                    </div>

                </article>

                <div class="comment-panel bg-white shadow m-b-20">

                    <h2 class="content-title p-15 m-0 border-bottom"><i class="icon-comment"></i> Xəbərlə bağlı rəy
                        bildirin.</h2>

                    <div class="p-20">
                        <div id="comments" class="comments">
                            <div class="fb-comments " width="100%"
                                 data-href="<?= $_SERVER['SERVER_NAME'] ?><?php echo Url::toRoute(['site/news', 'n_id' => $news->u_id, 'n_slug' => $news->slug]) ?>"></div>
                        </div>
                    </div>

                </div>

                <div class="news-list">

                    <h2 class="content-title shadow bg-white p-15 m-t-0 m-b-20">
                        <a href="category.php" title="Digər foto xəbərlər">
                            <i class="icon-newspaper-2"></i>
                            Bənzər xəbərlər
                            <i class="icon-circle-right float-right"></i>
                        </a>
                    </h2>

                    <div class="row-7">
                        <?php foreach ($related_newses as $related_news) { ?>
                            <div class="item-col col-md-4 col-sm-4 col-xs-12">

                                <article class="item bg-white shadow m-b-20 relative clear" itemscope
                                         itemtype="http://schema.org/NewsArticle">

                                    <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                    <a href="<?php echo Url::toRoute(['site/news', 'n_id' => $related_news->u_id, 'n_slug' => $related_news->slug]) ?>"
                                       title="<?= $related_news->first_name ?> <?= $related_news->second_name ?>"
                                       itemprop="url">

                                        <div class="thumb hover" itemprop="image" itemscope
                                             itemtype="http://schema.org/ImageObject">
                                            <img src="<?= $related_news->image; ?>"
                                                 alt="alt" width="640" height="360">
                                            <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                            <meta itemprop="width" content="640">
                                            <meta itemprop="height" content="360">
                                        </div>

                                        <div class="info p-15">

                                            <div class="meta-data line-camp line-1">
                                                <i class="icon-photo-gallery type m-r-5"></i>

                                                <span class="m-r-10 category"
                                                      itemprop="genre"><?= $related_news->category->name ?></span>

                                                <time datetime="2017-06-26T16:40" itemprop="datePublished">
                                                    <?php echo $formatter->asDate($related_news->publish_start_date, 'php:d F  H:i');?>
                                                </time>
                                            </div>

                                            <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">
                                                <span class="prefix-name"><?= $related_news->first_name ?></span>
                                                <span class="suffix-name"><?= $related_news->second_name ?></span>
                                            </h3>

                                            <div itemprop="publisher" itemscope
                                                 itemtype="https://schema.org/Organization">
                                                <div itemprop="logo" itemscope
                                                     itemtype="https://schema.org/ImageObject">
                                                    <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                    <meta itemprop="width" content="400">
                                                    <meta itemprop="height" content="400">
                                                </div>
                                                <meta itemprop="name" content="goldnews.az">
                                            </div>

                                            <meta itemprop="author" content="Eldar Zeynallı">
                                            <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                            <meta itemprop="isFamilyFriendly" content="true">

                                        </div>

                                    </a>

                                </article>

                            </div>
                        <?php } ?>
                    </div>

                    <a href="<?php echo Url::toRoute(['site/category', 'c_slug' => $news->category->slug]) ?>"
                       class="show-more shadow bg-white text-center block p-15 m-b-20">Daha çox göstər</a>

                </div>

            </main>

            <?php include "include/_right_bar.php" ?>

        </div>

    </div>

</div>
