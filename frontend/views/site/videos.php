<?php
/**
 * Created by PhpStorm.
 * User: rahim
 * Date: 8/14/2017
 * Time: 16:19
 */
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'Video Xəbərlər';
$this->registerMetaTag([
    'property' => 'og:url',
    'content' => Url::toRoute(['site/multimedias', 'm_slug' => 'video-xeber']),
]);
$this->registerMetaTag([
    'property' => 'og:title',
    'content' => 'Video Xəbərlər',
]);

$this->registerMetaTag([
    'property' => 'og:image',
    'content' => 'static/main-base/assets/images/logos/gold_news.png',
]);

$formatter = \Yii::$app->formatter;
?>
<div class="page-wrapper">

    <div class="container p-20">

        <div class="row-15">

            <main class="main-side col-md-9 col-sm-12 col-xs-12" data-main-col="1">

                <div class="gallery-list">

                    <nav class="bg-white shadow p-15 m-b-20">

                        <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="<?php echo Url::toRoute(['site/index']) ?>" title="goldnews.az - Ana səhifə"
                                   itemprop="item">
                                    <span itemprop="name">Ana səhifə</span>
                                </a>
                                <meta itemprop="position" content="1">
                            </li>
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="<?php echo Url::toRoute(['site/multimedias', 'm_slug' => 'video-xeber']) ?>"
                                   title="Video xəbərlər" itemprop="item">
                                    <span itemprop="name">Video xəbərlər</span>
                                </a>
                                <meta itemprop="position" content="2">
                            </li>
                        </ol>

                    </nav>

                    <div class="row-10">
                        <?php if (isset($multimedias) and !empty($multimedias)) { ?>
                            <?php foreach ($multimedias as $multimedia) { ?>
                                <div class="item-col col-md-6 col-sm-6 col-xs-12">

                                    <div class="item bg-white shadow m-b-20 relative clear" itemscope
                                         itemtype="http://schema.org/VideoObject">

                                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/videos/video-url">

                                        <a href="<?php echo Url::toRoute(['site/multimedia', 'm_slug' => 'video-xeber', 'i_id' => $multimedia->id, 'i_slug' => $multimedia->slug]) ?>"
                                           title="<?= $multimedia->prefix_name ?> <?= $multimedia->suffix_name ?>"
                                           itemprop="url">

                                            <div class="thumb hover">
                                                <i class="icon-player-play"></i>
                                                <img src="<?= $multimedia->image ?>" alt="alt" width="500" height="300"
                                                     itemprop="thumbnailUrl">
                                            </div>

                                            <div class="caption caption-bg p-15">

                                                <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="name">
                                                    <span class="prefix-name"><?= $multimedia->prefix_name ?></span>
                                                    <span class="suffix-name"><?= $multimedia->suffix_name ?></span>
                                                </h2>

                                                <span class="m-r-15"><?= $multimedia->read_count ?> <i
                                                            class="icon-play-thin"></i></span>

                                                <!--                                            <span class="m-r-15">Kateqoriya</span>-->

                                                <time datetime="2017-06-26T16:40">
                                                    <?php echo $formatter->asDate($multimedia->publish_start_date, 'php:d F  H:i');?>
                                                </time>

                                            </div>

                                        </a>

                                        <meta itemprop="description" content="description text">

                                        <meta itemprop="isFamilyFriendly" content="true">
                                        <meta itemprop="datePublished" content="2017-01-01T12:40">
                                        <meta itemprop="uploadDate" content="2017-01-01T12:40">
                                        <meta itemprop="interactionCount" content="2347"/>
                                        <meta itemprop="duration" content="PT6M58S">

                                    </div>

                                </div>
                            <?php } ?>
                        <?php } else { ?>
                            <p class="shadow" style="text-align: center;padding: 10px 5px;background: #fbe3e3;">
                                "Video Xəbər" yoxdur.
                            </p>
                        <?php } ?>
                    </div>

                    <nav class="pagination-col bg-white shadow p-15 m-b-20">
                        <?php echo LinkPager::widget([
                            'pagination' => $pages,
                        ]); ?>
                        <!--                        <a href="#" class="btn br float-left transition-3s disabled hidden-xs">ƏVVƏLKİ</a>-->
                        <!---->
                        <!--                        <div class="clearfix">-->
                        <!--                            <a href="#" class="active">1</a>-->
                        <!--                            <a href="#">2</a>-->
                        <!--                            <a href="#">3</a>-->
                        <!--                            <a href="#">4</a>-->
                        <!--                            <a href="#">5</a>-->
                        <!--                            <a>…</a>-->
                        <!--                            <a href="#">10</a>-->
                        <!--                            <a href="#">11</a>-->
                        <!--                        </div>-->
                        <!---->
                        <!--                        <a href="#" class="btn br float-right transition-3s hidden-xs">SONRAKI</a>-->
                    </nav>

                </div>

            </main>

            <?php include "include/_right_bar.php" ?>

        </div>

    </div>

</div>

