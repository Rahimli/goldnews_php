<?php
/**
 * Created by PhpStorm.
 * User: rahim
 * Date: 8/21/2017
 * Time: 12:27
 */

$formatter = \Yii::$app->formatter;

?>

<article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

    <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

    <a href="<?php echo $formatter->asDate($recent_news->publish_start_date, 'long'); ?>" title="<?= $recent_news->first_name?> <?= $recent_news->second_name?>">

        <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
            <i class="type xs icon-photo"></i>
            <img src="<?= $recent_news->image?>" alt="alt" width="200" height="200" itemprop="url">
            <meta itemprop="width" content="200">
            <meta itemprop="height" content="200">
        </div>

        <div class="info">

            <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">
                <span class="prefix-name"><?= $recent_news->first_name?></span>
                <span class="suffix-name"><?= $recent_news->second_name?></span>
            </h3>

            <!--                                        <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>-->
            <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> 20:15</time>

            <meta itemprop="dateModified" content="2017-01-15T20:15"/>
            <meta itemprop="author" content="Author">
            <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                    <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                    <meta itemprop="width" content="400">
                    <meta itemprop="height" content="400">
                </div>
                <meta itemprop="name" content="goldnews.az">
            </div>

        </div>

    </a>
</article>
