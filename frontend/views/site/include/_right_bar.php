<?php
/**
 * Created by PhpStorm.
 * User: rahim
 * Date: 7/23/2017
 * Time: 10:29
 */

use yii\helpers\Url;

$formatter = \Yii::$app->formatter;
$base_right_recent_newses = Yii::$app->params["base_right_recent_newses"]

?>
<input type="hidden" value="<?php echo Url::toRoute(['site/ajaxnewsletter']) ?>" id="ajax-newsletter-url">
<aside class="col-md-3 col-sm-12 col-xs-12" data-col="1" itemscope itemtype="http://schema.org/WPSideBar">

    <div class="fixed-wrap">

        <div class="row-10">

            <div class="col-lg-12 col-md-12 col-sm-4 hidden-xs m-b-20">
                <div class="banner-100p shadow">
                    <div class="content flex-center"></div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 hidden-sm col-xs-12">

                <div class="banner-20p m-b-10 shadow visible-xs">
                    <div class="content flex-center"></div>
                </div>

                <div class="time-line m-b-20 shadow border-bottom clear">

                    <h2 class="content-title m-0 p-15 border-bottom text-uppercase"><i class="icon-newspaper-2"></i> Xəbər lenti</h2>

                    <div class="content scrl">
                        <?php $i=1 ?>
                        <?php foreach ($base_right_recent_newses as $recent_news) { ?>
                            <article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                                <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />

                                <a href="<?php echo Url::toRoute(['site/news', 'n_id' => $recent_news->u_id, 'n_slug' => $recent_news->slug]); ?>" title="<?= $recent_news->first_name?> <?= $recent_news->second_name?>">

                                    <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                        <i class="type xs icon-photo"></i>
                                        <img src="<?= $recent_news->image?>" alt="alt" width="200" height="200" itemprop="url">
                                        <meta itemprop="width" content="200">
                                        <meta itemprop="height" content="200">
                                    </div>

                                    <div class="info">

                                        <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">
                                            <span class="prefix-name"><?= $recent_news->first_name?></span>
                                            <span class="suffix-name"><?= $recent_news->second_name?></span>
                                        </h3>

<!--                                        <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>-->
                                        <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T<?= $formatter->asDate($recent_news->publish_start_date, 'php: d, F')?>"><i class="icon-clock"></i> <?= $formatter->asDate($recent_news->publish_start_date, 'php: H:s')?></time>

                                        <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                        <meta itemprop="author" content="Author">
                                        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                                <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                                <meta itemprop="width" content="400">
                                                <meta itemprop="height" content="400">
                                            </div>
                                            <meta itemprop="name" content="goldnews.az">
                                        </div>

                                    </div>

                                </a>
                            </article>
                            <?php if ($i==1 or ($i-1)%5==0 or $i==count($base_right_recent_newses)){?>
                                <div class="item border-bottom visible-xs clear">
                                    <div class="banner-20p">
                                        <div class="content flex-center"></div>
                                    </div>
                                </div>
                            <?php }?>
                        <?php $i++?>
                        <?php }?>

                    </div>
                    <a id="scrl-div-loader" style="display: none;" class="item block text-center bg-gray clear">yüklənir....</a>

                </div>

            </div>

            <div class="col-lg-12 col-md-12 col-sm-4 hidden-xs m-b-20">
                <div class="banner-100p shadow">
                    <div class="content flex-center"></div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-4 hidden-xs">
                <div class="banner-100p shadow">
                    <div class="content flex-center"></div>
                </div>
            </div>

        </div>

    </div>

</aside>
