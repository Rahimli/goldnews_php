<?php
/**
 * Created by PhpStorm.
 * User: rahim
 * Date: 8/20/2017
 * Time: 8:05
 */
use yii\helpers\Url;

$this->registerMetaTag([
    'property' => 'og:url',
    'content' => Url::toRoute(['site/page', 'p_slug' => $p_slug]),
]);
$this->registerMetaTag([
    'property' => 'og:title',
    'content' => $title,
]);

$this->registerMetaTag([
    'property' => 'og:image',
    'content' => 'static/main-base/assets/images/logos/gold_news.png',
]);

$formatter = \Yii::$app->formatter;
?>
<div class="page-wrapper">

    <div class="container p-l-20 p-r-20">

        <div class="row-15">

            <div class="main-side col-md-12 col-sm-12 col-xs-12">

                <div class="static-pages m-t-20">

                    <nav class="bg-white shadow p-15 m-b-20">

                        <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="<?= Url::toRoute(['site/index'])?>" title="goldnews.az - Ana səhifə" itemprop="item">
                                    <span itemprop="name">Ana səhifə</span>
                                </a>
                                <meta itemprop="position" content="1">
                            </li>
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="<?= Url::toRoute(['site/page', 'p_slug' => $p_slug])?>" title="<?= $title?>" itemprop="item">
                                    <span itemprop="name"><?= $title?></span>
                                </a>
                                <meta itemprop="position" content="2">
                            </li>
                        </ol>

                    </nav>

                    <div class="row-10">

                        <div class="col-md-3 col-sm-12 col-xs-12 m-b-20">

                            <div class="static-side-menu bg-white shadow p-t-10 p-b-10 clear">

                                <a href="<?php echo Url::toRoute(['site/page', 'p_slug' => 'haqqimizda']) ?>" class="tr-3s <?php if($p_slug=='haqqimizda'){echo 'current-page';}?>"><i class="icon-info-circle-thin"></i> Haqqımızda</a>
<!--                                <a href="contact.php" class="tr-3s"><i class="icon-envelope-o"></i> Bizimlə əlaqə</a>-->
                                <a href="<?php echo Url::toRoute(['site/page', 'p_slug' => 'saytda-reklam']) ?>" class="tr-3s <?php if($p_slug=='saytda-reklam'){echo 'current-page';}?>"><i class="icon-ads"></i> Saytda reklam</a>
                                <a href="<?php echo Url::toRoute(['site/weather']) ?>" class="tr-3s"><i class="wi wi-day-cloudy"></i> Hava</a>
                                <a href="<?php echo Url::toRoute(['site/currency']) ?>" class="tr-3s"><i class="icon-currency-azn"></i> Valyuta</a>

                            </div>

                        </div>

                        <div class="col-md-9 col-sm-12 col-xs-12 m-b-20">

                            <div class="static-page-wrapper shadow bg-white clear">
                                <h1 class="content-title p-20 m-0 border-bottom">
                                    <strong><i class="<?php if($p_slug=='haqqimizda'){echo 'icon-info-circle-thin';}else if($p_slug=='saytda-reklam'){echo 'icon-ads';}?>"></i> <?= $title?></strong>
                                </h1>

                                <div class="clear p-20">
                                    <?= $content?>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

