<?php
/**
 * Created by PhpStorm.
 * User: rahim
 * Date: 8/14/2017
 * Time: 15:18
 */
use yii\helpers\Url;
use yii\widgets\LinkPager;

$formatter = \Yii::$app->formatter;

$this->title = $category->name;
$this->registerMetaTag([
    'property' => 'og:url',
    'content' => Url::toRoute(['site/category', 'c_slug'=>$category->slug]),
]);
$this->registerMetaTag([
    'property' => 'og:title',
    'content' => $category->name,
]);

$this->registerMetaTag([
    'property' => 'og:image',
    'content' => 'static/main-base/assets/images/logos/gold_news.png',
]);
$this->registerMetaTag([
    'property' => 'article:section',
    'content' => $category->name,
]);

?>
<div class="page-wrapper">

    <div class="container p-20">

        <div class="row-15">

            <main class="main-side col-md-12 col-sm-12 col-xs-12">

                <div class="news-list">

                    <nav class="bg-white shadow p-15 m-b-20">

                        <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="<?php echo Url::toRoute(['site/index']) ?>" title="goldnews.az - Ana səhifə" itemprop="item">
                                    <span itemprop="name">Ana səhifə</span>
                                </a>
                                <meta itemprop="position" content="1">
                            </li>
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="<?php echo Url::toRoute(['site/category', 'c_slug' => $category->slug]) ?>" title="<?= $category->name?>" itemprop="item">
                                    <span itemprop="name"><?= $category->name?></span>
                                </a>
                                <meta itemprop="position" content="2">
                            </li>
                        </ol>

                    </nav>

                    <div class="row-7">
                        <?php if(isset($newses) and !empty($newses)) {?>
                        <?php $i = 1; ?>
                        <?php foreach ($newses as $news) { ?>
                            <?php if ($i == 1 or ($i % 8 == 0)) { ?>
                                <div class="item-col col-md-3 col-sm-4 col-xs-12">

                                    <div class="banner-100p shadow m-b-20">
                                        <div class="content flex-center"></div>
                                    </div>

                                </div>
                            <?php } ?>
                            <div class="item-col col-md-3 col-sm-4 col-xs-12">

                                <article class="item bg-white shadow m-b-20 relative clear" itemscope
                                         itemtype="http://schema.org/NewsArticle">

                                    <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                    <a href="<?php echo Url::toRoute(['site/news', 'n_id' => $news->u_id, 'n_slug' => $news->slug]) ?>" title="<?= $news->first_name?> <?= $news->second_name?>" itemprop="url">

                                        <div class="thumb hover" itemprop="image" itemscope
                                             itemtype="http://schema.org/ImageObject">
                                            <img src="<?= $news->image;?>"
                                                 alt="alt" width="640" height="360">
                                            <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                            <meta itemprop="width" content="640">
                                            <meta itemprop="height" content="360">
                                        </div>

                                        <div class="info p-15">

                                            <div class="meta-data line-camp line-1">
                                                <i class="icon-photo-gallery type m-r-5"></i>

                                                <span class="m-r-10 category" itemprop="genre"><?=$category->name?></span>

                                                <time datetime="2017-06-26T16:40" itemprop="datePublished">
                                                    <?php echo $formatter->asDate($news->publish_start_date, 'php:d F  H:i');?>
                                                </time>
                                            </div>

                                            <h3 class="title line-camp line-4 tr-3s m-t-5 m-b-0" itemprop="headline">
                                                <span class="prefix-name"><?= $news->first_name?></span>
                                                <span class="suffix-name"><?= $news->second_name?></span>
                                            </h3>

                                            <div itemprop="publisher" itemscope
                                                 itemtype="https://schema.org/Organization">
                                                <div itemprop="logo" itemscope
                                                     itemtype="https://schema.org/ImageObject">
                                                    <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                                    <meta itemprop="width" content="400">
                                                    <meta itemprop="height" content="400">
                                                </div>
                                                <meta itemprop="name" content="goldnews.az">
                                            </div>

                                            <meta itemprop="author" content="Eldar Zeynallı">
                                            <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                                            <meta itemprop="isFamilyFriendly" content="true">

                                        </div>

                                    </a>

                                </article>

                            </div>
                            <?php $i++; ?>
                        <?php } ?>
                        <?php }else{ ?>
                            <p class="shadow" style="text-align: center;padding: 10px 5px;background: #fbe3e3;">"<?= $category->name ?>" kateqoriyasında xəbər yoxdur.</p>
                        <?php } ?>

                    </div>

                    <nav class="pagination-col bg-white shadow p-15 m-b-20">

                        <?php echo LinkPager::widget([
                            'pagination' => $pages,
                        ]); ?>
<!--                        <a href="#" class="btn br float-left transition-3s disabled hidden-xs">ƏVVƏLKİ</a>-->
<!---->
<!--                        <div class="clearfix">-->
<!--                            <a href="#" class="active">1</a>-->
<!--                            <a href="#">2</a>-->
<!--                            <a href="#">3</a>-->
<!--                            <a href="#">4</a>-->
<!--                            <a href="#">5</a>-->
<!--                            <a>…</a>-->
<!--                            <a href="#">10</a>-->
<!--                            <a href="#">11</a>-->
<!--                        </div>-->
<!---->
<!--                        <a href="#" class="btn br float-right transition-3s hidden-xs">SONRAKI</a>-->
                    </nav>

                </div>

            </main>

        </div>

    </div>

</div>
<?//= ?>
