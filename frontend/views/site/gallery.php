<?php
/**
 * Created by PhpStorm.
 * User: rahim
 * Date: 8/14/2017
 * Time: 16:50
 */

use yii\helpers\Url;

$formatter = \Yii::$app->formatter;

$this->title = $multimedia->prefix_name .' '. $multimedia->suffix_name;
$this->registerMetaTag([
    'property' => 'og:url',
    'content' => Url::toRoute(['site/multimedia', 'm_slug' => 'foto-xeber', 'i_id' => $multimedia->id, 'i_slug' => $multimedia->slug]),
]);
$this->registerMetaTag([
    'property' => 'og:title',
    'content' => $multimedia->prefix_name .' '. $multimedia->suffix_name,
]);

$this->registerMetaTag([
    'property' => 'og:image',
    'content' => $multimedia->image,
]);
?>
<div class="page-wrapper">

    <div class="gallery-post-col shadow relative clear">

        <div class="top-wrapper clear p-t-20">

            <nav class="clear">

                <ol class="breadcrumb white m-b-15" itemscope itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <a href="<?php echo Url::toRoute(['site/index']) ?>" title="goldnews.az - Ana səhifə" itemprop="item">
                            <span itemprop="name">Ana səhifə</span>
                        </a>
                        <meta itemprop="position" content="1">
                    </li>
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <a href="<?php echo Url::toRoute(['site/multimedias','m_slug'=>'video-xeber']) ?>" title="Video xəbərlər" itemprop="item">
                            <span itemprop="name">Video xəbərlər</span>
                        </a>
                        <meta itemprop="position" content="2">
                    </li>
                    <li class="hidden-xs" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <a href="<?php echo Url::toRoute(['site/multimedia','m_slug'=>'video-xeber','i_id'=>$multimedia->id,'i_slug'=>$multimedia->slug]) ?>" title="<?= $multimedia->prefix_name?> <?= $multimedia->suffix_name?>" itemprop="item">
                            <span itemprop="name"><?= $multimedia->prefix_name?> <?= $multimedia->suffix_name?></span>
                        </a>
                        <meta itemprop="position" content="3">
                    </li>
                </ol>

            </nav>

            <div class="row-0 m-b-30" itemscope itemtype="http://schema.org/NewsArticle">

                <div itemprop="mainEntityOfPage">

                    <div class="col-md-8 col-sm-12 col-xs-12">
                        <figure class="thumb" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                            <img src="https://wallpaperbrowse.com/media/images/free_high_resolution_images_for_download-1.jpg" alt="alt" width="850" height="500">

                            <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                            <meta itemprop="width" content="850">
                            <meta itemprop="height" content="500">

                        </figure>
                    </div>

                    <div class="info col-md-4 col-sm-12 col-xs-12">

                        <div class="p-20">

                            <h1 class="title m-t-0 m-b-10" itemprop="headline">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h1>

                            <div class="description" itemprop="description">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    A ab atque consequuntur deserunt ea earum eligendi error illum minus molestiae mollitia nisi quasi,
                                    recusandae rerum similique suscipit tempora vero voluptatum.</p>
                            </div>

                            <div class="clear m-b-10">
<!--                                <span class="m-r-15">Kateqoriya</span>-->
                                <time datetime="2017-07-02T14:00" class="m-r-15">
                                    <?php echo $formatter->asDate($multimedia->publish_start_date, 'php:d F  H:i');?>
                                </time>
                                <span><i class="icon-eye"></i> 3254</span>
                            </div>

<!--                            <div class="gallery-tags clear m-b-15 p-t-15" itemprop="keywords">-->
<!--                                <span class="hidden-xs"><i class="icon-tags"></i> Açar sözlər:</span>-->
<!--                                <a href="#" class="tr-3s" rel="tag">#tag name</a>-->
<!--                                <a href="#" class="tr-3s" rel="tag">#tag name</a>-->
<!--                                <a href="#" class="tr-3s" rel="tag">#tag name</a>-->
<!--                            </div>-->

                            <div class="share-button clear">
                                <a href="#" class="fb tr-3s"><i class="icon-social-facebook"></i></a>
                                <a href="#" class="tw tr-3s"><i class="icon-social-twitter"></i></a>
                                <a href="#" class="gp tr-3s"><i class="icon-social-google-plus"></i></a>
                                <a href="#" class="wp tr-3s visible-xs"><i class="icon-social-whatsapp"></i></a>
                                <a href="#" class="email tr-3s"><i class="icon-envelope"></i></a>
                                <a href="#" class="rss tr-3s hidden-xs"><i class="icon-rss"></i></a>
                            </div>

                            <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                    <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                    <meta itemprop="width" content="400">
                                    <meta itemprop="height" content="400">
                                </div>
                                <meta itemprop="name" content="Planksoft">
                            </div>

                            <meta itemprop="author" content="Eldar Zeynallı">
                            <meta itemprop="dateModified" content="2015-02-05T09:20"/>
                            <meta itemprop="isFamilyFriendly" content="true">

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <div class="next-items-carousel owl-carousel owl-theme m-b-30">

            <div class="item">
                <a href="photo-view.php" title="title">
                    <div class="thumb tr-3s">
                        <img src="http://img.youtube.com/vi/s43o5vLk3jQ/0.jpg" alt="alt" width="" height="">
                    </div>
                </a>
            </div>
            <div class="item">
                <a href="photo-view.php" title="title">
                    <div class="thumb tr-3s">
                        <img src="http://img.youtube.com/vi/s43o5vLk3jQ/0.jpg" alt="alt" width="" height="">
                    </div>
                </a>
            </div>
            <div class="item">
                <a href="photo-view.php" title="title">
                    <div class="thumb tr-3s">
                        <img src="http://img.youtube.com/vi/s43o5vLk3jQ/0.jpg" alt="alt" width="" height="">
                    </div>
                </a>
            </div>
            <div class="item">
                <a href="photo-view.php" title="title">
                    <div class="thumb tr-3s">
                        <img src="http://img.youtube.com/vi/s43o5vLk3jQ/0.jpg" alt="alt" width="" height="">
                    </div>
                </a>
            </div>
            <div class="item">
                <a href="photo-view.php" title="title">
                    <div class="thumb tr-3s">
                        <img src="http://img.youtube.com/vi/s43o5vLk3jQ/0.jpg" alt="alt" width="" height="">
                    </div>
                </a>
            </div>
            <div class="item">
                <a href="photo-view.php" title="title">
                    <div class="thumb tr-3s">
                        <img src="http://img.youtube.com/vi/s43o5vLk3jQ/0.jpg" alt="alt" width="" height="">
                    </div>
                </a>
            </div>
            <div class="item">
                <a href="photo-view.php" title="title">
                    <div class="thumb tr-3s">
                        <img src="http://img.youtube.com/vi/s43o5vLk3jQ/0.jpg" alt="alt" width="" height="">
                    </div>
                </a>
            </div>
            <div class="item">
                <a href="photo-view.php" title="title">
                    <div class="thumb tr-3s">
                        <img src="http://img.youtube.com/vi/s43o5vLk3jQ/0.jpg" alt="alt" width="" height="">
                    </div>
                </a>
            </div>
            <div class="item">
                <a href="photo-view.php" title="title">
                    <div class="thumb tr-3s">
                        <img src="http://img.youtube.com/vi/s43o5vLk3jQ/0.jpg" alt="alt" width="" height="">
                    </div>
                </a>
            </div>

        </div>

    </div>

    <div class="container p-20">

        <div class="row-15">

            <main class="main-side col-md-9 col-sm-12 col-xs-12" data-main-col="1">

                <div class="comment-panel shadow bg-white m-b-20">

                    <h2 class="content-title p-15 m-0 border-bottom"><i class="icon-comment"></i> Xəbərlə bağlı rəy bildirin.</h2>

                    <div class="p-20">
                        <img src="assets/images/comment.jpg" alt="" style="width:100%;">
                    </div>

                </div>

                <div class="gallery-list">

                    <h2 class="content-title shadow bg-white p-15 m-t-0 m-b-20">
                        <a href="photos.php" title="Digər foto xəbərlər">
                            <i class="icon-photo-gallery"></i>
                            Digər foto xəbərlər
                            <i class="icon-circle-right float-right"></i>
                        </a>
                    </h2>

                    <div class="row-10">
                        <?php foreach ($other_multimedias as $other_multimedia){?>
                            <div class="item-col col-md-6 col-sm-6 col-xs-12">

                                <article class="item shadow bg-white m-b-20 relative clear" itemscope itemtype="http://schema.org/NewsArticle">

                                    <link itemprop="mainEntityOfPage" href="http://goldnews.az/news-url">

                                    <a href="<?php echo Url::toRoute(['site/multimedia', 'm_slug' => 'foto-xeber', 'i_id' => $other_multimedia->id, 'i_slug' => $other_multimedia->slug]) ?>" title="<?= $other_multimedia->prefix_name?> <?= $other_multimedia->suffix_name?>" itemprop="url">

                                        <figure class="thumb hover"  itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                                            <i class="icon-photo-gallery"></i>
                                            <img src="<?= $other_multimedia->image ?>" alt="alt" width="500" height="300">

                                            <link itemprop="url" href="https://goldnews.az/images/image.jpg">
                                            <meta itemprop="width" content="500">
                                            <meta itemprop="height" content="300">

                                        </figure>

                                        <div class="caption caption-bg p-15">

                                            <h2 class="title line-camp line-2 m-t-0 m-b-10 tr-3s" itemprop="headline">
                                                <span class="prefix-name"><?= $other_multimedia->prefix_name?></span>
                                                <span class="suffix-name"><?= $other_multimedia->suffix_name?></span>
                                            </h2>

                                            <span class="m-r-15">2 <i class="icon-photo"></i></span>

                                            <time datetime="2017-06-26T16:40" itemprop="datePublished">26 İyun</time>

                                        </div>

                                    </a>

                                    <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                        <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                            <link itemprop="url" href="https://goldnews.az/images/logo.png">
                                            <meta itemprop="width" content="400">
                                            <meta itemprop="height" content="400">
                                        </div>
                                        <meta itemprop="name" content="Planksoft">
                                    </div>

                                    <meta itemprop="author" content="Eldar Zeynallı">
                                    <meta itemprop="description" content="description text">
                                    <meta itemprop="dateModified" content="2015-02-05T09:20"/>

                                </article>

                            </div>
                        <?php }?>

                    </div>

                    <a href="photos.php" class="show-more shadow bg-white text-center block p-15 m-b-20">Daha çox göstər</a>

                </div>

            </main>

            <?php include "include/_right_bar.php" ?>

        </div>

    </div>

</div>

