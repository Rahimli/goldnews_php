<?php
/**
 * Created by PhpStorm.
 * User: rahim
 * Date: 8/15/2017
 * Time: 1:21
 */

use yii\helpers\Url;

$this->title = 'Hava';
$this->registerMetaTag([
    'property' => 'og:url',
    'content' => Url::toRoute(['site/weather']),
]);
$this->registerMetaTag([
    'property' => 'og:title',
    'content' => 'Hava',
]);

$this->registerMetaTag([
    'property' => 'og:image',
    'content' => 'static/main-base/assets/images/logos/gold_news.png',
]);

$formatter = \Yii::$app->formatter;
?>
<div class="page-wrapper">

    <div class="container p-l-20 p-r-20">

        <div class="row-15">

            <div class="main-side col-md-12 col-sm-12 col-xs-12">

                <div class="static-pages m-t-20">

                    <nav class="bg-white shadow p-15 m-b-20">

                        <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="index.php" title="goldnews.az - Ana səhifə" itemprop="item">
                                    <span itemprop="name">Ana səhifə</span>
                                </a>
                                <meta itemprop="position" content="1">
                            </li>
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="category.php" title="Hava" itemprop="item">
                                    <span itemprop="name">Hava</span>
                                </a>
                                <meta itemprop="position" content="2">
                            </li>
                        </ol>

                    </nav>

                    <div class="row-10">

                        <div class="col-md-3 col-sm-12 col-xs-12 m-b-20">

                            <div class="static-side-menu shadow bg-white p-t-10 p-b-10 clear">

                                <a href="<?php echo Url::toRoute(['site/page', 'p_slug' => 'haqqimizda']) ?>"
                                   class="tr-3s"><i class="icon-info-circle-thin"></i> Haqqımızda</a>
                                <!--                                <a href="contact.php" class="tr-3s"><i class="icon-envelope-o"></i> Bizimlə əlaqə</a>-->
                                <a href="<?php echo Url::toRoute(['site/page', 'p_slug' => 'saytda-reklam']) ?>"
                                   class="tr-3s "><i class="icon-ads"></i> Saytda reklam</a>
                                <a href="<?php echo Url::toRoute(['site/weather']) ?>" class="tr-3s current-page"><i
                                            class="wi wi-day-cloudy"></i> Hava</a>
                                <a href="<?php echo Url::toRoute(['site/currency']) ?>" class="tr-3s"><i
                                            class="icon-currency-azn"></i> Valyuta</a>

                            </div>

                        </div>

                        <div class="col-md-9 col-sm-12 col-xs-12 m-b-20">

                            <div class="static-page-wrapper shadow bg-white clear">

                                <h1 class="content-title p-20 m-0 border-bottom"><strong><i
                                                class="wi wi-day-cloudy"></i> HAVA PROQNOZU</strong></h1>

                                <div class="weather">

                                    <div class="header relative"
                                         style="background-image: url(http://www.travelwings.com/special-offers/images/Baku-1.jpg)">

                                        <div class="days row-0">
                                            <?php $i = 1 ?>
                                            <?php foreach ($first_weathers as $first_weather) { ?>
                                                <?php $degrees = explode("/", $first_weather->degrees );?>
                                                <?php if ($i == 1) { ?>

                                                    <div class="day cunnet-day col-md-5 col-sm-5 col-xs-12">
                                                        <div class="day-name p-10">
                                                            <?= $formatter->asDate($first_weather->date, 'php: l')?>
                                                        </div>
                                                        <div class="info-weather p-30">

                                                            <label class="city-name">Bakı</label>

                                                            <h2 class="deg"><b> <?= $degrees[1]?> <sup>o</sup>C</b> <i
                                                                        class="wi wi-day-cloudy"></i></h2>

                                                            <span class="m-r-15"><i class="wi wi-moonrise"></i>  <?= $degrees[0]?>  <sup>o</sup>C</span>
                                                            <span class="m-r-15"><i class="wi wi-strong-wind"></i> <?= $first_weather->wind_speed ?> m/s</span>
                                                            <span><i class="wi wi-humidity"></i> <?= $first_weather->moisture ?>%</span>

                                                        </div>

                                                    </div>
                                                <?php } ?>
                                                <?php if ($i >= 2 and $i <= 7) { ?>
                                                    <?php if ($i == 2) { ?>
                                                        <div class="col-md-7 col-sm-7 col-xs-12">

                                                        <div class="row-0">
                                                    <?php } ?>
                                                    <div class="day next-day col-md-2 col-sm-2 col-xs-4">

                                                        <div class="day-name p-10 text-center"><?= $formatter->asDate($first_weather->date, 'php: l')?></div>

                                                        <div class="info-weather flex-center">

                                                            <span class="icon m-b-20"><i
                                                                        class="wi wi-day-cloudy"></i></span>

                                                            <strong class="deg m-b-20"><b><?= $degrees[0]?><sup>o</sup>C</b></strong>

                                                            <span><i class="wi wi-moonrise"></i> <?= $degrees[1]?> <sup>o</sup>C</span>

                                                        </div>

                                                    </div>
                                                    <?php if ($i == 7) { ?>
                                                        </div>

                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                                <?php $i++ ?>
                                            <?php } ?>


                                        </div>

                                    </div>

                                    <div class="clear p-20">

                                        <h4 class="m-t-0 m-b-20"><b>Digər şəhərlər</b></h4>

                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover">

                                                <thead>

                                                <tr>
                                                    <th>Şəhər</th>
                                                    <th>Atmosfer hadisələri</th>
                                                    <th>Temperatur <sup>o</sup>C</th>
                                                    <th>Külək m/s</th>
                                                    <th>Rütubət %</th>
                                                </tr>

                                                </thead>

                                                <tbody>
                                                <?php foreach ($weathers as $weather) { ?>
                                                    <tr>
                                                        <td><a href="#"><b><?= $weather->city->name ?></b></a></td>
                                                        <td><i class="wi wi-day-cloudy"></i> <?= $weather->status ?>
                                                        </td>
                                                        <td><b><?= $weather->degrees ?></b></td>
                                                        <td>
                                                            <i class="wi wi-strong-wind"></i> <?= $weather->wind_speed ?>
                                                        </td>
                                                        <td><?= $weather->moisture ?></td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>

                                            </table>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

