<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
use \yii\web\Request;
$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());
return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
//        'languageSwitcher',
        ],
    'controllerNamespace' => 'frontend\controllers',
    'language' => 'az-Az',

    // set source language to be English
    'sourceLanguage' => 'az-AZ',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => $baseUrl,
        ],
//        'languageSwitcher' => [
//            'class' => 'common\components\languageSwitcher',
//        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
//        'cache' => [
//            'class' => 'yii\caching\MemCache',
//            'servers' => [
//                [
//                    'host' => '127.0.0.1',
//                    'port' => 11211,
//                    'weight' => 60,
//                ],
//            ],
//        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'baseUrl' => $baseUrl,
            //'class' => 'yii\web\UrlManager',
           'class' => 'codemix\localeurls\UrlManager',
            // Disable index.php
            'showScriptName' => false,
            // Disable r= routes
            'enablePrettyUrl' => true,
            'languages' => ['az-AZ','az', 'en*', 'ru',],
            'rules' => array(
                '/' => 'site/index',
                'page/<p_slug:[\w-]+>/' => 'site/page',
                'search/' => 'site/search',
                'category/<c_slug:[\w-]+>/' => 'site/category',
                'news/<n_id:[\w-]+>/<n_slug:[\w-]+>/read/' => 'site/news',
                'news/<n_id:[\w-]+>/<n_slug:[\w-]+>/prenews/' => 'site/prenews',
                'tag/<t_id:\d+>/<t_name:[\w-]+>' => 'site/tags',
                'ajax-newsletter-url/' => 'site/ajaxnewsletter',
                'multimedias/<m_slug:[\w-]+>/' => 'site/multimedias',
                'multimedias/<m_slug:[\w-]+>/<i_id:\d+>/<i_slug:[\w-]+>' => 'site/multimedia',
                'valyuta' => 'site/currency',
                'hava' => 'site/weather',
                'xml-parse-test/' => 'site/parsetest',
                'parsehtml' => 'site/parsehtml',
                'categoryxml' => 'site/categoryxml',
                'newsxml' => 'site/newsxml',
                'newscategoryxml' => 'site/newscategoryxml',
                'weatherhtml' => 'site/weatherhtml',
            ),
        ],


    ],
    'params' => $params,
];
