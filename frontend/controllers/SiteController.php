<?php

namespace frontend\controllers;

use backend\models\Category;
use backend\models\CategoryNews;
use backend\models\City;
use backend\models\Currency;
use backend\models\Multimedia;
use backend\models\News;
use backend\models\Social;
use backend\models\Tag;
//use frontend\models\HtmlParser;
use backend\models\Weather;
use DateTime;
use DOMDocument;
use function GuzzleHttp\Psr7\str;
use Yii;
use yii\base\InvalidParamException;
use yii\data\Pagination;
use yii\db\Exception;
use yii\db\Expression;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\httpclient\Client;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {

        Yii::$app->params["base_currencies"] = Currency::find()->orderBy('order_index')->limit(3)->all();
        Yii::$app->params["base_first_weather"] = Weather::findOne(['city_id'=>1,'date'=>date('Y-m-d')]);
        Yii::$app->params["base_socials"] = Social::find()->orderBy('order_index')->all();
        Yii::$app->params["base_categories"] = Category::find()->where(['status' => 1])->where(['IS', 'parent', null])->orderBy('order_index')->all();
        Yii::$app->params["base_footer_newses"] = News::find()->where(['is_active' => 1])->orderBy(['publish_start_date' => SORT_DESC])->limit(3)->all();
        Yii::$app->params["base_right_popular_newses"] = News::find()->where(['is_active' => 1])->orderBy(['read_count' => SORT_DESC, 'publish_start_date' => SORT_DESC])->limit(6)->all();
        Yii::$app->params["base_right_recent_newses"] = News::find()->where(['is_active' => 1])->orderBy(['publish_start_date' => SORT_DESC])->limit(16)->all();
        Yii::$app->params["base_right_random_newses"] = News::find()->where(['is_active' => 1, 'show_slider' => 1])->orderBy(new Expression('rand()'))->limit(6)->all();
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }



    public function beforeAction($action)
    {
        if ($action->id == 'error') {
            $this->layout = 'main_base';
        }

//        $cookies = Yii::$app->response->cookies;
//        $cookies->add(new \yii\web\Cookie([
//            'name' => 'language',
//            'value' => 'az'
//        ]));
        return parent::beforeAction($action);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        try{
        $this->layout = 'main_base';
//        $base_categories = Category::find()->where(['status' => 1])->orderBy('order_index')->all();
        $base_slider_newses = News::find()->where(['is_active' => 1, 'show_slider' => 1])->orderBy(['publish_start_date' => SORT_DESC])->limit(5)->all();
        $context = [
            'base_slider_newses' => $base_slider_newses,
        ];
        return $this->render('index', $context);
        }catch (Exception $exception){
            die(var_dump($exception));
        }
    }

    public function actionAjaxnewsletter(){
        if(Yii::$app->request->isAjax && Yii::$app->request->isGet){
            $message_code = 0;
            $formatter = \Yii::$app->formatter;
            sleep(0.5);
            $content = '';
            $pageNO = Yii::$app->getRequest()->getQueryParam('page');
            $offset_number = 16 + (int)$pageNO*8;
//        $query = News::find()->where(['is_active' => 1, 'category_id' => $category->id])->orderBy(['publish_start_date' => SORT_DESC]);
            $query = News::find()->where(['is_active' => 1])->orderBy(['publish_start_date' => SORT_DESC])->offset($offset_number)->limit(8)->all();
            $countQuery = count($query);
//            $pages = new Pagination(['totalCount' => $countQuery->count(),'pageSizeLimit'=>[4, 50],  'pageSize' => 5]);
//            $newses = $query->offset(16 )
//
//                ->all();
//            $content = $pageNO . $offset_number;
            foreach ($query as $recent_news){
                $content = $content . '<article class="item clear border-bottom" itemscope itemtype="http://schema.org/NewsArticle">

                        <link itemprop="mainEntityOfPage" href="http://goldnews.az/article-1" />
                    
                        <a href="'.Url::toRoute(['site/news', 'n_id' => $recent_news->u_id, 'n_slug' => $recent_news->slug]).'" title="'.$recent_news->first_name . $recent_news->second_name.'">
                    
                            <div class="thumb hover" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                <i class="type xs icon-photo"></i>
                                <img src="'. $recent_news->image . '" alt="alt" width="200" height="200" itemprop="url">
                                <meta itemprop="width" content="200">
                                <meta itemprop="height" content="200">
                            </div>
                    
                            <div class="info">
                    
                                <h3 class="title m-0 m-b-5 tr-3s" itemprop="headline">
                                    <span class="prefix-name">'.$recent_news->first_name.'</span>
                                    <span class="suffix-name">'.$recent_news->second_name.'</span>
                                </h3>
                    
                                <!--                                        <span class="meta-data m-r-10" itemprop="genre" title="Category Name">Category Name</span>-->
                                <time class="meta-data" itemprop="datePublished" datetime="2017-01-15T20:15"><i class="icon-clock"></i> '.$formatter->asDate($recent_news->publish_start_date, 'php: d, F').'</time>
                    
                                <meta itemprop="dateModified" content="2017-01-15T20:15"/>
                                <meta itemprop="author" content="Author">
                                <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                        <meta itemprop="url" content="https://goldnews.az/logo.jpg">
                                        <meta itemprop="width" content="400">
                                        <meta itemprop="height" content="400">
                                    </div>
                                    <meta itemprop="name" content="goldnews.az">
                                </div>
                    
                            </div>
                    
                        </a>
                    </article>';
            }
            $message_code = 1;
            $data = array('message_code' => $message_code, 'content' => $content);
            \Yii::$app->response->format = 'json';
            return $data;
        }else{

        }
    }
    public function actionPage($p_slug)
    {
        $this->layout = 'main_base';
//        $base_categories = Category::find()->where(['status' => 1])->orderBy('order_index')->all();
        if ($p_slug == 'saytda-reklam') {
            $title = 'SAYTDA REKLAM';
            $content = '';
        } else if ($p_slug == 'haqqimizda') {
            $title = 'HAQQIMIZDA';
            $content = '';
        } else {
            $this->layout = 'main_base';
            return $this->render('error');
        }
        $context = [
            'p_slug' => $p_slug,
            'title' => $title,
            'content' => $content,
        ];
        return $this->render('page', $context);
    }

    public function actionCategory($c_slug)
    {
        $this->layout = 'main_base';
        $category = Category::findOne(['slug' => $c_slug]);
        if (empty($category)) {
            $this->layout = 'main_base';
            return $this->render('error');
        }
//        $query = News::find()->where(['is_active' => 1, 'category_id' => $category->id])->orderBy(['publish_start_date' => SORT_DESC]);
        $query = News::find()->where(['category_id' => $category->id])
            ->orderBy(['publish_start_date' => SORT_DESC]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 24]);
        $newses = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
//        die(var_dump($pages));
        $context = [
            'category' => $category,
            'newses' => $newses,
            'pages' => $pages,
        ];
        return $this->render('category', $context);
    }

    public function actionRightnews()
    {
        $message_code = 0;
        $content = '';
        if(Yii::$app->request->isAjax && Yii::$app->request->isGet){
//        $query = News::find()->where(['is_active' => 1, 'category_id' => $category->id])->orderBy(['publish_start_date' => SORT_DESC]);
        $query = News::find()->where(['is_active' => 1])
            ->orderBy(['publish_start_date' => SORT_DESC]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(),'pageSizeLimit'=>[4, 50],  'pageSize' => 5]);
        $newses = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        foreach ($newses as $news){
            $content = $content . $this->render('include/right_bar_part', [
                'news' => $news,
            ]);
        }
        $message_code = 1;
            $data = array('message_code' => $message_code, 'context' => $content);
            \Yii::$app->response->format = 'json';
            return $data;
        }
    }

    public function actionMultimedias($m_slug)
    {
        $this->layout = 'main_base';
//        if(empty($category)){
//            $this->layout = 'main_base';
//            return $this->render('error');
//        }
        if ($m_slug == "video-xeber") {
            $page = "videos";
            $type = "video_news";
        } else if ($m_slug == "foto-xeber") {
            $page = "galleries";
            $type = "gallery_news";
//            die($page);
        } else {
            $this->layout = 'main_base';
            return $this->render('error');
        }
        $query = Multimedia::find()->where(['is_active' => 1, 'type' => $type])->orderBy(['publish_start_date' => SORT_DESC]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 12]);
        $multimedias = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        $context = [
            'multimedias' => $multimedias,
            'pages' => $pages,
        ];

        return $this->render($page, $context);
    }

    public function actionMultimedia($m_slug, $i_id, $i_slug)
    {
        $this->layout = 'main_base';
//        if(empty($category)){
//            $this->layout = 'main_base';
//            return $this->render('error');
//        }
        $multimedia = Multimedia::findOne(['is_active' => 1, 'id' => $i_id, 'slug' => $i_slug]);
//        echo $multimedia->prefix_name;
        if ($m_slug == "video-xeber" and $multimedia->type == "video_news") {
            $page = "video";
            $other_multimedias = Multimedia::find()->where(['is_active' => 1, 'type' => 'video_news'])->orderBy(['publish_start_date' => SORT_DESC])->limit(6)->all();
        } else if ($m_slug == "foto-xeber" and $multimedia->type == "gallery_news") {
            $page = "gallery";
            $other_multimedias = Multimedia::find()->where(['is_active' => 1, 'type' => 'gallery_news'])->orderBy(['publish_start_date' => SORT_DESC])->limit(6)->all();
        } else {
            $this->layout = 'main_base';
            return $this->render('error');
        }

        $context = [
            'multimedia' => $multimedia,
            'other_multimedias' => $other_multimedias,
        ];
        return $this->render($page, $context);
    }

    public function actionNews($n_id, $n_slug)
    {
        $this->layout = 'main_base';
        $news = News::find()->where(['u_id' => $n_id, 'slug' => $n_slug, 'is_active' => 1])->one();
        $related_newses = News::find()->where(['is_active' => 1, 'category_id' => $news->category_id])->andWhere(['<>', 'id', $news->id])->limit(6)->all();
//        die(var_dump(count($related_newses)));
        $context = [
            'news' => $news,
            'related_newses' => $related_newses,
        ];
        return $this->render('news', $context);
    }

    public function actionSearch()
    {
//        $get_current_language = Yii::$app->language;
        $search = Yii::$app->getRequest()->getQueryParam('query');
        if ($search != strip_tags($search) or $search != htmlspecialchars($search, $flags = ENT_COMPAT | ENT_HTML401, $encoding = ini_get("default_charset"), $double_encode = true)) {
            htmlspecialchars($search, $flags = ENT_COMPAT | ENT_HTML401, $encoding = ini_get("default_charset"), $double_encode = true);
            //            $search = htmlspecialchars(htmlentities(urlencode(urldecode($search))));
            //            die(utf8_decode(urldecode($search)));
            $search = htmlspecialchars(htmlspecialchars(urlencode($search)));

            $context = [
//            'search' => $search,
            ];
//        $search = filter_var($search, FILTER_SANITIZE_STRING);
        } else {
//            die('sas'.strlen($search).'sasa');
            $search = htmlspecialchars(strip_tags($search));
            if (!empty($search) && strlen(trim($search, " ")) > 0) {
//                die('dsdd');
                $query = News::find()->andFilterWhere([
                    'or',
                    ['like', 'first_name', $search],
                    ['like', 'second_name', $search],
                    ['like', 'full_text', $search],
                    ['like', 'short_text', $search]
                ])->andWhere(['is_active' => 1])->orderBy(['publish_start_date' => SORT_DESC]);
                $countQuery = clone $query;
                $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 8]);
                $newses = $query->offset($pages->offset)
                    ->limit($pages->limit)
                    ->all();
                $context = [
                    'newses' => $newses,
                    'search' => $search,
                    'pages' => $pages,
                ];
            } else {
                $context = [
//            'data_years' => $data_years,
                ];
            }
        }
        $this->layout = 'main_base';

        return $this->render('search', $context);
    }


    public function actionTags($t_id, $t_name)
    {
        $this->layout = 'main_base';
        $tag = Tag::findOne(['id' => $t_id]);
        if (empty($tag)) {
            $this->layout = 'main_base';
            return $this->render('error');
        }
//        $query = News::find()->where(['is_active' => 1, 'category_id' => $category->id])->orderBy(['publish_start_date' => SORT_DESC]);
        $query = News::find()
            ->select('news.*')
            ->innerJoin('news_to_tag', '`news`.`id` = `news_to_tag`.`news_id`')
            ->where(['news_to_tag.tag_id' => $tag->id, 'news.is_active' => 1])
            ->orderBy(['publish_start_date' => SORT_DESC]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 24]);
        $newses = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
//        die(var_dump($pages));
        $context = [
            'tag' => $tag,
            'newses' => $newses,
            'pages' => $pages,
        ];
        return $this->render('search', $context);
    }

    public function actionCurrency()
    {
        $this->layout = 'main_base';
        $currencies = Currency::find()->all();
        $context = [
            'currencies' => $currencies,
//            'related_newses' => $related_newses,
        ];
        return $this->render('currency', $context);
    }

    public function actionWeather()
    {
        $this->layout = 'main_base';
        $first_weathers = Weather::find()->where(['city_id'=>1])->limit(7)->orderBy(['date'=>SORT_ASC])->all();
        $weathers = Weather::find()->where(['date'=>date('Y-m-d')])->all();
//        die(var_dump(count($weathers)));
        $context = [
            'weathers' => $weathers,
            'first_weathers' => $first_weathers,
        ];
        return $this->render('weather', $context);
    }


    public function actionPrenews($n_id, $n_slug)
    {
        $this->layout = 'main_base';
//        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == 1 or Yii::$app->user->identity->is_superuser )) {
            $news = News::find()->where(['u_id' => $n_id, 'slug' => $n_slug])->one();
            $related_newses = News::find()->where(['is_active' => 1, 'category_id' => $news->category_id])->andWhere(['<>', 'id', $news->id])->limit(3)->all();
//        die(var_dump(count($related_newses)));
            $context = [
                'news' => $news,
                'related_newses' => $related_newses,
            ];
            return $this->render('news', $context);
//        } else {
//            if (!Yii::$app->user->isGuest) {
//                Yii::$app->user->logout();
//            }
//            return $this->goHome();
//        }
    }

    public function actionParsehtml()
    {
// Retrieve the DOM from a given URL
        require_once 'HtmlParser.php';
        $html = file_get_html('https://report.az/valyuta/');
//        die(var_dump($html->find('tr.currency-table__row')));
        $currency_tables = $html->find('tr.currency-table__row');
// Find all "A" tags and print their
        $j = 1;
        foreach ($currency_tables as $currency_table) {
            if ($j <= 49) {
//            die(var_dump($currency_table));
                $i = 1;
                $currency_new = new Currency();
                foreach ($currency_table->find('td.currency-table__cell') as $currency_table_item) {
//                die($currency_table_item);
                    if ($i <= 3) {
//                    die( $i . ' - ' . $currency_table_item->outertext . '</br>');
                        if ($i == 1) {
                            $currency_new->name = (string)$currency_table_item->plaintext;
                        }
                        if ($i == 2) {
                            $currency_new->nominal = (string)$currency_table_item->plaintext;
                        }
                        if ($i == 3) {
                            $currency_new->value = (string)$currency_table_item->plaintext;
                        }
                    }
                    if ($i == 4) {
                        $currency_new->status = 2;
                        $class = $currency_table_item->find('span', 0)->class;
                        echo $i . ' - ' . $class . '</br>';
                        if ($class == 'currency-table__cell-index cell-index__down') {
                            $currency_new->status = 2;
                        }
                        if ($class == 'currency-table__cell-index cell-index__neitral') {
                            $currency_new->status = 0;
                        }
                        if ($class == 'currency-table__cell-index cell-index__up') {
                            $currency_new->status = 1;
                        }
                    }
                    $i += 1;
                }

                $currency_new->order_index = $j;
                $currency_new->save();
                $save = $currency_new->save();
                if ($save) {
                    echo '</br>*******************************</br>';
                } else {
                    echo '</br>%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%</br>';
                }

            }
            echo '</br>' . $j . '</br>';
            $j += 1;
        }
    }
//        die();
//    }
    public function actionWeatherhtml()
    {
// Retrieve the DOM from a given URL
        require_once 'HtmlParser.php';
        $html = file_get_html('https://report.az/hava/bak%C4%B1/');
//        die(var_dump($html->find('tr.currency-table__row')));
        $currency_tables = $html->find('tr.weather-table__row ');
// Find all "A" tags and print their
        $j = 1;
        $cities = City::find()->orderBy('order_index')->all();
        foreach ($cities as $city) {
            $date = date('Y-m-d');
            foreach ($currency_tables as $currency_table) {
//            if($j<=10){
//            die(var_dump($currency_table));
                $i = 1;
                $weather_new = new Weather();
                $weather_new->city_id = $city->id;
//                die('sas');
                foreach ($currency_table->find('td') as $currency_table_item) {
                    if($i==2) {
//                        $weather_new->icon = (string)$currency_table_item->find('img', 0)->src
                        $weather_new->icon = (string)$currency_table_item->find('img', 0)->src;
                        $weather_new->status = (string)$currency_table_item->find('span', 0)->plaintext;
//                        echo $i. ' - salam, ';
                        echo $currency_table_item->find('span', 0)->plaintext;
                        echo $currency_table_item->find('img', 0)->src;
                    }
                    if($i==3){
                        echo $currency_table_item->plaintext;
                        $weather_new->degrees = (string)$currency_table_item->plaintext;
                    }
                    if($i==4){
                        echo $currency_table_item->plaintext;
                        $weather_new->wind_speed = (string)$currency_table_item->plaintext;
                    }
                    if($i==5){
                        echo $currency_table_item->plaintext;
                        $weather_new->moisture = (string)$currency_table_item->plaintext;
                    }
                    //                    if($i==2){
//                        $currency_new->nominal = (string)$currency_table_item->plaintext;
//                    }
//                    if($i==3){
//                        $currency_new->value = (string)$currency_table_item->plaintext;
//                    }
//
//                    $currency_new->status = 2;
//                    $class = $currency_table_item->find('span', 0)->class;
//                    echo $i . ' - ' . $class . '</br>';
//                    if ($class == 'currency-table__cell-index cell-index__down') {
//                        $currency_new->status = 2;
//                    }
//                    if ($class == 'currency-table__cell-index cell-index__neitral') {
//                        $currency_new->status = 0;
//                    }
//                    if ($class == 'currency-table__cell-index cell-index__up') {
//                        $currency_new->status = 1;
//                    }
//                    $i += 1;
//            }
//
//                $currency_new->order_index = $j;
//                $currency_new->save();
//                $save = $currency_new->save();
//            if($save){
//                echo '</br>*******************************</br>';
//            }else{
//                echo '</br>%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%</br>';
//            }

//            }
//            echo '</br>'.$j.'</br>';
//            $j +=1;
                    echo '</br>';
                    $i+=1;
                }


                $weather_new->date=$date;
                $save = $weather_new->save();
                if ($save) {
                    echo '</br>*******************************</br>';
                } else {
                    echo '</br>%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%</br>';
//                    die(print_r($weather_new->getErrors()));
//                    echo '</br>%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%</br>';
                }

                echo '</br>$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$</br>';
                echo $date;
                echo '</br>$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$</br>';
                $date = date('Y-m-d', strtotime($date . ' +1 day'));
//                die('</br>haha') ;
            }
            die('</br>haha') ;
        }
//        die($i);
    }
//        die();
//    }
    public function actionParsetest()
    {
        $this->layout = 'main_base';
//        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
        $url = 'http://cbar.az/currencies/31.07.2017.xml';
        $xml = simplexml_load_file($url) or die("feed not loading");
        $currencies = $xml->ValType;
//        for ($i=0; $i<39;$i++){
//            echo '<br>*************************************************************************************<br>';
//            $s_i = (string)$i;
//            if (isset($currencies->Valute[$i]->Nominal)) {
//                echo $currencies->Valute[$i]->Name.'---ID:'.$currencies->Currency[$i]->Value;
//            }
//            echo '</br>************************************************************************************<br>';
//        }
//        echo '<br>BREAK HTML';
        for ($vt = 0; $vt < 2; $vt++) {
            for ($va = 0; $va < 45; $va++) {
                if (isset($currencies[$vt]->Valute[$va]->Nominal)) {
                    echo $currencies[$vt]->Valute[$va]['Code'][0] . '********' . $currencies[$vt]->Valute[$va]->Nominal . '********' . $currencies[$vt]->Valute[$va]->Name . '********' . $currencies[$vt]->Valute[$va]->Value;
                    echo " <br>";
                }
            }
        }
        echo $currencies[0]['Type'];
//            $client = new Client(['baseUrl' => 'http://cbar.az/currencies/31.07.2017.xml',
//                'requestConfig' => [
//                    'format' => Client::FORMAT_XML
//                ],
//                'responseConfig' => [
//                    'format' => Client::FORMAT_XML
//                ],]);
//            $response = $client->createRequest()
////                ->setHeaders(['content-type' => 'application/json'])
////                ->setUrl('articles/search')
//                ->addHeaders(['content-type' => 'application/json'])
//                ->setContent('{query_string: "USD"}')
//
//                ->send();
//
//            echo 'Search results:<br>';
////            echo $response->content;
//            echo '<br/>';
//            die(var_dump($response));
//        }else{
//            if(!Yii::$app->user->isGuest){
//                Yii::$app->user->logout();
//            }
//            return $this->goHome();
    }
//    public function actionParsehtml()
//    {
//// Retrieve the DOM from a given URL
//        $html = file_get_html('https://davidwalsh.name/');
//
//// Find all "A" tags and print their HREFs
//        foreach($html->find('a') as $e)
//            echo $e->href . '<br>';
//    }
//    public function actionParsetest()
//    {
//        $this->layout = 'main_base';
////        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
//        $url = 'http://cbar.az/currencies/31.07.2017.xml';
//        $xml = simplexml_load_file($url) or die("feed not loading");
//        $currencies = $xml->ValType;
////        for ($i=0; $i<39;$i++){
////            echo '<br>*************************************************************************************<br>';
////            $s_i = (string)$i;
////            if (isset($currencies->Valute[$i]->Nominal)) {
////                echo $currencies->Valute[$i]->Name.'---ID:'.$currencies->Currency[$i]->Value;
////            }
////            echo '</br>************************************************************************************<br>';
////        }
////        echo '<br>BREAK HTML';
//        for ($vt = 0; $vt < 2; $vt++) {
//            for ($va = 0; $va < 45; $va++) {
//                if (isset($currencies[$vt]->Valute[$va]->Nominal)) {
//                    echo $currencies[$vt]->Valute[$va]['Code'][0].'********'.$currencies[$vt]->Valute[$va]->Nominal.'********'.$currencies[$vt]->Valute[$va]->Name.'********'.$currencies[$vt]->Valute[$va]->Value;
//                    echo " <br>";
//                }
//            }
//        }
//        echo $currencies[0]['Type'];
////            $client = new Client(['baseUrl' => 'http://cbar.az/currencies/31.07.2017.xml',
////                'requestConfig' => [
////                    'format' => Client::FORMAT_XML
////                ],
////                'responseConfig' => [
////                    'format' => Client::FORMAT_XML
////                ],]);
////            $response = $client->createRequest()
//////                ->setHeaders(['content-type' => 'application/json'])
//////                ->setUrl('articles/search')
////                ->addHeaders(['content-type' => 'application/json'])
////                ->setContent('{query_string: "USD"}')
////
////                ->send();
////
////            echo 'Search results:<br>';
//////            echo $response->content;
////            echo '<br/>';
////            die(var_dump($response));
////        }else{
////            if(!Yii::$app->user->isGuest){
////                Yii::$app->user->logout();
////            }
////            return $this->goHome();
//    }
//
//


    public function actionCategoryxml()
    {
        $this->layout = 'main_base';
//        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
        $url = 'http://goldnews.dev/media/dump_nn.xml';
        $xml = simplexml_load_file($url) or die("feed not loading");
        $categories = $xml->records;
//        die(var_dump($categories->row[1]));

//        echo '<br>BREAK HTML';
//        for ($vt = 0; $vt < 2; $vt++) {
        for ($va = 0; $va < 32; $va++) {
            $n_category = new Category();
            $id = 0;
            $name = '';
            $order_index = 0;
            $unique_name = '';
            $description = '';
            $publish_start_date = '';
            $date = '';
            $slug = '';
            $author_id = 0;
            $parent_id = 0;
            for ($vf = 0; $vf < 17; $vf++) {
                if (isset($categories->row[$va]->column[$vf]['name'])) {
//                    echo $categories->row[$va]->column[$vf]['name'].'********'.$currencies[$vt]->Valute[$va]->Nominal.'********'.$currencies[$vt]->Valute[$va]->Name.'********'.$currencies[$vt]->Valute[$va]->Value;
//                    echo " <br>";
                    switch ($categories->row[$va]->column[$vf]['name']) {
                        case 'id':
                            $id = $categories->row[$va]->column[$vf];
                            break;
                        case 'order_index':
                            $order_index = $categories->row[$va]->column[$vf];
                            break;
                        case 'name':
                            $name = $categories->row[$va]->column[$vf];
                            break;
                        case 'unique_name':
                            $unique_name = $categories->row[$va]->column[$vf];
                            break;
                        case 'description':
                            $description = $categories->row[$va]->column[$vf];
                            break;
                        case 'publish_start_date':
                            $publish_start_date = new DateTime($categories->row[$va]->column[$vf]);
                            break;
                        case 'date':
                            $date = new DateTime($categories->row[$va]->column[$vf]);
                            break;
                        case 'slug':
                            $slug = $categories->row[$va]->column[$vf];
                            break;
                        case 'author_id':
                            $author_id = $categories->row[$va]->column[$vf];
                            break;
                        case 'parent_id':
                            $parent_id = $categories->row[$va]->column[$vf];
                            break;
                    }

                }
            }
            $s = 'id - ' . $id . ' *** ' . 'name- ' . $name . ' *** ' . 'order_index - ' . intval($order_index) . ' *** ' . 'id' . $id . ' *** ' . 'id' . $id . ' *** ';
//            die($date->format('Y-m-d H:i:s'));
//            die($name);
//                $date =

            $n_category->id = $id;

            $n_category->parent = intval($parent_id);
            $n_category->status = 1;
            $n_category->order_index = intval($order_index);
            $n_category->name = (string)$name;
            $n_category->unique_name = (string)$unique_name;
            $n_category->logo = '';
            $n_category->author = 1;
            $n_category->description = (string)$description;
            if (empty($publish_start_date)) {
                $publish_start_date = date('Y-m-d H:i:s');
            } else {
                $publish_start_date = $publish_start_date->format('Y-m-d H:i:s');
            }
            if (empty($date)) {
                $date = date('Y-m-d H:i:s');
            } else {
                $date = $date->format('Y-m-d H:i:s');
            }
            $n_category->publish_start_date = $publish_start_date;
            $n_category->publish_end_date = '';
            $n_category->news_background = '';
            $n_category->suffix_color = '';
            $n_category->prefix_color = '';
            $n_category->date = $date;
            $n_category->news_count = 0;
            $n_category->slug = (string)$slug;
            $save = $n_category->save();
            if (($save)) {
                echo 'ela';
            } else {
                die($date);
                echo 'olmadi';
//                    die(print_r($n_category->getErrors()) . 'asa' );
            }
        }
//        }
//        echo $currencies[0]['Type'];

    }


    public function actionNewsxml()
    {
        $this->layout = 'main_base';
//        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
        $url = 'http://goldnews.dev/xmls/xml/8-25/newses.xml';
        $xml = simplexml_load_file($url) or die("feed not loading");
        $categories = $xml->records;
//        die(var_dump($categories->row[1]));

//        echo '<br>BREAK HTML';
//        for ($vt = 0; $vt < 2; $vt++) {
        $ids = '';
        for ($va = 0; $va < 76; $va++) {

            $n_news = new News();
            $id = 0;
            $first_name = '';
            $second_name = '';
            $show_slider = 0;
            $short_text = '';
            $full_text = '';
            $meta_keywords = '';
            $meta_description = '';
            $read_count = '';
            $author_id = '';
            $image = '';
            $updated_by_user_id = '';
            $publish_start_date = '';
            $date = '';
            $slug = '';
            $author_id = 0;
//                $parent_id = 0;
            for ($vf = 0; $vf < 17; $vf++) {
                if (isset($categories->row[$va]->column[$vf]['name'])) {
//                    echo $categories->row[$va]->column[$vf]['name'].'********'.$currencies[$vt]->Valute[$va]->Nominal.'********'.$currencies[$vt]->Valute[$va]->Name.'********'.$currencies[$vt]->Valute[$va]->Value;
//                    echo " <br>";
                    switch ($categories->row[$va]->column[$vf]['name']) {
                        case 'id':
                            $id = $categories->row[$va]->column[$vf];
                            break;
                        case 'first_name':
                            $first_name = $categories->row[$va]->column[$vf];
                            break;
                        case 'second_name':
                            $second_name = $categories->row[$va]->column[$vf];
                            break;
                        case 'show_slider':
                            $show_slider = $categories->row[$va]->column[$vf];
                            break;
                        case 'short_text':
                            $short_text = $categories->row[$va]->column[$vf];
                            break;
                        case 'full_text':
                            $full_text = $categories->row[$va]->column[$vf];
                            break;
                        case 'image':
                            $image = $categories->row[$va]->column[$vf];
                            break;
                        case 'meta_keywords':
                            $meta_keywords = $categories->row[$va]->column[$vf];
                            break;
                        case 'meta_description':
                            $meta_description = $categories->row[$va]->column[$vf];
                            break;
                        case 'read_count':
                            $read_count = $categories->row[$va]->column[$vf];
                            break;
                        case 'publish_start_date':
                            $publish_start_date = new DateTime($categories->row[$va]->column[$vf]);
                            break;
                        case 'date':
                            $date = new DateTime($categories->row[$va]->column[$vf]);
                            break;
                        case 'slug':
                            $slug = $categories->row[$va]->column[$vf];
                            break;
                        case 'author_id':
                            $author_id = $categories->row[$va]->column[$vf];
                            break;
                        case 'updated_by_user_id':
                            $updated_by_user_id = $categories->row[$va]->column[$vf];
                            break;
                    }

                }
            }
//            $s =  'id - '. $id . ' *** '.'name- '. $name . ' *** '.'order_index - '. intval($order_index) . ' *** '.'id'. $id . ' *** '.'id'. $id . ' *** ';
//            die($date->format('Y-m-d H:i:s'));
//            die($name);
//                $date =
            $ids = $ids . ',\'' . $id . '\'';
            $n_news->u_id = (string)$id;
            $n_news->category_id = 0;
            $n_news->is_active = 1;
            if (empty($author_id)) {
                $author_id = 1;
            }
            if (is_numeric($updated_by_user_id)) {
                $updated_by_user_id = intval($updated_by_user_id);
            } else {
                $updated_by_user_id = null;
            }
            $n_news->author = intval($author_id);
            $n_news->updated_by_user = $updated_by_user_id;
            $n_news->image = (string)$image;
            $n_news->first_name = (string)$first_name;
            $n_news->second_name = (string)$second_name;
            if ($show_slider == 't') {
                $show_slider = 1;
            } else {
                $show_slider = 0;
            }
            $n_news->show_slider = $show_slider;
            $n_news->short_text = (string)$short_text;
            $n_news->full_text = (string)$full_text;
            $n_news->meta_keywords = (string)$meta_keywords;
            $n_news->meta_description = (string)$meta_description;
            $n_news->read_count = $read_count;
            if (empty($publish_start_date)) {
                $publish_start_date = date('Y-m-d H:i:s');
            } else {
                $publish_start_date = $publish_start_date->format('Y-m-d H:i:s');
            }
            if (empty($date)) {
                $date = date('Y-m-d H:i:s');
            } else {
                $date = $date->format('Y-m-d H:i:s');
            }
            $n_news->publish_start_date = $publish_start_date;
            $n_news->publish_end_date = '';
            $n_news->date = $date;
            $n_news->slug = (string)$slug;

            $save = $n_news->save();
            if (($save)) {
                echo '</br>ela';
            } else {
//                    die(print_r($n_news->getErrors()) . 'author------'.$author_id . ' update------'.$updated_by_user_id . ' id------'.$id);
//                    die($date);
                echo '</br>' . (print_r($n_news->getErrors()) . 'author------' . $author_id . ' update------' . $updated_by_user_id . ' id------' . $id);
            }
        }
//        }
        echo $ids;
        die();

    }


    public function actionNewscategoryxml()
    {
        $this->layout = 'main_base';
//        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
        $url = 'http://goldnews.dev/xmls/xml/8-25/cat_news.xml';
        $xml = simplexml_load_file($url) or die("feed not loading");
        $categories = $xml->records;
//        die(var_dump($categories->row[1]));

//        echo '<br>BREAK HTML';
//        for ($vt = 0; $vt < 2; $vt++) {
        for ($va = 0; $va < 1030; $va++) {
            $nc_news = new CategoryNews();
//                $id = 0;
            $news_id = 0;
            $category_id = 0;
//                $parent_id = 0;
            for ($vf = 0; $vf < 3; $vf++) {
                if (isset($categories->row[$va]->column[$vf]['name'])) {
//                    echo $categories->row[$va]->column[$vf]['name'].'********'.$currencies[$vt]->Valute[$va]->Nominal.'********'.$currencies[$vt]->Valute[$va]->Name.'********'.$currencies[$vt]->Valute[$va]->Value;
//                    echo " <br>";
                    switch ($categories->row[$va]->column[$vf]['name']) {
                        case 'news_id':
                            $news_id = $categories->row[$va]->column[$vf];
                            break;
                        case 'category_id':
                            $category_id = $categories->row[$va]->column[$vf];
                            break;
                    }

                }
            }
//            $s =  'id - '. $id . ' *** '.'name- '. $name . ' *** '.'order_index - '. intval($order_index) . ' *** '.'id'. $id . ' *** '.'id'. $id . ' *** ';
//            die($date->format('Y-m-d H:i:s'));
//            die($name);
//                $date =
//                $news = new News
//                if()
            $news_id = (string)$news_id;
            $news_o = News::find()->where(['u_id' => $news_id])->one();
//                $category = Category::find()->where(['slug' => $c_slug])->one();
            $category_o = Category::find()->where(['id' => intval($category_id)])->one();
//                die($category_id);
            if (!empty($category_o) and !empty($category_o->id) and !empty($news_o)) {
//                    die('category_id------' .$category_o->id.'</br> news_id------' .$news_o->id);
                $news_o->category_id = intval($category_o->id);
//                    $nc_news->news_id = intval($news_o->id);
                $n_save = $news_o->save();
                if (($n_save)) {
                    echo '</br>ela';
                } else {
//                    die(print_r($n_news->getErrors()) . 'author------'.$author_id . ' update------'.$updated_by_user_id . ' id------'.$id);
                    echo '</br>yox';
//                    echo '</br>'.(print_r($n_news->getErrors()) . 'author------'.$author_id . ' update------'.$updated_by_user_id . ' id------'.$id);
                }
            } else {
                echo '</br>yox bratan';
            }
//                $nc_news->author = intval($author_id);

        }
//        }
//        echo $currencies[0]['Type'];

    }


}
