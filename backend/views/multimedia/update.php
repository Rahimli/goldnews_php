<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Multimedia */

$this->title = 'Update Multimedia: ' . $model->multimedia->id;
$this->params['breadcrumbs'][] = ['label' => 'Multimedia', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->multimedia->id, 'url' => ['view', 'id' => $model->multimedia->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="multimedia-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
