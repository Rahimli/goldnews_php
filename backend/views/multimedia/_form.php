<?php

use backend\models\MultimediaImage;
use backend\models\MultimediaVideo;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Multimedia */
/* @var $form yii\widgets\ActiveForm */
?>
<link rel="stylesheet" href="/static/admin/croper/news/cropper.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>

<link rel="stylesheet" href="/static/admin/croper/news/cropper-admin-main.css"/>
<div class="multimedia-form">

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false, // TODO get this working with client validation
    ]); ?>
    <?= $model->errorSummary($form); ?>
    <fieldset>
        <legend>Multimedia</legend>
        <?= $form->field($model->multimedia, 'is_active')->checkbox() ?>

        <?= $form->field($model->multimedia, 'show_slider')->checkbox() ?>

        <?= $form->field($model->multimedia, 'type')->dropDownList(['' => 'Tipi', 'gallery_news' => 'Foto Xəbər', 'video_news' => 'Video Xəbər']); ?>

        <div class="portlet-body">
            <!-- BEGIN FORM-->
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="img-container">
                            <img id="main-image" src="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <img id="cropped-main-image" src="" class="img-responsive">
                    </div>
                    <div class="col-md-6">
                        <img id="result-cropped-main-image" src="<?php if (!$model->multimedia->isNewRecord) {
                            echo $model->multimedia->image;
                        } ?>" class="preview-sm">
                        <input type="hidden" id="hidden-result-cropped-main-image" name="result_cropped_main_image"
                               value=""/>
                        <input type="hidden" id="hidden-original-main-image" value=""/>
                    </div>
                </div>
                <div class="row" id="actions">
                    <div class="col-md-9 docs-buttons">
                        <!-- <h3>Toolbar:</h3> -->
                        <div class="btn-group">
                            <div class="btn btn-info" data-method="zoom" data-option="0.1"
                                 title="Zoom In">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.zoom(0.1)">
                                            <span class="fa fa-search-plus"></span>
                                        </span>
                            </div>
                            <div class="btn btn-info" data-method="zoom" data-option="-0.1"
                                 title="Zoom Out">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.zoom(-0.1)">
                                            <span class="fa fa-search-minus"></span>
                                        </span>
                            </div>
                        </div>
                        <div class="btn-group">
                            <div class="btn btn-primary" data-method="move" data-option="-10"
                                 data-second-option="0"
                                 title="Move Left">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(-10, 0)">
                                            <span class="fa fa-arrow-left"></span>
                                        </span>
                            </div>
                            <div class="btn btn-primary" data-method="move" data-option="10"
                                 data-second-option="0"
                                 title="Move Right">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(10, 0)">
                                            <span class="fa fa-arrow-right"></span>
                                        </span>
                            </div>
                            <div class="btn btn-primary" data-method="move" data-option="0"
                                 data-second-option="-10"
                                 title="Move Up">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(0, -10)">
                                            <span class="fa fa-arrow-up"></span>
                                        </span>
                            </div>
                            <div class="btn btn-primary" data-method="move" data-option="0"
                                 data-second-option="10"
                                 title="Move Down">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(0, 10)">
                                            <span class="fa fa-arrow-down"></span>
                                        </span>
                            </div>
                        </div>

                        <div class="btn-group">
                            <label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">
                                <input type="file" class="sr-only" id="inputImage" name="file"
                                       accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">

                                <span class="docs-tooltip" data-toggle="tooltip" title="Şəkil Seç">
                                            <span class="fa fa-upload"></span>
                                        </span>
                            </label>
                        </div>
                        <div class="btn-group btn-group-crop">
                            <div class="btn btn-primary" data-method="getCroppedCanvas"
                                 data-option="{ &quot;width&quot;: 757, &quot;height&quot;: 343 }">
                                        <span class="docs-tooltip" data-toggle="tooltip"
                                              title="Kəs və göstər">
                                            Kəs və göstər
                                        </span>
                            </div>
                        </div>
                        <!-- Show the cropped image in modal -->
                        <div class="modal fade docs-cropped" id="getCroppedCanvasModal" role="dialog" aria-hidden="true"
                             aria-labelledby="getCroppedCanvasTitle" tabindex="-1">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <div class="close" data-dismiss="modal" aria-hidden="true">&times;</div>
                                        <h4 class="modal-title" id="getCroppedCanvasTitle">Cropped</h4>
                                    </div>
                                    <div class="modal-body"></div>
                                    <div class="modal-footer">
                                        <div class="btn btn-default" data-dismiss="modal">Close</div>
                                        <div class="btn btn-primary" id="download"
                                             href="javascript:void(0);"
                                             download="cropped.jpg">Download</div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal -->
                    </div><!-- /.docs-buttons -->
                </div>
            </div>
        </div>


        <?= $form->field($model->multimedia, 'prefix_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model->multimedia, 'suffix_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model->multimedia, 'slug')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model->multimedia, 'short_text')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model->multimedia, 'meta_keywords')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model->multimedia, 'meta_description')->textarea(['rows' => 6]) ?>

        <label >Başlanğıc Tarixi</label>
        <?php
        echo DatePicker::widget([
            'model' => $model->multimedia,
            'attribute' => 'publish_start_date',
            'options' => ['placeholder' => 'Yayımlanma Tarixi ...'],
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ]
        ]);
        ?>
        <br/>
        <label >Bitiş Tarixi</label>
        <?php
        echo DatePicker::widget([
            'model' => $model->multimedia,
            'attribute' => 'publish_end_date',
            'options' => ['placeholder' => 'Bitiş Tarixi...'],
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ]
        ]);
        ?>
        <br/>

        <?= $form->field($model->multimedia, 'read_count')->textInput() ?>
    </fieldset>
    <hr style="border-top: 1px solid #222d32;"/>
    <fieldset  id="m-image-area">
        <legend>MultimediaImages
            <?php
            // new multimedia_image button
            echo Html::a('New MultimediaImage', 'javascript:void(0);', [
                'id' => 'multimedia-new-multimedia_image-button',
                'class' => 'pull-right btn btn-success btn-xs'
            ])
            ?>
        </legend>
        <?php

        // multimedia_image table
        $multimedia_image = new MultimediaImage();
        $multimedia_image->loadDefaultValues();

        echo '<table id="multimedia-multimediaImages" class="table table-condensed table-bordered">';
        echo '<thead>';
        echo '<tr>';
//        echo '<th>' . '</th>';
//        echo '<th>' . '</th>';
        echo '<td>&nbsp;</td>';
        echo '</tr>';
        echo '</thead>';
        echo '</tbody>';

        // existing multimediaImages fields
        foreach ($model->multimediaImages as $key => $_multimedia_image) {
            echo '<tr>';
            echo $this->render('_form-multimedia-multimedia_image', [
                'key' => $_multimedia_image->isNewRecord ? (strpos($key, 'new') !== false ? $key : 'new' . $key) : $_multimedia_image->id,
                'form' => $form,
                'multimedia_image' => $_multimedia_image,
            ]);
            echo '</tr>';
        }
        // new multimedia_image fields
        echo '<tr id="multimedia-new-multimedia_image-block" style="display: none;">';
        echo $this->render('_form-multimedia-multimedia_image', [
            'key' => '__id__',
            'form' => $form,
            'multimedia_image' => $multimedia_image,
        ]);
        echo '</tr>';
        echo '</tbody>';
        echo '</table>';
        ?>
        <?php ob_start(); // output buffer the javascript to register later ?>
        <script>
            // add multimedia_image button
            var multimedia_image_k = <?php echo isset($key) ? str_replace('new', '', $key) : 0; ?>;
            $('#multimedia-new-multimedia_image-button').on('click', function () {
                multimedia_image_k += 1;
                $('#multimedia-multimediaImages').find('tbody')
                    .append('<tr>' + $('#multimedia-new-multimedia_image-block').html().replace(/__id__/g, 'new' + multimedia_image_k) + '</tr>');

            });

            // remove multimedia_image button
            $(document).on('click', '.multimedia-remove-multimedia_image-button', function () {
                $(this).closest('tbody tr').remove();
            });
            <?php
            // OPTIONAL: click add when the form first loads to display the first new row
            if (!Yii::$app->request->isPost && $model->multimedia->isNewRecord)
                echo "$('#multimedia-new-multimedia_image-button').click();";
            ?>
        </script>
        <?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>    </fieldset>
    <hr style="border-top: 1px solid #222d32;"/>
    <fieldset id="m-video-area">
        <legend>Multimedia Videos
            <?php
            // new multimedia_video button
            echo Html::a('New MultimediaVideo', 'javascript:void(0);', [
                'id' => 'multimedia-new-multimedia_video-button',
                'class' => 'pull-right btn btn-success btn-xs'
            ])
            ?>
        </legend>
        <?php

        // multimedia_video table
        $multimedia_video = new MultimediaVideo();
        $multimedia_video->loadDefaultValues();

        echo '<table id="multimedia-multimediaVideos" class="table table-condensed table-bordered">';
        echo '<thead>';
        echo '<tr>';
        echo '<th>'  . '</th>';
        echo '<th>' . '</th>';
        echo '<td>&nbsp;</td>';
        echo '</tr>';
        echo '</thead>';
        echo '</tbody>';

        // existing multimediaVideos fields
        foreach ($model->multimediaVideos as $key => $_multimedia_video) {
            echo '<tr>';
            echo $this->render('_form-multimedia-multimedia_video', [
                'key' => $_multimedia_video->isNewRecord ? (strpos($key, 'new') !== false ? $key : 'new' . $key) : $_multimedia_video->id,
                'form' => $form,
                'multimedia_video' => $_multimedia_video,
            ]);
            echo '</tr>';
        }
        // new multimedia_video fields
        echo '<tr id="multimedia-new-multimedia_video-block" style="display: none;">';
        echo $this->render('_form-multimedia-multimedia_video', [
            'key' => '__id__',
            'form' => $form,
            'multimedia_video' => $multimedia_video,
        ]);
        echo '</tr>';
        echo '</tbody>';
        echo '</table>';
        ?>
        <?php ob_start(); // output buffer the javascript to register later ?>
        <script>
            // add multimedia_video button
            var multimedia_video_k = <?php echo isset($key) ? str_replace('new', '', $key) : 0; ?>;
            $('#multimedia-new-multimedia_video-button').on('click', function () {
                multimedia_video_k += 1;
                $('#multimedia-multimediaVideos').find('tbody')
                    .append('<tr>' + $('#multimedia-new-multimedia_video-block').html().replace(/__id__/g, 'new' + multimedia_video_k) + '</tr>');

            });

            // remove multimedia_video button
            $(document).on('click', '.multimedia-remove-multimedia_video-button', function () {
                $(this).closest('tbody tr').remove();
            });
            <?php
            // OPTIONAL: click add when the form first loads to display the first new row
            if (!Yii::$app->request->isPost && $model->multimedia->isNewRecord)
                echo "$('#multimedia-new-multimedia_video-button').click();";
            ?>
        </script>
        <?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>    </fieldset>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => $model->multimedia->isNewRecord ? 'btn btn-success' : 'btn btn-primary']); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="/static/admin/croper/news/cropper-custom-main.js"></script>
<script src="/static/admin/croper/news/cropper.js"></script>

<script>
    var multimedia_type = $('#multimedia-type');
    var m_video_area = $('#m-video-area');
    var m_image_area = $('#m-image-area');

    $(document).ready(function () {
        if (multimedia_type.val() == 'gallery_news') {
            m_video_area.hide();
            m_image_area.show();
        } else if (multimedia_type.val() == 'video_news') {
            m_video_area.show();
            m_image_area.hide();
//            lang_div_en.click(function() {
//                alert( "Handler for .click() called." );
//            });
        } else {
            m_video_area.hide();
            m_image_area.hide();
        }
    });

    multimedia_type.change(function () {
//        alert( "Handler for .change() called." );
        if (multimedia_type.val() == 'gallery_news') {
            m_video_area.hide();
            m_image_area.show();
        } else if (multimedia_type.val() == 'video_news') {
            m_video_area.show();
            m_image_area.hide();
//            lang_div_en.click(function() {
//                alert( "Handler for .click() called." );
//            });
        } else {
            m_video_area.hide();
            m_image_area.hide();
        }
    });
</script>

<script>
    //    * @param string s
    //    * @param object opt
    //    * @return string

    function url_slug(s, opt) {
        s = String(s);
        opt = Object(opt);

        var defaults = {
            'delimiter': '-',
            'limit': 100,
            'lowercase': true,
            'replacements': {},
            'transliterate': (typeof(XRegExp) === 'undefined') ? true : false
        };

        // Merge options
        for (var k in defaults) {
            if (!opt.hasOwnProperty(k)) {
                opt[k] = defaults[k];
            }
        }

        var char_map = {
            // Latin
            'À': 'A', 'Á': 'A', 'Â': 'A', 'Ã': 'A', 'Ä': 'A', 'Å': 'A', 'Æ': 'AE', 'Ç': 'C',
            'È': 'E', 'É': 'E', 'Ê': 'E', 'Ë': 'E', 'Ì': 'I', 'Í': 'I', 'Î': 'I', 'Ï': 'I',
            'Ð': 'D', 'Ñ': 'N', 'Ò': 'O', 'Ó': 'O', 'Ô': 'O', 'Õ': 'O', 'Ö': 'O', 'Ő': 'O',
            'Ø': 'O', 'Ù': 'U', 'Ú': 'U', 'Û': 'U', 'Ü': 'U', 'Ű': 'U', 'Ý': 'Y', 'Þ': 'TH',
            'ß': 'ss',
            'à': 'a', 'á': 'a', 'â': 'a', 'ã': 'a', 'ä': 'a', 'å': 'a', 'æ': 'ae', 'ç': 'c',
            'è': 'e', 'é': 'e', 'ê': 'e', 'ë': 'e', 'ì': 'i', 'í': 'i', 'î': 'i', 'ï': 'i',
            'ð': 'd', 'ñ': 'n', 'ò': 'o', 'ó': 'o', 'ô': 'o', 'õ': 'o', 'ö': 'o', 'ő': 'o',
            'ø': 'o', 'ù': 'u', 'ú': 'u', 'û': 'u', 'ü': 'u', 'ű': 'u', 'ý': 'y', 'þ': 'th',
            'ÿ': 'y',

            // Latin symbols
            '©': '(c)',

            // Greek
            'Α': 'A', 'Β': 'B', 'Γ': 'G', 'Δ': 'D', 'Ε': 'E', 'Ζ': 'Z', 'Η': 'H', 'Θ': '8',
            'Ι': 'I', 'Κ': 'K', 'Λ': 'L', 'Μ': 'M', 'Ν': 'N', 'Ξ': '3', 'Ο': 'O', 'Π': 'P',
            'Ρ': 'R', 'Σ': 'S', 'Τ': 'T', 'Υ': 'Y', 'Φ': 'F', 'Χ': 'X', 'Ψ': 'PS', 'Ω': 'W',
            'Ά': 'A', 'Έ': 'E', 'Ί': 'I', 'Ό': 'O', 'Ύ': 'Y', 'Ή': 'H', 'Ώ': 'W', 'Ϊ': 'I',
            'Ϋ': 'Y',
            'α': 'a', 'β': 'b', 'γ': 'g', 'δ': 'd', 'ε': 'e', 'ζ': 'z', 'η': 'h', 'θ': '8',
            'ι': 'i', 'κ': 'k', 'λ': 'l', 'μ': 'm', 'ν': 'n', 'ξ': '3', 'ο': 'o', 'π': 'p',
            'ρ': 'r', 'σ': 's', 'τ': 't', 'υ': 'y', 'φ': 'f', 'χ': 'x', 'ψ': 'ps', 'ω': 'w',
            'ά': 'a', 'έ': 'e', 'ί': 'i', 'ό': 'o', 'ύ': 'y', 'ή': 'h', 'ώ': 'w', 'ς': 's',
            'ϊ': 'i', 'ΰ': 'y', 'ϋ': 'y', 'ΐ': 'i',

            // Turkish
            'Ş': 'S', 'İ': 'I', 'Ç': 'C', 'Ü': 'U', 'Ö': 'O', 'Ğ': 'G',
            'ş': 's', 'ı': 'i', 'ç': 'c', 'ü': 'u', 'ö': 'o', 'ğ': 'g',

            // Russian
            'А': 'A', 'Б': 'B', 'В': 'V', 'Г': 'G', 'Д': 'D', 'Е': 'E', 'Ё': 'Yo', 'Ж': 'Zh',
            'З': 'Z', 'И': 'I', 'Й': 'J', 'К': 'K', 'Л': 'L', 'М': 'M', 'Н': 'N', 'О': 'O',
            'П': 'P', 'Р': 'R', 'С': 'S', 'Т': 'T', 'У': 'U', 'Ф': 'F', 'Х': 'H', 'Ц': 'C',
            'Ч': 'Ch', 'Ш': 'Sh', 'Щ': 'Sh', 'Ъ': '', 'Ы': 'Y', 'Ь': '', 'Э': 'E', 'Ю': 'Yu',
            'Я': 'Ya',
            'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'yo', 'ж': 'zh',
            'з': 'z', 'и': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o',
            'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'c',
            'ч': 'ch', 'ш': 'sh', 'щ': 'sh', 'ъ': '', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu',
            'я': 'ya',

            // Ukrainian
            'Є': 'Ye', 'І': 'I', 'Ї': 'Yi', 'Ґ': 'G',
            'є': 'ye', 'і': 'i', 'ї': 'yi', 'ґ': 'g',

            // Azerbaijanian
            'Ə': 'E',
            'ə': 'e',

            // Czech
            'Č': 'C', 'Ď': 'D', 'Ě': 'E', 'Ň': 'N', 'Ř': 'R', 'Š': 'S', 'Ť': 'T', 'Ů': 'U',
            'Ž': 'Z',
            'č': 'c', 'ď': 'd', 'ě': 'e', 'ň': 'n', 'ř': 'r', 'š': 's', 'ť': 't', 'ů': 'u',
            'ž': 'z',

            // Polish
            'Ą': 'A', 'Ć': 'C', 'Ę': 'e', 'Ł': 'L', 'Ń': 'N', 'Ó': 'o', 'Ś': 'S', 'Ź': 'Z',
            'Ż': 'Z',
            'ą': 'a', 'ć': 'c', 'ę': 'e', 'ł': 'l', 'ń': 'n', 'ó': 'o', 'ś': 's', 'ź': 'z',
            'ż': 'z',

            // Latvian
            'Ā': 'A', 'Č': 'C', 'Ē': 'E', 'Ģ': 'G', 'Ī': 'i', 'Ķ': 'k', 'Ļ': 'L', 'Ņ': 'N',
            'Š': 'S', 'Ū': 'u', 'Ž': 'Z',
            'ā': 'a', 'č': 'c', 'ē': 'e', 'ģ': 'g', 'ī': 'i', 'ķ': 'k', 'ļ': 'l', 'ņ': 'n',
            'š': 's', 'ū': 'u', 'ž': 'z'
        };

        // Make custom replacements
        for (var k in opt.replacements) {
            s = s.replace(RegExp(k, 'g'), opt.replacements[k]);
        }

        // Transliterate characters to ASCII
        if (opt.transliterate) {
            for (var k in char_map) {
                s = s.replace(RegExp(k, 'g'), char_map[k]);
            }
        }

        // Replace non-alphanumeric characters with our delimiter
        var alnum = (typeof(XRegExp) === 'undefined') ? RegExp('[^a-z0-9]+', 'ig') : XRegExp('[^\\p{L}\\p{N}]+', 'ig');
        s = s.replace(alnum, opt.delimiter);

        // Remove duplicate delimiters
        s = s.replace(RegExp('[' + opt.delimiter + ']{2,}', 'g'), opt.delimiter);

        // Truncate slug to max. characters
        s = s.substring(0, opt.limit);

        // Remove delimiter from ends
        s = s.replace(RegExp('(^' + opt.delimiter + '|' + opt.delimiter + '$)', 'g'), '');

        return opt.lowercase ? s.toLowerCase() : s;
    }

</script>
<script>
    var multimedia_prefix_name = $("#multimedia-prefix_name");
    var multimedia_suffix_name = $("#multimedia-suffix_name");
    var multimedia_slug_d = $("#multimedia-slug");
    var slug_symbol = '';
    multimedia_prefix_name.keyup(function () {
        str_prefix_name = multimedia_prefix_name.val();
        str_suffix_name = multimedia_suffix_name.val();
        if (str_prefix_name.trim()!=""){
            if (str_suffix_name.trim()!=""){
                slug_symbol = '-'
            }else{
                slug_symbol = ''
            }
        }else{
            slug_symbol = ''
        }
        multimedia_slug_d.val(url_slug(str_prefix_name) + slug_symbol + url_slug(str_suffix_name) );
//        }
    });
    multimedia_suffix_name.keyup(function () {
        str_prefix_name = multimedia_prefix_name.val();
        str_suffix_name = multimedia_suffix_name.val();
        if (str_prefix_name.trim()!=""){
            if (str_suffix_name.trim()!=""){
                slug_symbol = '-'
            }else{
                slug_symbol = ''
            }
        }else{
            slug_symbol = ''
        }
        multimedia_slug_d.val(url_slug(str_prefix_name) + slug_symbol + url_slug(str_suffix_name) );
//        }
    });

</script>