<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MultimediaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Multimedia';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="multimedia-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Multimedia', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'is_active',
            'author',
            'updated_by_user',
            'image',
            // 'type',
            // 'prefix_name',
            // 'suffix_name',
            // 'slug',
            // 'show_slider',
            // 'short_text',
            // 'meta_keywords',
            // 'meta_description:ntext',
            // 'publish_start_date',
            // 'publish_end_date',
            // 'read_count',
            // 'date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

<script>
    $("tbody tr td").each(function() {
        var htmlRegex = new RegExp("<([A-Za-z][A-Za-z0-9]*)\b[^>]*>(.*?)</\1>");
        if($(this).html().indexOf("<a") < 0){
            // Within tr we find the last td child element and get content
            $(this).text($(this).text().slice(0, 25));
        }
    });
</script>