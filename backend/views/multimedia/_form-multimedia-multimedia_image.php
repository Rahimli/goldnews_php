<?php
/**
 * Created by PhpStorm.
 * User: rahim
 * Date: 7/25/2017
 * Time: 12:59
 */
use backend\models\MultimediaImage;
use mihaildev\elfinder\InputFile;
use yii\helpers\Html;
?>
<td>
<?php echo $form->field($multimedia_image, 'image')->widget(InputFile::className(), [
    'language' => 'en',
    'controller' => 'fm',
    'path' => '/multimedia',
    'filter'        => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
    'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
    'options'       => ['class' => 'form-control',
        'name' => "MultimediaImages[$key][image]",'id' => "MultimediaImages_{$key}_image",],
    'buttonOptions' => ['class' => 'btn btn-default'],
    'multiple'      => false       // возможность выбора нескольких файлов
]);?>

</td>
<td>
    <?= $form->field($multimedia_image, 'context')->textInput([
        'id' => "MultimediaImages_{$key}_context",
        'name' => "MultimediaImages[$key][context]",
    ])->label() ?>
</td>
<td>
    <?= Html::a('<i class="fa fa-times" aria-hidden="true"></i> Şəkili Sil ', 'javascript:void(0);', [
        'class' => 'multimedia-remove-multimedia_image-button btn btn-danger btn-xs',
    ]) ?>
</td>
