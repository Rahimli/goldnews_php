<?php
/**
 * Created by PhpStorm.
 * User: rahim
 * Date: 7/25/2017
 * Time: 12:59
 */
use backend\models\MultimediaVideo;
use mihaildev\elfinder\InputFile;
use yii\helpers\Html;
?>
<td>
    <?php echo $form->field($multimedia_video, 'video_file')->widget(InputFile::className(), [
        'language' => 'en',
        'controller' => 'fm',
        'path' => '/multimedia',
        'filter'        => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
        'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
        'options'       => ['class' => 'form-control',
            'name' => "MultimediaVideos[$key][video_file]",'id' => "MultimediaVideos_{$key}_video_file",],
        'buttonOptions' => ['class' => 'btn btn-default'],
        'multiple'      => false       // возможность выбора нескольких файлов
    ]);?>

</td>
<td>
    <?= $form->field($multimedia_video, 'video_url')->textInput([
        'id' => "MultimediaVideos_{$key}_video_url",
        'name' => "MultimediaVideos[$key][video_url]",
    ]) ?>
</td>
<td>
    <?= Html::a('<i class="fa fa-times" aria-hidden="true"></i> Videonu Sil ', 'javascript:void(0);', [
        'class' => 'multimedia-remove-multimedia_video-button btn btn-danger btn-xs',
    ]) ?>
</td>
