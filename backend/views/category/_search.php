<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'parent') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'order_index') ?>

    <?= $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'unique_name') ?>

    <?php // echo $form->field($model, 'logo') ?>

    <?php // echo $form->field($model, 'author') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'publish_start_date') ?>

    <?php // echo $form->field($model, 'publish_end_date') ?>

    <?php // echo $form->field($model, 'news_background') ?>

    <?php // echo $form->field($model, 'suffix_color') ?>

    <?php // echo $form->field($model, 'prefix_color') ?>

    <?php // echo $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'news_count') ?>

    <?php // echo $form->field($model, 'slug') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
