<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\NewsView */

$this->title = 'Create News View';
$this->params['breadcrumbs'][] = ['label' => 'News Views', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
