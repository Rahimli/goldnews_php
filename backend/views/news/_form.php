<?php

use backend\models\Tag;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use mihaildev\elfinder\ElFinder;
/* @var $this yii\web\View */
/* @var $model backend\models\News */
/* @var $form yii\widgets\ActiveForm */
?>
<link rel="stylesheet" href="/static/admin/croper/news/cropper.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>

<link rel="stylesheet" href="/static/admin/croper/news/cropper-admin-main.css"/>
<div class="news-form">
    <div id="message-div" style="display: none" class="alert alert-danger">

        <?php foreach ($model->getErrors() as $error => $value){ ?>
            <?php die($model->getErrors())?>
            <strong id="message-div-strong"><?= $error ?></strong>
        <?php }?>
    </div>
    <?php $form = ActiveForm::begin([
        'id' => 'news-form',
//        'options' => ['onsubmit' => 'alert("Salam")',],
    ]); ?>

    <?php $categoryArray = ArrayHelper::map(\backend\models\Category::find()->orderBy('name')->all(), 'id', 'name') ?>


    <?php echo $form->field($model, 'category_id')->widget(Select2::classname(), [
        'data' => $categoryArray,
        'options' => ['placeholder' => '-- Kateqoriya Seç --'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('Kateqoriya');
    ?>
    <?= $form->field($model, 'is_active')->checkbox() ?>

    <?= $form->field($model, 'show_slider')->checkbox() ?>

    <div class="portlet-body">
        <!-- BEGIN FORM-->
        <div class="form-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="img-container">
                        <img id="main-image" src="">
                    </div>
                </div>
                <div class="col-md-6">
                    <img id="cropped-main-image" src="" class="img-responsive">
                </div>
                <div class="col-md-6">
                    <img id="result-cropped-main-image" src="<?php if (!$model->isNewRecord) {
                        echo $model->image;
                    } ?>" class="preview-sm">
                    <input type="hidden" id="hidden-result-cropped-main-image" name="result_cropped_main_image"
                           value=""/>
                    <input type="hidden" id="hidden-original-main-image" value=""/>
                </div>
            </div>
            <div class="row" id="actions">
                <div class="col-md-9 docs-buttons">
                    <!-- <h3>Toolbar:</h3> -->
                    <div class="btn-group">
                        <div class="btn btn-info" data-method="zoom" data-option="0.1"
                           title="Zoom In">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.zoom(0.1)">
                                            <span class="fa fa-search-plus"></span>
                                        </span>
                        </div>
                        <div class="btn btn-info" data-method="zoom" data-option="-0.1"
                           title="Zoom Out">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.zoom(-0.1)">
                                            <span class="fa fa-search-minus"></span>
                                        </span>
                        </div>
                    </div>
                    <div class="btn-group">
                        <div class="btn btn-primary" data-method="move" data-option="-10"
                           data-second-option="0"
                           title="Move Left">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(-10, 0)">
                                            <span class="fa fa-arrow-left"></span>
                                        </span>
                        </div>
                        <div class="btn btn-primary" data-method="move" data-option="10"
                           data-second-option="0"
                           title="Move Right">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(10, 0)">
                                            <span class="fa fa-arrow-right"></span>
                                        </span>
                        </div>
                        <div class="btn btn-primary" data-method="move" data-option="0"
                           data-second-option="-10"
                           title="Move Up">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(0, -10)">
                                            <span class="fa fa-arrow-up"></span>
                                        </span>
                        </div>
                        <div class="btn btn-primary" data-method="move" data-option="0"
                           data-second-option="10"
                           title="Move Down">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(0, 10)">
                                            <span class="fa fa-arrow-down"></span>
                                        </span>
                        </div>
                    </div>

                    <div class="btn-group">
                        <label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">
                            <input type="file" class="sr-only" id="inputImage" name="file"
                                   accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">

                            <span class="docs-tooltip" data-toggle="tooltip" title="Şəkil Seç">
                                            <span class="fa fa-upload"></span>
                                        </span>
                        </label>
                    </div>
                    <div class="btn-group btn-group-crop">
                        <div class="btn btn-primary" data-method="getCroppedCanvas"
                           data-option="{ &quot;width&quot;: 757, &quot;height&quot;: 343 }">
                                        <span class="docs-tooltip" data-toggle="tooltip"
                                              title="Kəs və göstər">
                                            Kəs və göstər
                                        </span>
                        </div>
                    </div>
                    <!-- Show the cropped image in modal -->
                    <div class="modal fade docs-cropped" id="getCroppedCanvasModal" role="dialog" aria-hidden="true"
                         aria-labelledby="getCroppedCanvasTitle" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="close" data-dismiss="modal" aria-hidden="true">&times;</div>
                                    <h4 class="modal-title" id="getCroppedCanvasTitle">Cropped</h4>
                                </div>
                                <div class="modal-body"></div>
                                <div class="modal-footer">
                                    <div class="btn btn-default" data-dismiss="modal">Close</div>
                                    <div class="btn btn-primary" id="download"
                                       href="javascript:void(0);"
                                       download="cropped.jpg">Download</div>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal -->
                </div><!-- /.docs-buttons -->
            </div>
        </div>
    </div>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true])->label('Prefiks') ?>

    <?= $form->field($model, 'second_name')->textInput(['maxlength' => true])->label('Suffiks') ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_text')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'full_text')->widget(CKEditor::className(),
        ['editorOptions' => ElFinder::ckeditorOptions('fm',
            ['preset' => 'full',
                'language' => 'az',
//                'controller' => 'fm',
                'path' => '/ck-news',
                'filter' => 'image',
                'multiple' => true,
            ]),
        ]); ?>
    <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>

    <label >Başlanğıc Tarixi</label>
    <?php
    echo DatePicker::widget([
        'model' => $model,
        'attribute' => 'publish_start_date',
        'options' => ['placeholder' => 'Yayımlanma Tarixi ...'],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);
    ?>
    <br/>
    <label >Bitiş Tarixi</label>
    <?php
        echo DatePicker::widget([
            'model' => $model,
            'attribute' => 'publish_end_date',
            'options' => ['placeholder' => 'Bitiş Tarixi...'],
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ]
        ]);
    ?>
    <br/>
    <fieldset>
        <legend>Taqlar</legend>
        <?= $form->field($model, 'tag_ids')->widget(Select2::className(), [
            'model' => $model,
            'attribute' => 'tag_ids',
            'data' => ArrayHelper::map(Tag::find()->all(), 'name', 'name'),
            'options' => [
                'multiple' => true,
            ],
            'pluginOptions' => [
                'tags' => true,
            ],
        ]); ?>
    </fieldset>

    <?= $form->field($model, 'read_count')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="/static/admin/croper/news/cropper-custom-main.js"></script>
<script src="/static/admin/croper/news/cropper.js"></script>
<!--<script>-->

<script>
    //    * @param string s
    //    * @param object opt
    //    * @return string

    function url_slug(s, opt) {
        s = String(s);
        opt = Object(opt);

        var defaults = {
            'delimiter': '-',
            'limit': 100,
            'lowercase': true,
            'replacements': {},
            'transliterate': (typeof(XRegExp) === 'undefined') ? true : false
        };

        // Merge options
        for (var k in defaults) {
            if (!opt.hasOwnProperty(k)) {
                opt[k] = defaults[k];
            }
        }

        var char_map = {
            // Latin
            'À': 'A', 'Á': 'A', 'Â': 'A', 'Ã': 'A', 'Ä': 'A', 'Å': 'A', 'Æ': 'AE', 'Ç': 'C',
            'È': 'E', 'É': 'E', 'Ê': 'E', 'Ë': 'E', 'Ì': 'I', 'Í': 'I', 'Î': 'I', 'Ï': 'I',
            'Ð': 'D', 'Ñ': 'N', 'Ò': 'O', 'Ó': 'O', 'Ô': 'O', 'Õ': 'O', 'Ö': 'O', 'Ő': 'O',
            'Ø': 'O', 'Ù': 'U', 'Ú': 'U', 'Û': 'U', 'Ü': 'U', 'Ű': 'U', 'Ý': 'Y', 'Þ': 'TH',
            'ß': 'ss',
            'à': 'a', 'á': 'a', 'â': 'a', 'ã': 'a', 'ä': 'a', 'å': 'a', 'æ': 'ae', 'ç': 'c',
            'è': 'e', 'é': 'e', 'ê': 'e', 'ë': 'e', 'ì': 'i', 'í': 'i', 'î': 'i', 'ï': 'i',
            'ð': 'd', 'ñ': 'n', 'ò': 'o', 'ó': 'o', 'ô': 'o', 'õ': 'o', 'ö': 'o', 'ő': 'o',
            'ø': 'o', 'ù': 'u', 'ú': 'u', 'û': 'u', 'ü': 'u', 'ű': 'u', 'ý': 'y', 'þ': 'th',
            'ÿ': 'y',

            // Latin symbols
            '©': '(c)',

            // Greek
            'Α': 'A', 'Β': 'B', 'Γ': 'G', 'Δ': 'D', 'Ε': 'E', 'Ζ': 'Z', 'Η': 'H', 'Θ': '8',
            'Ι': 'I', 'Κ': 'K', 'Λ': 'L', 'Μ': 'M', 'Ν': 'N', 'Ξ': '3', 'Ο': 'O', 'Π': 'P',
            'Ρ': 'R', 'Σ': 'S', 'Τ': 'T', 'Υ': 'Y', 'Φ': 'F', 'Χ': 'X', 'Ψ': 'PS', 'Ω': 'W',
            'Ά': 'A', 'Έ': 'E', 'Ί': 'I', 'Ό': 'O', 'Ύ': 'Y', 'Ή': 'H', 'Ώ': 'W', 'Ϊ': 'I',
            'Ϋ': 'Y',
            'α': 'a', 'β': 'b', 'γ': 'g', 'δ': 'd', 'ε': 'e', 'ζ': 'z', 'η': 'h', 'θ': '8',
            'ι': 'i', 'κ': 'k', 'λ': 'l', 'μ': 'm', 'ν': 'n', 'ξ': '3', 'ο': 'o', 'π': 'p',
            'ρ': 'r', 'σ': 's', 'τ': 't', 'υ': 'y', 'φ': 'f', 'χ': 'x', 'ψ': 'ps', 'ω': 'w',
            'ά': 'a', 'έ': 'e', 'ί': 'i', 'ό': 'o', 'ύ': 'y', 'ή': 'h', 'ώ': 'w', 'ς': 's',
            'ϊ': 'i', 'ΰ': 'y', 'ϋ': 'y', 'ΐ': 'i',

            // Turkish
            'Ş': 'S', 'İ': 'I', 'Ç': 'C', 'Ü': 'U', 'Ö': 'O', 'Ğ': 'G',
            'ş': 's', 'ı': 'i', 'ç': 'c', 'ü': 'u', 'ö': 'o', 'ğ': 'g',

            // Russian
            'А': 'A', 'Б': 'B', 'В': 'V', 'Г': 'G', 'Д': 'D', 'Е': 'E', 'Ё': 'Yo', 'Ж': 'Zh',
            'З': 'Z', 'И': 'I', 'Й': 'J', 'К': 'K', 'Л': 'L', 'М': 'M', 'Н': 'N', 'О': 'O',
            'П': 'P', 'Р': 'R', 'С': 'S', 'Т': 'T', 'У': 'U', 'Ф': 'F', 'Х': 'H', 'Ц': 'C',
            'Ч': 'Ch', 'Ш': 'Sh', 'Щ': 'Sh', 'Ъ': '', 'Ы': 'Y', 'Ь': '', 'Э': 'E', 'Ю': 'Yu',
            'Я': 'Ya',
            'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'yo', 'ж': 'zh',
            'з': 'z', 'и': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o',
            'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'c',
            'ч': 'ch', 'ш': 'sh', 'щ': 'sh', 'ъ': '', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu',
            'я': 'ya',

            // Ukrainian
            'Є': 'Ye', 'І': 'I', 'Ї': 'Yi', 'Ґ': 'G',
            'є': 'ye', 'і': 'i', 'ї': 'yi', 'ґ': 'g',

            // Azerbaijanian
            'Ə': 'E',
            'ə': 'e',

            // Czech
            'Č': 'C', 'Ď': 'D', 'Ě': 'E', 'Ň': 'N', 'Ř': 'R', 'Š': 'S', 'Ť': 'T', 'Ů': 'U',
            'Ž': 'Z',
            'č': 'c', 'ď': 'd', 'ě': 'e', 'ň': 'n', 'ř': 'r', 'š': 's', 'ť': 't', 'ů': 'u',
            'ž': 'z',

            // Polish
            'Ą': 'A', 'Ć': 'C', 'Ę': 'e', 'Ł': 'L', 'Ń': 'N', 'Ó': 'o', 'Ś': 'S', 'Ź': 'Z',
            'Ż': 'Z',
            'ą': 'a', 'ć': 'c', 'ę': 'e', 'ł': 'l', 'ń': 'n', 'ó': 'o', 'ś': 's', 'ź': 'z',
            'ż': 'z',

            // Latvian
            'Ā': 'A', 'Č': 'C', 'Ē': 'E', 'Ģ': 'G', 'Ī': 'i', 'Ķ': 'k', 'Ļ': 'L', 'Ņ': 'N',
            'Š': 'S', 'Ū': 'u', 'Ž': 'Z',
            'ā': 'a', 'č': 'c', 'ē': 'e', 'ģ': 'g', 'ī': 'i', 'ķ': 'k', 'ļ': 'l', 'ņ': 'n',
            'š': 's', 'ū': 'u', 'ž': 'z'
        };

        // Make custom replacements
        for (var k in opt.replacements) {
            s = s.replace(RegExp(k, 'g'), opt.replacements[k]);
        }

        // Transliterate characters to ASCII
        if (opt.transliterate) {
            for (var k in char_map) {
                s = s.replace(RegExp(k, 'g'), char_map[k]);
            }
        }

        // Replace non-alphanumeric characters with our delimiter
        var alnum = (typeof(XRegExp) === 'undefined') ? RegExp('[^a-z0-9]+', 'ig') : XRegExp('[^\\p{L}\\p{N}]+', 'ig');
        s = s.replace(alnum, opt.delimiter);

        // Remove duplicate delimiters
        s = s.replace(RegExp('[' + opt.delimiter + ']{2,}', 'g'), opt.delimiter);

        // Truncate slug to max. characters
        s = s.substring(0, opt.limit);

        // Remove delimiter from ends
        s = s.replace(RegExp('(^' + opt.delimiter + '|' + opt.delimiter + '$)', 'g'), '');

        return opt.lowercase ? s.toLowerCase() : s;
    }

</script>
<script>
    var news_first_name = $("#news-first_name");
    var news_second_name = $("#news-second_name");
    var news_slug_d = $("#news-slug");
    var slug_symbol = '';
    news_first_name.keyup(function () {
        str_first_name = news_first_name.val();
        str_second_name = news_second_name.val();
        if (str_first_name.trim()!=""){
            if (str_second_name.trim()!=""){
                slug_symbol = '-'
            }else{
                slug_symbol = ''
            }
        }else{
            slug_symbol = ''
        }
        news_slug_d.val(url_slug(str_first_name) + slug_symbol + url_slug(str_second_name) );
//        }
    });
    news_second_name.keyup(function () {
        str_first_name = news_first_name.val();
        str_second_name = news_second_name.val();
        if (str_first_name.trim()!=""){
            if (str_second_name.trim()!=""){
                slug_symbol = '-'
            }else{
                slug_symbol = ''
            }
        }else{
            slug_symbol = ''
        }
        news_slug_d.val(url_slug(str_first_name) + slug_symbol + url_slug(str_second_name) );
//        }
    });

</script>