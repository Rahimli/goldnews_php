<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'News';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create News', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
//            'category_id',
//            ['attribute' => 'category_id',
//                'value' => 'category.name',
//            ],
            'is_active',
//            'author',
//            'updated_by_user',
            // 'image',
             'first_name',
             'second_name',
            [
                'attribute'=>'preview',
                'format'=>'raw',
                'value' => function($data)
                {
                    return
                        Html::a('Preview', '/news/'.$data->u_id.'/'.$data->slug.'/prenews', ['class' => 'btn btn-primary','target'=>'_blank']);

                }
            ],
            // 'slug',
            // 'show_slider',
            // 'short_text',
            // 'full_text:ntext',
            // 'meta_keywords',
            // 'meta_description:ntext',
            // 'publish_start_date',
            // 'publish_end_date',
            // 'read_count',
            // 'date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

<script>
    $("tbody tr td").each(function() {
        var htmlRegex = new RegExp("<([A-Za-z][A-Za-z0-9]*)\b[^>]*>(.*?)</\1>");
        if($(this).html().indexOf("<a") < 0){
            // Within tr we find the last td child element and get content
            $(this).text($(this).text().slice(0, 25));
        }
    });
</script>