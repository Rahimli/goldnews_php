<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\News */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'u_id',
//            'category_id',
            'is_active',
            'author',
            'updated_by_user',
            'image',
            'first_name',
            'second_name',
            'slug',
            'show_slider',
            'short_text',
            'full_text:ntext',
            'meta_keywords',
            'meta_description:ntext',
            'publish_start_date',
            'publish_end_date',
            'read_count',
            'date',
        ],
    ]) ?>

</div>
