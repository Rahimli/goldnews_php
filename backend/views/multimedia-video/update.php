<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\MultimediaVideo */

$this->title = 'Update Multimedia Video: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Multimedia Videos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="multimedia-video-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
