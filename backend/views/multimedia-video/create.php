<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\MultimediaVideo */

$this->title = 'Create Multimedia Video';
$this->params['breadcrumbs'][] = ['label' => 'Multimedia Videos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="multimedia-video-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
