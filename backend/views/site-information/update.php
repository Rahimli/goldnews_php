<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\SiteInformation */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Site Information',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Site Informations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="site-information-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
