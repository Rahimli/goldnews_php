<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\MultimediaImage */

$this->title = 'Update Multimedia Image: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Multimedia Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="multimedia-image-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
