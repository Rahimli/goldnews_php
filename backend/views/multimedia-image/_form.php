<?php

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MultimediaImage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="multimedia-image-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $multimediaArray = ArrayHelper::map(\backend\models\Multimedia::find()->orderBy('prefix_name')->all(), 'id', 'prefix_name') ?>

    <?php echo $form->field($model, 'multimedia_id')->widget(Select2::classname(), [
        'data' => $multimediaArray,
        'options' => ['placeholder' => '-- Multimedya Seç --'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('Multimedya');
    ?>

    <?= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'context')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
