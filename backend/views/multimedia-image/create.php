<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\MultimediaImage */

$this->title = 'Create Multimedia Image';
$this->params['breadcrumbs'][] = ['label' => 'Multimedia Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="multimedia-image-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
