<?php

namespace backend\controllers;

use app\models\form\MultimediaForm;
use Yii;
use backend\models\Multimedia;
use backend\models\MultimediaSearch;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MultimediaController implements the CRUD actions for Multimedia model.
 */
class MultimediaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Multimedia models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            $searchModel = new MultimediaSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }else{
        if(!Yii::$app->user->isGuest){
            Yii::$app->user->logout();
        }
        return $this->goHome();
    }
    }

    /**
     * Displays a single Multimedia model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }else{
        if(!Yii::$app->user->isGuest){
            Yii::$app->user->logout();
        }
        return $this->goHome();
    }
    }

    /**
     * Creates a new Multimedia model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            $model = new MultimediaForm();
            $model->multimedia = new Multimedia();
            $model->multimedia->loadDefaultValues();
            $model->multimedia->author = Yii::$app->user->identity->id;
            $model->multimedia->publish_start_date = date('Y-m-d');
            $model->setAttributes(Yii::$app->request->post());

            if (Yii::$app->request->post()) {

                if (Yii::$app->user->identity->position == 'redaktor' or Yii::$app->user->identity->is_superuser == 1) {
                } else {
                    $model->is_active = 0;
                }
                if ($model->save()) {
                    Yii::$app->getSession()->setFlash('success', 'Multimedia has been created.');
                    return $this->redirect(['update', 'id' => $model->multimedia->id]);
                } else {
                    return $this->render('create', ['model' => $model]);
                }
            }
            return $this->render('create', ['model' => $model]);
        }else{
            if(!Yii::$app->user->isGuest){
                Yii::$app->user->logout();
            }
            return $this->goHome();
        }
    }

    /**
     * Updates an existing Multimedia model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

    public function actionUpdate($id)
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            $model = new MultimediaForm();
            $model->multimedia = $this->findModel($id);
            $model->setAttributes(Yii::$app->request->post());
            $model->multimedia->updated_by_user = Yii::$app->user->identity->id;

            if (Yii::$app->request->post()) {
                $base_64_image = $_POST['result_cropped_main_image'];
                if (!empty($base_64_image)) {
                    $md5_val = md5(uniqid(rand(), true));
                    $md5_val_2 = md5(uniqid(rand(), true));
                    $file_destination = '/media/multimedia/' . $md5_val . '-' . $md5_val_2 . '.jpg';
                    file_put_contents(Url::to('@frontend/web') . $file_destination, base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base_64_image)));
                    $model->multimedia->image = $file_destination;
//                $model->save();
                }
                if (Yii::$app->user->identity->position == 'redaktor' or Yii::$app->user->identity->is_superuser == 1) {
                } else {
                    $model->is_active = 0;
                }
                if ($model->save()) {
                    Yii::$app->getSession()->setFlash('success', 'multimedia has been updated.');
                    return $this->redirect(['update', 'id' => $model->multimedia->id]);
                } else {
                    return $this->render('create', ['model' => $model]);
                }
            }
            return $this->render('update', ['model' => $model]);
        }else{
            if(!Yii::$app->user->isGuest){
                Yii::$app->user->logout();
            }
            return $this->goHome();
        }
    }


    /**
     * Deletes an existing Multimedia model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }else{
            if(!Yii::$app->user->isGuest){
                Yii::$app->user->logout();
            }
            return $this->goHome();
        }
    }

    /**
     * Finds the Multimedia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Multimedia the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Multimedia::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
