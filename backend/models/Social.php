<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "social".
 *
 * @property integer $id
 * @property string $name
 * @property integer $order_index
 * @property string $url
 * @property string $icon
 * @property string $date
 */
class Social extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'social';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'order_index', 'url', 'icon'], 'required'],
            [['order_index'], 'integer'],
            [['date'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['url'], 'string', 'max' => 500],
            [['icon'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'order_index' => 'Order Index',
            'url' => 'Url',
            'icon' => 'Icon',
            'date' => 'Date',
        ];
    }
}
