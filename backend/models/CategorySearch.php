<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Category;

/**
 * CategorySearch represents the model behind the search form about `backend\models\Category`.
 */
class CategorySearch extends Category
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent', 'status', 'order_index', 'author', 'news_count'], 'integer'],
            [['name', 'unique_name', 'logo', 'description', 'publish_start_date', 'publish_end_date', 'news_background', 'suffix_color', 'prefix_color', 'date', 'slug'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Category::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parent' => $this->parent,
            'status' => $this->status,
            'order_index' => $this->order_index,
            'author' => $this->author,
            'publish_start_date' => $this->publish_start_date,
            'publish_end_date' => $this->publish_end_date,
            'date' => $this->date,
            'news_count' => $this->news_count,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'unique_name', $this->unique_name])
            ->andFilterWhere(['like', 'logo', $this->logo])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'news_background', $this->news_background])
            ->andFilterWhere(['like', 'suffix_color', $this->suffix_color])
            ->andFilterWhere(['like', 'prefix_color', $this->prefix_color])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}
