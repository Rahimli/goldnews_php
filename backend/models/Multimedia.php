<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "multimedia".
 *
 * @property integer $id
 * @property integer $is_active
 * @property integer $author
 * @property integer $updated_by_user
 * @property string $image
 * @property string $type
 * @property string $prefix_name
 * @property string $suffix_name
 * @property string $slug
 * @property integer $show_slider
 * @property string $short_text
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $publish_start_date
 * @property string $publish_end_date
 * @property integer $read_count
 * @property string $date
 *
 * @property User $author0
 * @property User $updatedByUser
 * @property MultimediaImage[] $multimediaImages
 * @property MultimediaVideo[] $multimediaVideos
 */
class Multimedia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'multimedia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active', 'author', 'image', 'type', 'slug', 'show_slider', 'publish_start_date'], 'required'],
            [['is_active', 'author', 'updated_by_user', 'show_slider', 'read_count'], 'integer'],
            [['meta_description'], 'string'],
            [['publish_start_date', 'publish_end_date', 'date'], 'safe'],
            [['image', 'type', 'prefix_name', 'suffix_name', 'slug', 'short_text', 'meta_keywords'], 'string', 'max' => 255],
            [['author'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author' => 'id']],
            [['updated_by_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_active' => 'Is Active',
            'author' => 'Author',
            'updated_by_user' => 'Updated By User',
            'image' => 'Image',
            'type' => 'Type',
            'prefix_name' => 'Prefic Name',
            'suffix_name' => 'Suffix Name',
            'slug' => 'Slug',
            'show_slider' => 'Show Slider',
            'short_text' => 'Short Text',
            'meta_keywords' => 'Meta Keywords',
            'meta_description' => 'Meta Description',
            'publish_start_date' => 'Publish Start Date',
            'publish_end_date' => 'Publish End Date',
            'read_count' => 'Read Count',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor0()
    {
        return $this->hasOne(User::className(), ['id' => 'author']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedByUser()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMultimediaImages()
    {
        return $this->hasMany(MultimediaImage::className(), ['multimedia_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMultimediaVideos()
    {
        return $this->hasMany(MultimediaVideo::className(), ['multimedia_id' => 'id']);
    }
}
