<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%weather}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $status
 * @property string $icon
 * @property string $degrees
 * @property string $wind_speed
 * @property string $moisture
 * @property string $date
 *
 * @property City $city
 */
class Weather extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%weather}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'status', 'icon', 'degrees', 'wind_speed', 'moisture', 'date'], 'required'],
            [['city_id'], 'integer'],
            [['date'], 'safe'],
            [['status', 'icon', 'degrees', 'wind_speed', 'moisture'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'status' => 'Status',
            'icon' => 'Icon',
            'degrees' => 'Degrees',
            'wind_speed' => 'Wind Speed',
            'moisture' => 'Moisture',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }
}
