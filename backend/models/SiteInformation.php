<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "site_information".
 *
 * @property integer $id
 * @property string $site_name
 * @property string $site_full_name
 * @property string $image
 * @property string $meta_description
 * @property string $meta_keywords
 * @property integer $publish
 * @property string $date
 */
class SiteInformation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_information';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_name', 'date'], 'required'],
            [['meta_description'], 'string'],
            [['publish'], 'integer'],
            [['date'], 'safe'],
            [['site_name', 'site_full_name', 'image', 'meta_keywords'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'site_name' => 'Site Name',
            'site_full_name' => 'Site Full Name',
            'image' => 'Image',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'publish' => 'Publish',
            'date' => 'Date',
        ];
    }
}
