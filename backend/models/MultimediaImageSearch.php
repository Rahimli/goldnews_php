<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\MultimediaImage;

/**
 * MultimediaImageSearch represents the model behind the search form about `backend\models\MultimediaImage`.
 */
class MultimediaImageSearch extends MultimediaImage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'multimedia_id'], 'integer'],
            [['image', 'context', 'date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MultimediaImage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'multimedia_id' => $this->multimedia_id,
            'date' => $this->date,
        ]);

        $query->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'context', $this->context]);

        return $dataProvider;
    }
}
