<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%multimedia_image}}".
 *
 * @property integer $id
 * @property integer $multimedia_id
 * @property string $image
 * @property string $context
 * @property string $date
 *
 * @property Multimedia $multimedia
 */
class MultimediaImage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%multimedia_image}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image',], 'required'],
            [['multimedia_id'], 'integer'],
            [['context'], 'string'],
            [['date'], 'safe'],
            [['image'], 'string', 'max' => 255],
            [['multimedia_id'], 'exist', 'skipOnError' => true, 'targetClass' => Multimedia::className(), 'targetAttribute' => ['multimedia_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'multimedia_id' => 'Multimedia ID',
            'image' => 'Image',
            'context' => 'Context',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMultimedia()
    {
        return $this->hasOne(Multimedia::className(), ['id' => 'multimedia_id']);
    }
}
