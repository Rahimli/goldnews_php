<?php

namespace backend\models;

use cornernote\linkall\LinkAllBehavior;
use Yii;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property integer $id
 * @property string $u_id
 * @property integer $category_id
 * @property integer $is_active
 * @property integer $author
 * @property integer $updated_by_user
 * @property string $image
 * @property string $first_name
 * @property string $second_name
 * @property string $slug
 * @property integer $show_slider
 * @property string $short_text
 * @property string $full_text
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $publish_start_date
 * @property string $publish_end_date
 * @property integer $read_count
 * @property string $date
 *
 * @property User $author0
 * @property Category $category
 * @property User $updatedByUser
 * @property NewsView[] $newsViews
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $tag_ids;
    public static function tableName()
    {
        return '{{%news}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tag_ids','category_id', 'author', 'image', 'slug', 'full_text', 'publish_start_date'], 'required'],
            [['category_id', 'is_active', 'author', 'updated_by_user', 'show_slider', 'read_count'], 'integer'],
            [['full_text', 'meta_description'], 'string'],
            [['publish_start_date', 'publish_end_date', 'date'], 'safe'],
            [['u_id', 'image', 'first_name', 'second_name', 'slug', 'short_text', 'meta_keywords'], 'string', 'max' => 255],
            [['u_id'], 'unique'],
            [['author'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['updated_by_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'u_id' => 'U ID',
            'category_id' => 'Categorya',
            'is_active' => 'Is Active',
            'author' => 'Author',
            'updated_by_user' => 'Updated By User',
            'image' => 'Image',
            'first_name' => 'First Name',
            'second_name' => 'Second Name',
            'slug' => 'Slug',
            'show_slider' => 'Show Slider',
            'short_text' => 'Short Text',
            'full_text' => 'Full Text',
            'meta_keywords' => 'Meta Keywords',
            'meta_description' => 'Meta Description',
            'publish_start_date' => 'Publish Start Date',
            'publish_end_date' => 'Publish End Date',
            'read_count' => 'Read Count',
            'date' => 'Date',
        ];
    }
    public function behaviors()
    {
        return [
            LinkAllBehavior::className(),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function afterSave($insert, $changedAttributes)
    {
        $tags = [];
        foreach ($this->tag_ids as $tag_name) {
            $tag = Tag::getTagByName($tag_name);
            if ($tag) {
                $tags[] = $tag;
            }
        }
        $this->linkAll('tags', $tags);
        parent::afterSave($insert, $changedAttributes);
    }

    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])
            //->via('postToTag');
            ->viaTable('news_to_tag', ['news_id' => 'id']);
    }
    public function getAuthor0()
    {
        return $this->hasOne(User::className(), ['id' => 'author']);
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedByUser()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsViews()
    {
        return $this->hasMany(NewsView::className(), ['news' => 'id']);
    }
}
