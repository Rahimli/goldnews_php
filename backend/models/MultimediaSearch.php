<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Multimedia;

/**
 * MultimediaSearch represents the model behind the search form about `backend\models\Multimedia`.
 */
class MultimediaSearch extends Multimedia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_active', 'author', 'updated_by_user', 'show_slider', 'read_count'], 'integer'],
            [['image', 'type', 'prefix_name', 'suffix_name', 'slug', 'short_text', 'meta_keywords', 'meta_description', 'publish_start_date', 'publish_end_date', 'date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Multimedia::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_active' => $this->is_active,
            'author' => $this->author,
            'updated_by_user' => $this->updated_by_user,
            'show_slider' => $this->show_slider,
            'publish_start_date' => $this->publish_start_date,
            'publish_end_date' => $this->publish_end_date,
            'read_count' => $this->read_count,
            'date' => $this->date,
        ]);

        $query->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'prefix_name', $this->prefix_name])
            ->andFilterWhere(['like', 'suffix_name', $this->suffix_name])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'short_text', $this->short_text])
            ->andFilterWhere(['like', 'meta_keywords', $this->meta_keywords])
            ->andFilterWhere(['like', 'meta_description', $this->meta_description]);

        return $dataProvider;
    }
}
