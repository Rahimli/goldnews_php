<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%category}}".
 *
 * @property integer $id
 * @property integer $parent
 * @property integer $status
 * @property integer $order_index
 * @property string $name
 * @property string $unique_name
 * @property string $logo
 * @property integer $author
 * @property string $description
 * @property string $publish_start_date
 * @property string $publish_end_date
 * @property string $news_background
 * @property string $suffix_color
 * @property string $prefix_color
 * @property string $date
 * @property integer $news_count
 * @property string $slug
 *
 * @property Category $parent0
 * @property Category[] $categories
 * @property News[] $news
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent', 'status', 'order_index', 'author', 'news_count'], 'integer'],
            [['status', 'order_index', 'name', 'unique_name', 'publish_start_date', 'slug'], 'required'],
            [['description'], 'string'],
//            [['parent'],null],
            [['publish_start_date', 'publish_end_date', 'date'], 'safe'],
            [['name', 'unique_name', 'logo', 'news_background', 'suffix_color', 'prefix_color', 'slug'], 'string', 'max' => 255],
            [['unique_name'], 'unique'],
            [['slug'], 'unique'],
            [['parent'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['parent' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent' => 'Parent',
            'status' => 'Status',
            'order_index' => 'Order Index',
            'name' => 'Name',
            'unique_name' => 'Unique Name',
            'logo' => 'Logo',
            'author' => 'Author',
            'description' => 'Description',
            'publish_start_date' => 'Publish Start Date',
            'publish_end_date' => 'Publish End Date',
            'news_background' => 'News Background',
            'suffix_color' => 'Suffix Color',
            'prefix_color' => 'Prefix Color',
            'date' => 'Date',
            'news_count' => 'News Count',
            'slug' => 'Slug',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    /**
     * @return \yii\db\ActiveQuery
     */
//    public static function getCategoryByID($category_id)
//    {
//        $category = Category::find()->where(['id' => $category_id])->one();
////        if (!$category) {
////            $category = new category();
////            $category->id = $id;
////            $category->save(false);
////        }
//        return $category;
//    }
    public function getParent0()
    {
        return $this->hasOne(Category::className(), ['id' => 'parent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['parent' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasMany(News::className(), ['category_id' => 'id']);
    }
    public function getNews7()
    {
        return $this->hasMany(News::className(), ['category_id' => 'id'])->limit(11)->all();
    }
//    public function getNews7()
//    {
//        $query = News::find()
//            ->select('news.*')
//            ->innerJoin('category_news', '`news`.`id` = `category_news`.`news_id`')
//            ->where(['category_news.category_id' => 1])->orderBy(['date' => SORT_DESC]);
//        return $query;
//    }
}
