<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%news_view}}".
 *
 * @property integer $id
 * @property integer $news
 * @property string $ip
 * @property string $session
 * @property string $date
 *
 * @property News $news0
 */
class NewsView extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news_view}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news', 'ip', 'session', 'date'], 'required'],
            [['news'], 'integer'],
            [['date'], 'safe'],
            [['ip'], 'string', 'max' => 40],
            [['session'], 'string', 'max' => 255],
            [['news'], 'exist', 'skipOnError' => true, 'targetClass' => News::className(), 'targetAttribute' => ['news' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'news' => 'News',
            'ip' => 'Ip',
            'session' => 'Session',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews0()
    {
        return $this->hasOne(News::className(), ['id' => 'news']);
    }
}
