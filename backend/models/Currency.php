<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "currency".
 *
 * @property integer $id
 * @property integer $order_index
 * @property string $name
 * @property string $value
 * @property string $nominal
 * @property integer $status
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_index', 'name', 'value', 'nominal'], 'required'],
            [['order_index', 'status'], 'integer'],
            [['name', 'value', 'nominal'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_index' => 'Order Index',
            'name' => 'Name',
            'value' => 'Value',
            'nominal' => 'Nominal',
            'status' => 'Status',
        ];
    }
}
