<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%multimedia_video}}".
 *
 * @property integer $id
 * @property integer $multimedia_id
 * @property string $video_file
 * @property string $video_url
 * @property string $date
 *
 * @property Multimedia $multimedia
 */
class MultimediaVideo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%multimedia_video}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['multimedia_id'], 'required'],
            [['multimedia_id'], 'integer'],
            [['date'], 'safe'],
            [['video_file', 'video_url'], 'string', 'max' => 500],
            [['multimedia_id'], 'exist', 'skipOnError' => true, 'targetClass' => Multimedia::className(), 'targetAttribute' => ['multimedia_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'multimedia_id' => 'Multimedia ID',
            'video_file' => 'Video File',
            'video_url' => 'Video Url',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMultimedia()
    {
        return $this->hasOne(Multimedia::className(), ['id' => 'multimedia_id']);
    }
}
