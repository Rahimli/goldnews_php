<?php
/**
 * Created by PhpStorm.
 * User: rahim
 * Date: 7/25/2017
 * Time: 12:17
 */

namespace app\models\form;

use backend\models\Multimedia;
use backend\models\MultimediaImage;
use backend\models\MultimediaVideo;
use Yii;
use yii\base\Model;
use yii\widgets\ActiveForm;

class MultimediaForm extends Model
{
    private $_multimedia;
    private $_multimediaImages;
    private $_multimediaVideos;

    public function rules()
    {
        return [
            [['Multimedia'], 'required'],
            [['MultimediaImages'], 'safe'],
            [['MultimediaVideos'], 'safe'],
        ];
    }

    public function afterValidate()
    {
        if (!Model::validateMultiple($this->getAllModels())) {
            $this->addError(null); // add an empty error to prevent saving
        }
        parent::afterValidate();
    }

    public function save()
    {
        if (!$this->validate()) {
            return false;
        }
        $transaction = Yii::$app->db->beginTransaction();
        if (!$this->multimedia->save()) {
            $transaction->rollBack();
            return false;
        }
        if (!$this->saveMultimediaImages()) {
            $transaction->rollBack();
            return false;
        }
        if (!$this->saveMultimediaVideos()) {
            $transaction->rollBack();
            return false;
        }
        $transaction->commit();
        return true;
    }

    public function saveMultimediaImages()
    {
        $keep = [];
        foreach ($this->multimediaImages as $multimedia_image) {
            $multimedia_image->multimedia_id = $this->multimedia->id;
            if (!$multimedia_image->save(false)) {
                return false;
            }
            $keep[] = $multimedia_image->id;
        }
        $query = MultimediaImage::find()->andWhere(['multimedia_id' => $this->multimedia->id]);
        if ($keep) {
            $query->andWhere(['not in', 'id', $keep]);
        }
        foreach ($query->all() as $multimedia_image) {
            $multimedia_image->delete();
        }
        return true;
    }
    public function saveMultimediaVideos()
    {
        $keep = [];
        foreach ($this->multimediaVideos as $multimedia_video) {
            $multimedia_video->multimedia_id = $this->multimedia->id;
            if (!$multimedia_video->save(false)) {
                return false;
            }
            $keep[] = $multimedia_video->id;
        }
        $query = MultimediaVideo::find()->andWhere(['multimedia_id' => $this->multimedia->id]);
        if ($keep) {
            $query->andWhere(['not in', 'id', $keep]);
        }
        foreach ($query->all() as $multimedia_video) {
            $multimedia_video->delete();
        }
        return true;
    }

    public function getMultimedia()
    {
        return $this->_multimedia;
    }

    public function setMultimedia($multimedia)
    {
        if ($multimedia instanceof Multimedia) {
            $this->_multimedia = $multimedia;
        } else if (is_array($multimedia)) {
            $this->_multimedia->setAttributes($multimedia);
        }
    }

    public function getMultimediaImages()
    {
        if ($this->_multimediaImages === null) {
            $this->_multimediaImages = $this->multimedia->isNewRecord ? [] : $this->multimedia->multimediaImages;
        }
        return $this->_multimediaImages;
    }

    public function getMultimediaVideos()
    {
        if ($this->_multimediaVideos === null) {
            $this->_multimediaVideos = $this->multimedia->isNewRecord ? [] : $this->multimedia->multimediaVideos;
        }
        return $this->_multimediaVideos;
    }

    private function getMultimediaImage($key)
    {
        $multimedia_image = $key && strpos($key, 'new') === false ? MultimediaImage::findOne($key) : false;
        if (!$multimedia_image) {
            $multimedia_image = new MultimediaImage();
            $multimedia_image->loadDefaultValues();
        }
        return $multimedia_image;
    }

    private function getMultimediaVideo($key)
    {
        $multimedia_video = $key && strpos($key, 'new') === false ? MultimediaVideo::findOne($key) : false;
        if (!$multimedia_video) {
            $multimedia_video = new MultimediaVideo();
            $multimedia_video->loadDefaultValues();
        }
        return $multimedia_video;
    }

    public function setMultimediaImages($multimediaImages)
    {
        unset($multimediaImages['__id__']); // remove the hidden "new MultimediaImage" row
        $this->_multimediaImages = [];
        foreach ($multimediaImages as $key => $multimedia_image) {
            if (is_array($multimedia_image)) {
                $this->_multimediaImages[$key] = $this->getMultimediaImage($key);
                $this->_multimediaImages[$key]->setAttributes($multimedia_image);
            } elseif ($multimedia_image instanceof MultimediaImage) {
                $this->_multimediaImages[$multimedia_image->id] = $multimedia_image;
            }
        }
    }


    public function setMultimediaVideos($multimediaVideos)
    {
        unset($multimediaVideos['__id__']); // remove the hidden "new MultimediaVideos" row
        $this->_multimediaVideos = [];
        foreach ($multimediaVideos as $key => $multimedia_video) {
            if (is_array($multimedia_video)) {
                $this->_multimediaVideos[$key] = $this->getMultimediaVideo($key);
                $this->_multimediaVideos[$key]->setAttributes($multimedia_video);
            } elseif ($multimedia_video instanceof MultimediaVideo) {
                $this->_multimediaVideos[$multimedia_video->id] = $multimedia_video;
            }
        }
    }

    public function errorSummary($form)
    {
        $errorLists = [];
        foreach ($this->getAllModels() as $id => $model) {
            $errorList = $form->errorSummary($model, [
                'header' => '<p>Please fix the following errors for <b>' . $id . '</b></p>',
            ]);
            $errorList = str_replace('<li></li>', '', $errorList); // remove the empty error
            $errorLists[] = $errorList;
        }
        return implode('', $errorLists);
    }

    private function getAllModels()
    {
        $models = [
            'Multimedia' => $this->multimedia,
        ];
        foreach ($this->multimediaImages as $id => $multimedia_image) {
            $models['MultimediaImage.' . $id] = $this->multimediaImages[$id];
        }
        foreach ($this->multimediaVideos as $id => $multimedia_video) {
            $models['MultimediaVideos.' . $id] = $this->multimediaVideos[$id];
        }
        return $models;
    }
}