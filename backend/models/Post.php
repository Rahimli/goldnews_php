<?php

namespace backend\models;
use cornernote\linkall\LinkAllBehavior;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%post}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $body
 *
 * @property PostToTag[] $postToTags
 * @property Tag[] $tags
 */
class Post extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $tag_ids;

    public static function tableName()
    {
        return '{{%post}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'body', 'tag_ids'], 'required'],
            [['body'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    public function behaviors()
    {
        return [
            LinkAllBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
        ];
    }
    public function afterSave($insert, $changedAttributes)
    {
        $tags = [];
        foreach ($this->tag_ids as $tag_name) {
            $tag = Tag::getTagByName($tag_name);
            if ($tag) {
                $tags[] = $tag;
            }
        }
        $this->linkAll('tags', $tags);
        parent::afterSave($insert, $changedAttributes);
    }

    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])
            //->via('postToTag');
            ->viaTable('post_to_tag', ['post_id' => 'id']);
    }
}
    /**
     * @return \yii\db\ActiveQuery
     */


//    public function afterSave($insert, $changedAttributes)
//    {
//        $tags = [];
//        foreach ($this->tag_ids as $tag_name) {
//            $tag = Tag::getTagByName($tag_name);
//            if ($tag) {
//                $tags[] = $tag;
//            }
//        }
//        $this->linkAll('tags', $tags);
//        parent::afterSave($insert, $changedAttributes);
//    }
//
//    public function getTags()
//    {
//        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])
//            //->via('postToTag');
//            ->viaTable('post_to_tag', ['post_id' => 'id']);
//    }
//}
