<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property integer $id
 * @property string $name
 * @property string $unique_name
 * @property integer $order_index
 * @property integer $is_active
 * @property string $date
 *
 * @property Weather[] $weathers
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'unique_name', 'order_index', 'is_active'], 'required'],
            [['order_index', 'is_active'], 'integer'],
            [['date'], 'safe'],
            [['name', 'unique_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'unique_name' => 'Unique Name',
            'order_index' => 'Order Index',
            'is_active' => 'Is Active',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWeathers()
    {
        return $this->hasMany(Weather::className(), ['city_id' => 'id']);
    }
}
