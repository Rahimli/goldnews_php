<?php

namespace backend\controllers;

use Yii;
use backend\models\News;
use backend\models\NewsSearch;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            $searchModel = new NewsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
        }
    }

    public function actionEditor()
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            $searchModel = new NewsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
        }
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        } else {
        }
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {

            $model = new News();
            $model->read_count = 0;
            $message = "Xəta";
            $message_code = 0;
//        die(\thamtech\uuid\helpers\UuidHelper::uuid());
            $model->publish_start_date = date('Y-m-d');
            if ($model->load(Yii::$app->request->post())) {
                if ((Yii::$app->user->identity->position == 'redaktor') or Yii::$app->user->identity->is_superuser == "1") {

                }else{
                    $model->is_active = 0;
                }
                $model->u_id = \thamtech\uuid\helpers\UuidHelper::uuid();
                $base_64_image = $_POST['result_cropped_main_image'];
                if (!empty($base_64_image)) {
                    $md5_val = md5(uniqid(rand(), true));
                    $md5_val_2 = md5(uniqid(rand(), true));
                    $file_destination = '/media/news/' . $md5_val . '-' . $md5_val_2 . '.jpg';
                    file_put_contents(Url::to('@frontend/web') . $file_destination, base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base_64_image)));
                    $model->image = $file_destination;
                    $model->author = Yii::$app->user->identity->id;
                    if (Yii::$app->user->identity->position == 'redaktor' or Yii::$app->user->identity->is_superuser == 1) {
                    }else{
                        $model->is_active = 0;
                    }
                    try {
                        if (isset($model->id) and !empty($model->id)) {

                            $model->save();
                            $message = "Əla";
                            $message_code = 1;
                        } else {
                            $message = "";
                            $message_code = -1;
                        }
                        $url = Url::to(['news/update', 'id' => $model->id]);
                    } catch (Exception $e) {
                        $url = '';
                        $message_code = 0;
                    }
                    $data = array('message' => $message, 'message_code' => $message_code, 'url' => $url);
                    \Yii::$app->response->format = 'json';
//                return $data;
                    return $this->redirect(['update', 'id' => $model->id]);
                } else {
                    $model->addError('image', 'Şəkil Boş Qoyula Bilməz.');
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
            } else {
                $model->tag_ids = ArrayHelper::map($model->tags, 'name', 'name');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else{
            if(!Yii::$app->user->isGuest){
                Yii::$app->user->logout();
            }
            return $this->goHome();
        }
    }

//     public function actionCreate()
//     {
//         $model = new News();
//         $model->read_count = 0;
//         $model->publish_start_date = date('Y-m-d');
//         if ($model->load(Yii::$app->request->post())) {
//             try {

//                 $base_64_image = $_POST['result_cropped_main_image'];
//                 if (!empty($base_64_image)) {
//                     $md5_val = md5(uniqid(rand(), true));
//                     $md5_val_2 = md5(uniqid(rand(), true));
//                     $file_destination = '/media/news/' . $md5_val . '-' . $md5_val_2 . '.jpg';
//                     file_put_contents(Url::to('@frontend/web') . $file_destination, base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base_64_image)));
//                     $model->image = $file_destination;
//                     $model->author = Yii::$app->user->identity->id;
//                     $model_save = $model->save();
//                     if ($model_save) {
//                         return $this->redirect(['update', 'id' => $model->id]);
//                     } else {
// //                    $model->addError('image', 'Şəkil Boş Qoyula Bilməz.');
//                         return $this->render('create', [
//                             'model' => $model,
//                         ]);
//                     }
//                 } else {
//                     $model->addError('image', 'Şəkil Boş Qoyula Bilməz.');
//                     return $this->render('create', [
//                         'model' => $model,
//                     ]);
//                 }
//             }catch (Exception $e) {
//                 return $this->render('create', [
//                     'model' => $model,
//                 ]);
//             }
//         } else {
//             return $this->render('create', [
//                 'model' => $model,
//             ]);
//         }
//     }


    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            $model = $this->findModel($id);
            $message = "Xəta";
            $message_code = 0;
            if ($model->load(Yii::$app->request->post())) {
//        if ($model->load(Yii::$app->request->post())) {

//            die('asaskl');
                $base_64_image = $_POST['result_cropped_main_image'];
                if (!empty($base_64_image)) {
                    $md5_val = md5(uniqid(rand(), true));
                    $md5_val_2 = md5(uniqid(rand(), true));
                    $file_destination = '/media/news/' . $md5_val . '-' . $md5_val_2 . '.jpg';
                    file_put_contents(Url::to('@frontend/web') . $file_destination, base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base_64_image)));
                    $model->image = $file_destination;
//                $model->save();
                }
                if (Yii::$app->user->identity->position == 'redaktor' or Yii::$app->user->identity->is_superuser == 1) {
                }else{
                    $model->is_active = 0;
                }
                $model->updated_by_user = Yii::$app->user->identity->id;
                $model->save();
                $message = "Əla";
                $message_code = 1;
//            $url = Url::to(['news/view','id'=>$model->id]);
//            $data = array('message' => $message,'message_code' => $message_code,'url' => $url);
//            \Yii::$app->response->format = 'json';
//            return $data;
                return $this->redirect(['update', 'id' => $model->id]);
            } else {
                $model->tag_ids = ArrayHelper::map($model->tags, 'name', 'name');
//            $model->category_ids = ArrayHelper::map($model->categories, 'name', 'id');
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
        }
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        } else {
        }
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
